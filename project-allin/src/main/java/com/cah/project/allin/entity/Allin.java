package com.cah.project.allin.entity;

import lombok.Data;

/**
 * 功能描述: 规则实体 <br/>
 */
@Data
public class Allin {

    /** 所属组 */
    private String group;
    /** 规则编号 */
    private String no;
    /** 规则名称 */
    private String name;
    /** 状态：1-生效；0-未生效 */
    private String status;
    /** 表达式 */
    private String el;
    /** 描述 */
    private String comment;

}
