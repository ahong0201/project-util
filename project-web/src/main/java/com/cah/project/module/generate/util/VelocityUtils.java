package com.cah.project.module.generate.util;

import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.resource.ClassPathResource;
import cn.hutool.core.util.StrUtil;
import com.cah.project.conf.GenConfig;
import com.cah.project.exception.BusinessException;
import com.cah.project.module.generate.domain.bo.GenNameBO;
import com.cah.project.module.generate.domain.bo.GenTableBO;
import org.apache.velocity.VelocityContext;

import java.io.File;
import java.util.List;

/**
 * 功能描述: 模板引擎工具 <br/>
 */
public class VelocityUtils {

    /** 项目空间路径 */
    private static final String PROJECT_PATH = "main/java";
    /** mybatis空间路径 */
    private static final String MYBATIS_PATH = "main/resources/mapper";
    /** html空间路径 */
    private static final String TEMPLATES_PATH = "main/resources/templates";
    /** 默认上级菜单，系统工具 */
    private static final String DEFAULT_PARENT_MENU_ID = "3";

    /**
     * 功能描述: 设置模板变量信息 <br/>
     *
     * @param genTable 生成表定义
     * @return "org.apache.velocity.VelocityContext"
     */
    public static VelocityContext prepareContext(GenTableBO genTable) {
        VelocityContext velocityContext = new VelocityContext();
        velocityContext.put("table", genTable);
        velocityContext.put("swagger", GenConfig.getSwagger());
        velocityContext.put("date", DateUtil.today());
        return velocityContext;
    }

    /**
     * 功能描述: 通过模版类型，获取模版列表 <br/>
     *
     * @param tplCategory 模版类型
     * @return "java.util.List<java.lang.String>"
     */
    public static List<String> getTemplateList(String tplCategory) {
        List<String> templates = ListUtil.list(false);
        // 基础包，如果需要，可以复制base包，再进行修改 tplCategory 相当于包名
        ClassPathResource resource = new ClassPathResource("generate/"+ tplCategory +"/");
        List<File> files = FileUtil.loopFiles(resource.getFile());
        List<String> fileNameList = ListUtil.list(false);
        for (File f : files) {
            if(f.isFile()) {
                if(fileNameList.contains(f.getName())) {
                    throw new BusinessException("模板名称不能出现重复！" + f.getPath());
                } else {
                    fileNameList.add(f.getName());
                }
                String templateFilePath = StrUtil.subAfter(f.getPath(), "classes\\", true);
                System.out.println(templateFilePath);
                templates.add(templateFilePath.replaceAll("\\\\", "/"));
            }
        }
        return templates;
    }

    /**
     * 功能描述: 获取基础路径 <br/>
     *
     * @param genTable 定义表
     * @return "java.lang.String"
     */
    public static String getBasePath(GenTableBO genTable) {
        return StrUtil.isEmpty(genTable.getGenPath()) ?
                System.getProperty("user.dir") + File.separator + "code" + File.separator :
                // 判断最后一个字符是不是为 分隔符
                genTable.getGenPath().substring(genTable.getGenPath().length() - 1).indexOf(File.separator) == 0 ?
                        genTable.getGenPath() : genTable.getGenPath() + File.separator;
    }

    /**
     * 功能描述: 获取文件生成路径 <br/>
     *
     * @param basePath 基础路径
     * @param genTable 定义表
     * @param template 模版名
     * @return "java.lang.String"
     */
    public static String getPath(String basePath, GenTableBO genTable, String template) {
        String moduleBusiness = (StrUtil.isEmpty(genTable.getModuleName()) ? "" : genTable.getModuleName()) + (StrUtil.isEmpty(genTable.getBusinessName()) ? "" : File.separator + genTable.getBusinessName());
        if(StrUtil.isNotEmpty(moduleBusiness)) {
            moduleBusiness = moduleBusiness + File.separator;
        }
        String javaPath = basePath + "src/main/java/";
        String resourcePath = basePath + "src/main/resource/";
        String htmlPath = basePath + "src/main/resource/template/";
        if(template.contains("main.html")) {
            return htmlPath + moduleBusiness + "main.html";
        }
        if(template.contains("view.html")) {
            return htmlPath + moduleBusiness + "view.html";
        }
        if(template.contains("mapper.xml")) {
            return resourcePath + "mapper" + File.separator + moduleBusiness + File.separator + genTable.getGenName().getXmlMapper().getClassName() + ".xml";
        }
        if(template.contains("menu.sql")) {
            return resourcePath + "sql" + File.separator + genTable.getClassName() + ".sql";
        }
        if(template.contains("controller.java")) {
            return javaPath + getJavaFilePath(genTable.getGenName().getController());
        }
        if(template.contains("service.java")) {
            return javaPath + getJavaFilePath(genTable.getGenName().getService());
        }
        if(template.contains("serviceImpl.java")) {
            return javaPath + getJavaFilePath(genTable.getGenName().getServiceImpl());
        }
        if(template.contains("serviceRead.java")) {
            return javaPath + getJavaFilePath(genTable.getGenName().getServiceRead());
        }
        if(template.contains("serviceReadImpl.java")) {
            return javaPath + getJavaFilePath(genTable.getGenName().getServiceReadImpl());
        }
        if(template.contains("serviceWrite.java")) {
            return javaPath + getJavaFilePath(genTable.getGenName().getServiceWrite());
        }
        if(template.contains("serviceWriteImpl.java")) {
            return javaPath + getJavaFilePath(genTable.getGenName().getServiceWriteImpl());
        }
        if(template.contains("helper.java")) {
            return javaPath + getJavaFilePath(genTable.getGenName().getHelper());
        }
        if(template.contains("checker.java")) {
            return javaPath + getJavaFilePath(genTable.getGenName().getChecker());
        }
        if(template.contains("domain.converter.java")) {
            return javaPath + getJavaFilePath(genTable.getGenName().getConverter());
        }
        if(template.contains("domain.entity.java")) {
            return javaPath + getJavaFilePath(genTable.getGenName().getEntity());
        }
        if(template.contains("domain.info.java")) {
            return javaPath + getJavaFilePath(genTable.getGenName().getInfoEntity());
        }
        if(template.contains("domain.outList.java")) {
            return javaPath + getJavaFilePath(genTable.getGenName().getOutListEntity());
        }
        if(template.contains("domain.query.java")) {
            return javaPath + getJavaFilePath(genTable.getGenName().getQueryEntity());
        }
        if(template.contains("mapper.java")) {
            return javaPath + getJavaFilePath(genTable.getGenName().getJavaMapper());
        }
        throw new BusinessException("模版【" + template + "】查找不到对应的路径。");
    }

    public static String getJavaFilePath(GenNameBO.NameModelBO nameModel) {
        return nameModel.getClassFullPackage().replaceAll("\\.", "/") + ".java";
    }

}
