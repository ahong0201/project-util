package com.cah.project.module.generate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cah.project.module.generate.domain.entity.GenTableEntity;
import com.cah.project.module.generate.domain.vo.in.GenTableQuery;
import com.cah.project.module.generate.domain.vo.out.DbTableList;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 功能描述: 生成表定义 mapper <br/>
 */
public interface GenTableMapper extends BaseMapper<GenTableEntity> {

    /**
     * 功能描述: 分页查询数据当前数据库表列表 <br/>
     *
     * @param page 分页
     * @param query 查询入参
     * @return "com.baomidou.mybatisplus.core.metadata.IPage<com.cah.project.module.generate.domain.vo.out.GenTableList>"
     */
    IPage<DbTableList> selectDbPageList(@Param("page") Page<GenTableEntity> page, @Param("query") GenTableQuery query);

    List<DbTableList> selectDbList(List<String> tableNames);

}
