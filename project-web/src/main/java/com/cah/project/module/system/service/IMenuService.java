package com.cah.project.module.system.service;

import com.cah.project.module.system.domain.vo.out.MenuTree;

import java.util.List;

public interface IMenuService {

    List<MenuTree> getUserMenu();

    List<MenuTree> getOneMenu();
}
