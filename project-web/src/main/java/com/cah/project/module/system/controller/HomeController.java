package com.cah.project.module.system.controller;

import com.cah.project.core.util.ModelViewUtil;
import com.cah.project.module.constant.ViewConstant;
import io.swagger.annotations.ApiOperation;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 功能描述: 首页选项卡 <br/>
 */
@RestController
@RequestMapping("/home")
public class HomeController {

    @GetMapping("")
    @ApiOperation(value = "首页")
    public ModelAndView main(Model model) {
        // 可以添加数据，到 model 中，或者异步再次请求
        return ModelViewUtil.stringView(ViewConstant.PATH_HOME);
    }

}
