package com.cah.project.module.constant;

/**
 * 功能描述: 视图路径常量 <br/>
 */
public class ViewConstant {

    /** 主页索引 */
    public static final String PATH_INDEX = "index";
    /** 首页 */
    public static final String PATH_HOME = "home/main";
    /** 系统管理-菜单主页（列表） */
    public static final String PATH_SYS_MENU_MAIN = "system/menu/main";

    /** 主页（列表） */
    public static final String PATH_MAIN = "/main";
    /** 编辑页（详情页） */
    public static final String PATH_VIEW = "/view";

    /** 数据标准 */
    public static final String PATH_STD = "standard";
    /** 辞海 */
    public static final String PATH_CI_HAI = PATH_STD + "/ciHai";

    /** 数据标准-辞海主页（列表） */
    public static final String PATH_STD_CI_HAI_MAIN = "standard/ciHai/main";
    /** 数据标准-辞海编辑页（详情页） */
    public static final String PATH_STD_CI_HAI_VIEW = "standard/ciHai/view";

    /** 数据标准-辞海主页（列表） */
    public static final String PATH_STD_DICT_MAIN = "standard/dict/main";
    public static final String PATH_STD_DICT_TYPE_VIEW = "standard/dict/typeView";
    public static final String PATH_STD_DICT_DATA_VIEW = "standard/dict/dataView";

    /** 元数据-数据源主页（列表） */
    public static final String PATH_META_DATA_SOURCE_MAIN = "meta/data-source/main";
    /** 元数据-数据源编辑页（详情页） */
    public static final String PATH_META_DATA_SOURCE_VIEW = "meta/data-source/view";


    /** 代码生成 */
    public static final String PATH_GEN = "generate";
    /** 代码生成主页（列表） */
    public static final String PATH_GEN_MAIN = "generate/main";
    /** 代码生成编辑页（详情页） */
    public static final String PATH_GEN_VIEW = "generate/view";
    /** 代码生成导入实体表页 */
    public static final String PATH_GEN_IMPORT_TABLE = "generate/importTable";

    /** 数据详情标识 */
    public static final String MODEL_DATA_INFO = "dataInfo";

}
