package com.cah.project.module.generate.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cah.project.module.generate.domain.entity.GenTableColumnEntity;
import com.cah.project.module.generate.domain.vo.out.DbTableColumnList;
import com.cah.project.module.generate.mapper.GenTableColumnMapper;
import com.cah.project.module.generate.service.IGenTableColumnService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 功能描述: 生成表字段定义 服务实现 <br/>
 */
@Service
public class GenTableColumnServiceImpl extends ServiceImpl<GenTableColumnMapper, GenTableColumnEntity> implements IGenTableColumnService {

    @Override
    public List<DbTableColumnList> selectDbList(List<String> tableNames) {
        return baseMapper.selectDbList(tableNames);
    }

    @Override
    public List<GenTableColumnEntity> getByTableId(Long taleId) {
        return baseMapper.selectList(Wrappers.<GenTableColumnEntity>lambdaQuery()
                .eq(GenTableColumnEntity::getTableId, taleId)
                .orderByAsc(GenTableColumnEntity::getSort));
    }

}
