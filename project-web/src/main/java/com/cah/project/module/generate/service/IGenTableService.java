package com.cah.project.module.generate.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cah.project.core.domain.out.PageList;
import com.cah.project.module.generate.domain.entity.GenTableEntity;
import com.cah.project.module.generate.domain.vo.in.GenTableQuery;
import com.cah.project.module.generate.domain.vo.out.DbTableList;
import com.cah.project.module.generate.domain.vo.out.GenTableList;

import java.util.List;

/**
 * 功能描述: 生成表定义 服务接口 <br/>
 */
public interface IGenTableService extends IService<GenTableEntity> {

    /**
     * 功能描述: 分页查询 <br/>
     *
     * @param query 查询入参
     * @return "com.cah.project.core.domain.out.PageList<com.cah.project.module.generate.domain.vo.out.GenTableList>"
     */
    PageList<GenTableList> pageList(GenTableQuery query);

    /**
     * 功能描述: 分页查询数据当前数据库表列表 <br/>
     *
     * @param query 查询入参
     * @return "com.cah.project.core.domain.out.PageList<com.cah.project.module.generate.domain.vo.out.GenTableList>"
     */
    PageList<DbTableList> dbPageList(GenTableQuery query);

    /**
     * 功能描述: 通过表名列表，查询当前数据库表信息 <br/>
     *
     * @param tableNames 表名列表
     * @return "com.cah.project.core.domain.out.PageList<com.cah.project.module.generate.domain.vo.out.GenTableList>"
     */
    List<DbTableList> selectDbList(List<String> tableNames);

}
