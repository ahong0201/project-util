package com.cah.project.module.system.data;

import cn.hutool.core.collection.ListUtil;
import com.cah.project.module.system.domain.vo.out.MenuTree;

import java.util.List;

/**
 * 功能描述: 菜单数据 <br/>
 */
public class MenuData {

    /**
     * 功能描述: 获取全部菜单 <br/>
     *
     * @return "java.util.List<com.cah.project.module.system.domain.vo.out.MenuTree>"
     */
    public static List<MenuTree> getAllMenu() {
        // 系统管理
        MenuTree smt1 = MenuTree.builder().id("1").parentId("0").title("系统管理").type("0").openType(null).icon("layui-icon layui-icon-set-fill").href("").build();
        MenuTree smt11 = MenuTree.builder().id("11").parentId("1").title("用户管理").type("1").openType("_iframe").icon("layui-icon layui-icon layui-icon layui-icon layui-icon-rate").href("/user/main").build();
        MenuTree smt111 = MenuTree.builder().id("111").parentId("11").title("新增").type("3").openType("").icon("layui-icon layui-icon-vercode").href("").build();
        smt11.setChildren(ListUtil.toList(smt111));
        MenuTree smt12 = MenuTree.builder().id("12").parentId("1").title("菜单管理").type("1").openType("_iframe").icon("layui-icon layui-icon layui-icon layui-icon layui-icon-rate").href("/menu/main").build();
        MenuTree smt121 = MenuTree.builder().id("121").parentId("12").title("新增").type("3").openType("").icon("layui-icon layui-icon-vercode").href("").build();
        smt12.setChildren(ListUtil.toList(smt121));

        smt1.setChildren(ListUtil.toList(smt11, smt12));

        // 数据标准
        MenuTree smt2 = MenuTree.builder().id("2").parentId("0").title("数据标准").type("0").openType(null).icon("layui-icon layui-icon-set-fill").href("").build();
        MenuTree smt21 = MenuTree.builder().id("21").parentId("2").title("命名规范").type("1").openType("_iframe").icon("layui-icon layui-icon layui-icon layui-icon layui-icon-rate").href("/standard/ciHai/main").build();
        MenuTree smt211 = MenuTree.builder().id("211").parentId("21").title("新增").type("3").openType("").icon("layui-icon layui-icon-vercode").href("").build();
        smt21.setChildren(ListUtil.toList(smt211));
        // 数据字典
        MenuTree smt22 = MenuTree.builder().id("22").parentId("2").title("数据字典").type("1").openType("_iframe").icon("layui-icon layui-icon layui-icon layui-icon layui-icon-rate").href("/standard/dict/type/main").build();

        smt2.setChildren(ListUtil.toList(smt21, smt22));

        // 元数据
        MenuTree smt3 = MenuTree.builder().id("3").parentId("0").title("元数据").type("0").openType(null).icon("layui-icon layui-icon-set-fill").href("").build();
        MenuTree smt31 = MenuTree.builder().id("31").parentId("3").title("数据源").type("1").openType("_iframe").icon("layui-icon layui-icon layui-icon layui-icon layui-icon-rate").href("/meta/dataSource/main").build();
        MenuTree smt32 = MenuTree.builder().id("32").parentId("3").title("表管理").type("1").openType("_iframe").icon("layui-icon layui-icon layui-icon layui-icon layui-icon-rate").href("/meta/tableManage/main").build();

        smt3.setChildren(ListUtil.toList(smt31, smt32));

        // 开发工具
        MenuTree smt9 = MenuTree.builder().id("9").parentId("0").title("开发工具").type("0").openType(null).icon("layui-icon layui-icon layui-icon-senior").href("").build();
        MenuTree smt91 = MenuTree.builder().id("91").parentId("9").title("布局构建").type("1").openType("_iframe").icon("layui-icon-senior").href("component/code/index.html").build();

        MenuTree smt92 = MenuTree.builder().id("92").parentId("9").title("代码生成").type("1").openType("_iframe").icon("layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon-template-1").href("/gen/main").build();
        MenuTree smt93 = MenuTree.builder().id("93").parentId("9").title("Json格式化").type("1").openType("_iframe").icon("layui-icon-senior").href("component/json-view/index.html").build();

        smt9.setChildren(ListUtil.toList(smt91, smt93, smt92));

        return ListUtil.toList(smt1, smt2, smt3, smt9);
    }
}
