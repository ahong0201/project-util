package com.cah.project.module.generate.util;

import com.cah.project.module.constant.ProjectConstant;
import org.apache.velocity.app.Velocity;

import java.util.Properties;

/**
 * 功能描述: 模板引擎工厂 <br/>
 */
public class VelocityInitializer {

    /**
     * 功能描述: 初始化模板引擎 <br/>
     */
    public static void initVelocity() {
        Properties p = new Properties();
        try {
            p.setProperty("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
            p.setProperty(Velocity.ENCODING_DEFAULT, ProjectConstant.UTF8);
            Velocity.init(p);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
