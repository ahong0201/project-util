package com.cah.project.module.generate.service.helper;

import com.cah.project.core.cache.ThreadDataCacheUtil;
import com.cah.project.module.generate.domain.entity.GenTableColumnEntity;
import com.cah.project.module.generate.domain.entity.GenTableEntity;
import com.cah.project.module.generate.service.IGenTableColumnService;
import com.cah.project.module.generate.service.IGenTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 功能描述: 代码生成 数据缓存 帮助者 <br/>
 */
@Component
public class GenDataCacheHelper {

    @Autowired
    private IGenTableService genTableService;
    @Autowired
    private IGenTableColumnService genTableColumnService;

    public GenTableEntity getCacheGenTable(Long id) {
        Object obj = ThreadDataCacheUtil.get("genTable");
        if(obj == null) {
            cacheGenTable(id);
        }
        return (GenTableEntity) ThreadDataCacheUtil.get("genTable");
    }

    /**
     * 功能描述: 缓存数据 <br/>
     *
     * @param id 主键ID
     */
    public GenTableEntity cacheGenTable(Long id) {
        GenTableEntity genTable = (GenTableEntity) ThreadDataCacheUtil.get("genTable");
        if(genTable == null) {
            genTable = genTableService.getById(id);
            ThreadDataCacheUtil.put("genTable", genTable);
        }
        return genTable;
    }

    public List<GenTableColumnEntity> getCacheGenTableColumn(Long id) {
        Object obj = ThreadDataCacheUtil.get("columnList");
        if(obj == null) {
            cacheGenTableColumn(id);
        }
        return (List<GenTableColumnEntity>) ThreadDataCacheUtil.get("columnList");
    }

    public List<GenTableColumnEntity> cacheGenTableColumn(Long id) {
        List<GenTableColumnEntity> columnList = (List<GenTableColumnEntity>) ThreadDataCacheUtil.get("columnList");
        if(columnList == null) {
            columnList = genTableColumnService.getByTableId(id);
            ThreadDataCacheUtil.put("columnList", columnList);
        }
        return columnList;
    }

}
