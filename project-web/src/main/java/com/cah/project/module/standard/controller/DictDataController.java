package com.cah.project.module.standard.controller;

import com.cah.project.core.dict.domain.vo.DictDataOptions;
import com.cah.project.core.domain.out.CommonPageResult;
import com.cah.project.core.domain.out.CommonResult;
import com.cah.project.core.domain.out.PageList;
import com.cah.project.core.util.ModelViewUtil;
import com.cah.project.enums.YesOrNoEnum;
import com.cah.project.module.constant.ViewConstant;
import com.cah.project.module.standard.domain.vo.DictDataInfo;
import com.cah.project.module.standard.domain.vo.in.DictDataQuery;
import com.cah.project.module.standard.domain.vo.in.DictEnableInVO;
import com.cah.project.module.standard.domain.vo.out.DictDataList;
import com.cah.project.module.standard.service.IDictDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 功能描述: 字典数据 <br/>
 */
@RestController
@RequestMapping("/standard/dict/data")
@Api(tags = {"字典数据"})
public class DictDataController {

    @Autowired
    private IDictDataService dictDataService;

    @GetMapping("view")
    public ModelAndView view(Model model, String typeCode, String id) {
        DictDataInfo info = dictDataService.selectById(typeCode, id);
        return ModelViewUtil.dataView(ViewConstant.PATH_STD_DICT_DATA_VIEW, model, info);
    }

    @GetMapping("pageList")
    @ApiOperation(value="分页查询字典类型列表")
    public CommonPageResult<DictDataList> typePageList(DictDataQuery query){
        PageList<DictDataList> pageList = dictDataService.pageList(query);
        return CommonPageResult.data(pageList);
    }

    @PostMapping("save")
    @ApiOperation(value="保存字典类型数据")
    public CommonResult<String> save(@RequestBody DictDataInfo info){
        dictDataService.saveInfo(info);
        return CommonResult.ok();
    }

    @PutMapping("enable")
    @ApiOperation(value="保存字典类型数据")
    public CommonResult<String> typeEnable(@RequestBody DictEnableInVO inVO){
        dictDataService.updateEnable(inVO.getId(), YesOrNoEnum.YES);
        return CommonResult.ok();
    }

    @PutMapping("disable")
    @ApiOperation(value="保存字典类型数据")
    public CommonResult<String> typeDisable(@RequestBody DictEnableInVO inVO){
        dictDataService.updateEnable(inVO.getId(), YesOrNoEnum.NO);
        return CommonResult.ok();
    }

    @GetMapping("options/{typeCode}")
    @ApiOperation(value = "获取字典数据下拉选项")
    public CommonResult<List<DictDataOptions>> selectDataOptions(@PathVariable String typeCode) {
        return CommonResult.data(dictDataService.selectDataOptionsByCode(typeCode));
    }

    @GetMapping("options/{loadType}/{typeCode}")
    @ApiOperation(value = "获取字典数据下拉选项，可以根据加载参数，找找不同的数据")
    public CommonResult<List<DictDataOptions>> selectDataOptionsByLoadType(@PathVariable String loadType, @PathVariable String typeCode) {
        return CommonResult.data(dictDataService.selectDataOptionsByLoadType(loadType, typeCode));
    }

}
