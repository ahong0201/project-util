package com.cah.project.module.system.controller;


import com.cah.project.core.domain.out.CommonTreeResult;
import com.cah.project.core.util.ModelViewUtil;
import com.cah.project.module.constant.ViewConstant;
import com.cah.project.module.system.domain.vo.out.MenuTree;
import com.cah.project.module.system.service.IMenuService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
@RequestMapping("/menu")
public class MenuController {

    @Autowired
    private IMenuService menuService;

    @GetMapping("main")
    @ApiOperation(value = "获取用户列表视图")
    public ModelAndView main() {
        return ModelViewUtil.stringView(ViewConstant.PATH_SYS_MENU_MAIN);
    }

    @GetMapping("userMenu")
    @ApiOperation(value = "获取用户菜单数据")
    public List<MenuTree> getUserMenu() {
        return menuService.getUserMenu();
    }

    @GetMapping("oneMenu")
    @ApiOperation(value = "获取一级目录菜单树")
    public CommonTreeResult getOneMenu() {
        return CommonTreeResult.data(menuService.getOneMenu());
    }

}
