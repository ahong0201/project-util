package com.cah.project.module.generate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cah.project.module.generate.domain.entity.GenTableColumnEntity;
import com.cah.project.module.generate.domain.vo.out.DbTableColumnList;

import java.util.List;

/**
 * 功能描述: 生成表字段定义 mapper <br/>
 */
public interface GenTableColumnMapper extends BaseMapper<GenTableColumnEntity> {

    /**
     * 功能描述: 通过表名列表，查询数据库字段信息 <br/>
     *
     * @param tableNames 表明列表
     * @return "java.util.List<com.cah.project.module.generate.domain.vo.out.DbTableColumnList>"
     */
    List<DbTableColumnList> selectDbList(List<String> tableNames);

}
