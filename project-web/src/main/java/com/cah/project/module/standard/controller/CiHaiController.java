package com.cah.project.module.standard.controller;

import com.cah.project.core.domain.out.CommonPageResult;
import com.cah.project.core.domain.out.CommonResult;
import com.cah.project.core.domain.out.PageList;
import com.cah.project.core.util.ModelViewUtil;
import com.cah.project.module.constant.ViewConstant;
import com.cah.project.module.standard.domain.vo.CiHaiInfo;
import com.cah.project.module.standard.domain.vo.in.CiHaiQuery;
import com.cah.project.module.standard.domain.vo.out.CiHaiList;
import com.cah.project.module.standard.service.ICiHaiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 辞海 控制层 <br/>
 */
@RestController
@RequestMapping("/standard/ciHai")
@Api(tags = {"辞海（命名规范）"})
public class CiHaiController {

    @Autowired
    private ICiHaiService ciHaiService;

    @GetMapping("main")
    public ModelAndView main() {
        return ModelViewUtil.stringView(ViewConstant.PATH_STD_CI_HAI_MAIN);
    }

    @GetMapping("view")
    public ModelAndView view(Model model, String id) {
        CiHaiInfo info = ciHaiService.selectById(id);
        return ModelViewUtil.dataView(ViewConstant.PATH_STD_CI_HAI_VIEW, model, info);
    }

    @GetMapping("pageList")
    @ApiOperation(value="分页查询列表")
    public CommonPageResult<CiHaiList> pageList(CiHaiQuery query){
        PageList<CiHaiList> pageList = ciHaiService.pageList(query);
        return CommonPageResult.data(pageList);
    }

    @PostMapping("save")
    @ApiOperation(value="保存数据")
    public CommonResult<String> save(@RequestBody CiHaiInfo info){
        ciHaiService.saveInfo(info);
        return CommonResult.ok();
    }

    @DeleteMapping("remove/{id}")
    @ApiOperation(value="删除数据")
    public CommonResult<String> remove(@PathVariable("id") String id){
        ciHaiService.deleteById(id);
        return CommonResult.ok();
    }

}
