package com.cah.project.module.meta.controller;

import com.cah.project.core.domain.out.CommonPageResult;
import com.cah.project.core.domain.out.CommonResult;
import com.cah.project.core.domain.out.PageList;
import com.cah.project.core.util.ModelViewUtil;
import com.cah.project.module.constant.ViewConstant;
import com.cah.project.module.meta.domain.vo.DataSourceInfo;
import com.cah.project.module.meta.domain.vo.in.DataSourceQuery;
import com.cah.project.module.meta.domain.vo.out.DataSourceList;
import com.cah.project.module.meta.service.IDataSourceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * <p>数据源 控制层 </p> <br/>
 *
 * @author cah
 * @since 2021-10-06
 */
@Api(tags = {"数据源 控制层"})
@RestController
@RequestMapping("/meta/dataSource")
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class DataSourceController {

    private final IDataSourceService dataSourceService;

    @GetMapping("main")
    public ModelAndView main() {
        return ModelViewUtil.stringView(ViewConstant.PATH_META_DATA_SOURCE_MAIN);
    }

    @GetMapping("view")
    public ModelAndView view(Model model, String id) {
        DataSourceInfo info = dataSourceService.selectById(id);
        return ModelViewUtil.dataView(ViewConstant.PATH_META_DATA_SOURCE_VIEW, model, info);
    }

    @GetMapping("pageList")
    @ApiOperation(value="分页查询列表")
    public CommonPageResult<DataSourceList> pageList(DataSourceQuery query){
        PageList<DataSourceList> pageList = dataSourceService.pageList(query);
        return CommonPageResult.data(pageList);
    }

    @PostMapping("save")
    @ApiOperation(value="保存数据")
    public CommonResult<String> save(@RequestBody DataSourceInfo info){
        dataSourceService.saveInfo(info);
        return CommonResult.ok();
    }

    @PostMapping("linkTest")
    @ApiOperation(value="连接测试")
    public CommonResult<String> linkTest(@RequestBody DataSourceInfo info){
        dataSourceService.linkTest(info);
        return CommonResult.ok();
    }

    @GetMapping("linkTest/{id}")
    @ApiOperation(value="连接测试")
    public CommonResult<String> linkTestId(@PathVariable("id") String id){
        dataSourceService.linkTest(id);
        return CommonResult.ok();
    }

    @DeleteMapping("remove/{id}")
    @ApiOperation(value="删除数据")
    public CommonResult<String> remove(@PathVariable("id") String id){
        dataSourceService.deleteById(id);
        return CommonResult.ok();
    }

}
