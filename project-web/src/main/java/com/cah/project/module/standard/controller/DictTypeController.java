package com.cah.project.module.standard.controller;

import com.cah.project.core.dict.domain.vo.DictTypeOptions;
import com.cah.project.core.domain.out.CommonPageResult;
import com.cah.project.core.domain.out.CommonResult;
import com.cah.project.core.domain.out.PageList;
import com.cah.project.core.util.ModelViewUtil;
import com.cah.project.enums.YesOrNoEnum;
import com.cah.project.module.constant.ViewConstant;
import com.cah.project.module.standard.domain.vo.DictTypeInfo;
import com.cah.project.module.standard.domain.vo.in.DictEnableInVO;
import com.cah.project.module.standard.domain.vo.in.DictTypeQuery;
import com.cah.project.module.standard.domain.vo.out.DictTypeList;
import com.cah.project.module.standard.service.IDictTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collection;

/**
 * 功能描述: 字典类型 <br/>
 */
@RestController
@RequestMapping("/standard/dict/type")
@Api(tags = {"字典类型"})
public class DictTypeController {

    @Autowired
    private IDictTypeService dictTypeService;

    @GetMapping("main")
    public ModelAndView main() {
        return ModelViewUtil.stringView(ViewConstant.PATH_STD_DICT_MAIN);
    }

    @GetMapping("view")
    public ModelAndView view(Model model, String id) {
        DictTypeInfo info = dictTypeService.selectById(id);
        return ModelViewUtil.dataView(ViewConstant.PATH_STD_DICT_TYPE_VIEW, model, info);
    }

    @GetMapping("pageList")
    @ApiOperation(value="分页查询字典类型列表")
    public CommonPageResult<DictTypeList> typePageList(DictTypeQuery query){
        PageList<DictTypeList> pageList = dictTypeService.pageList(query);
        return CommonPageResult.data(pageList);
    }

    @PostMapping("save")
    @ApiOperation(value="保存字典类型数据")
    public CommonResult<String> save(@RequestBody DictTypeInfo info){
        dictTypeService.saveInfo(info);
        return CommonResult.ok();
    }

    @PutMapping("enable")
    @ApiOperation(value="保存字典类型数据")
    public CommonResult<String> typeEnable(@RequestBody DictEnableInVO inVO){
        dictTypeService.updateEnable(inVO.getId(), YesOrNoEnum.YES);
        return CommonResult.ok();
    }

    @PutMapping("disable")
    @ApiOperation(value="保存字典类型数据")
    public CommonResult<String> typeDisable(@RequestBody DictEnableInVO inVO){
        dictTypeService.updateEnable(inVO.getId(), YesOrNoEnum.NO);
        return CommonResult.ok();
    }

    @DeleteMapping("remove/{id}")
    public CommonResult<String> remove(@PathVariable("id") String id){
        dictTypeService.removeById(id);
        return CommonResult.ok();
    }

    @GetMapping("options")
    @ApiOperation(value = "获取字典类型下拉选项")
    public CommonResult<Collection<DictTypeOptions>> selectOptions() {
        return CommonResult.data(dictTypeService.selectOptions());
    }

}
