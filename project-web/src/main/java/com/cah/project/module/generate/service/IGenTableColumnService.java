package com.cah.project.module.generate.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cah.project.module.generate.domain.entity.GenTableColumnEntity;
import com.cah.project.module.generate.domain.vo.out.DbTableColumnList;

import java.util.List;

/**
 * 功能描述: 生成表字段定义 服务接口 <br/>
 */
public interface IGenTableColumnService extends IService<GenTableColumnEntity> {

    /**
     * 功能描述: 通过表名列表，查询数据库字段信息 <br/>
     *
     * @param tableNames 表明列表
     * @return "java.util.List<com.cah.project.module.generate.domain.vo.out.DbTableColumnList>"
     */
    List<DbTableColumnList> selectDbList(List<String> tableNames);

    /**
     * 功能描述: 通过定义表ID，获取字段列表 <br/>
     *
     * @param taleId 定义表ID
     * @return "com.cah.project.module.generate.domain.entity.GenTableColumnEntity"
     */
    List<GenTableColumnEntity> getByTableId(Long taleId);

}
