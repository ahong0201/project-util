package com.cah.project.module.meta.controller;

import com.cah.project.core.domain.out.CommonResult;
import com.cah.project.core.util.ModelViewUtil;
import com.cah.project.module.meta.domain.vo.in.DbTableInfoQuery;
import com.cah.project.module.meta.domain.vo.out.DbTableInfoList;
import com.cah.project.module.meta.service.ITableManageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Api(tags = {"表管理 控制层"})
@RestController
@RequestMapping("/meta/tableManage")
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class TableManageController {

    private final ITableManageService tableManageService;

    /** 元数据-表管理主页（列表） */
    private static final String PATH_META_TABLE_MANAGE_MAIN = "meta/table-manage/main";

    @GetMapping("main")
    public ModelAndView main() {
        return ModelViewUtil.stringView(PATH_META_TABLE_MANAGE_MAIN);
    }

    @GetMapping("selectAllList")
    @ApiOperation(value="分页查询列表")
    public CommonResult<List<DbTableInfoList>> selectAllList(DbTableInfoQuery query){
        List<DbTableInfoList> list = tableManageService.selectAllList(query);
        return CommonResult.data(list);
    }


}
