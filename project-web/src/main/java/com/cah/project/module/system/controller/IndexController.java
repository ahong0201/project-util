package com.cah.project.module.system.controller;

import com.cah.project.core.util.ModelViewUtil;
import com.cah.project.module.constant.ViewConstant;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 功能描述: 系统主页 <br/>
 */
@RestController
@RequestMapping("/index")
public class IndexController {

    @GetMapping("")
    @ApiOperation(value = "主页")
    public ModelAndView main() {
        return ModelViewUtil.stringView(ViewConstant.PATH_INDEX);
    }

}
