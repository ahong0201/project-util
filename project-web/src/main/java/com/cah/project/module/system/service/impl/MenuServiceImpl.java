package com.cah.project.module.system.service.impl;

import com.cah.project.module.system.data.MenuData;
import com.cah.project.module.system.domain.vo.out.MenuTree;
import com.cah.project.module.system.service.IMenuService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MenuServiceImpl implements IMenuService {

    @Override
    public List<MenuTree> getUserMenu() {
        return MenuData.getAllMenu();
    }

    @Override
    public List<MenuTree> getOneMenu() {
        List<MenuTree> oneTree = MenuData.getAllMenu().stream().filter(m -> "0".equals(m.getType())).collect(Collectors.toList());
        // 添加顶级菜单
        oneTree.add(MenuTree.builder().id("0").parentId("-1").title("顶级权限").build());
        return oneTree;
    }


}
