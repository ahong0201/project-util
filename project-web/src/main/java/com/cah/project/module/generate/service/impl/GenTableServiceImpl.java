package com.cah.project.module.generate.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cah.project.core.domain.out.PageList;
import com.cah.project.core.util.PageUtil;
import com.cah.project.module.generate.domain.converter.GenConverter;
import com.cah.project.module.generate.domain.entity.GenTableEntity;
import com.cah.project.module.generate.domain.vo.in.GenTableQuery;
import com.cah.project.module.generate.domain.vo.out.DbTableList;
import com.cah.project.module.generate.domain.vo.out.GenTableList;
import com.cah.project.module.generate.mapper.GenTableMapper;
import com.cah.project.module.generate.service.IGenTableService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 功能描述: 生成表定义 服务实现 <br/>
 */
@Service
public class GenTableServiceImpl extends ServiceImpl<GenTableMapper, GenTableEntity> implements IGenTableService {

    @Override
    public PageList<GenTableList> pageList(GenTableQuery query) {
        // 组转查询条件
        Wrapper<GenTableEntity> wrapper = Wrappers.<GenTableEntity>lambdaQuery()
                .like(StrUtil.isNotEmpty(query.getTableName()), GenTableEntity::getTableName, query.getTableName())
                .or()
                .like(StrUtil.isNotEmpty(query.getTableName()),GenTableEntity::getTableComment, query.getTableName())
                ;
        // 查询数据
        IPage<GenTableEntity> page = page(PageUtil.buildPage(query), wrapper);
        // 对象转换
        List<GenTableList> list = GenConverter.INSTANCE.toGenTableList(page.getRecords());
        return new PageList<>(PageUtil.buildPageInfo(page), list);
    }

    @Override
    public PageList<DbTableList> dbPageList(GenTableQuery query) {
        // 查询数据
        IPage<DbTableList> page = baseMapper.selectDbPageList(PageUtil.buildPage(query), query);
        return new PageList<>(PageUtil.buildPageInfo(page), page.getRecords());
    }

    @Override
    public List<DbTableList> selectDbList(List<String> tableNames) {
        if(CollectionUtil.isEmpty(tableNames)) {
            return ListUtil.list(false);
        }
        return baseMapper.selectDbList(tableNames);
    }

}
