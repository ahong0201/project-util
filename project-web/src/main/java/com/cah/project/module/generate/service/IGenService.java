package com.cah.project.module.generate.service;

import cn.hutool.core.io.IoUtil;
import com.cah.project.core.domain.out.PageList;
import com.cah.project.module.generate.domain.vo.GenTableColumnInfo;
import com.cah.project.module.generate.domain.vo.GenTableInfo;
import com.cah.project.module.generate.domain.vo.in.GenTableQuery;
import com.cah.project.module.generate.domain.vo.out.DbTableList;
import com.cah.project.module.generate.domain.vo.out.GenTableList;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

/**
 * 功能描述: 代码生成 服务接口 <br/>
 */
public interface IGenService {

    /**
     * 功能描述: 通过生成表定义ID，获取生成表详情 <br/>
     *
     * @param id 生成表定义ID
     * @return "com.cah.project.module.generate.domain.vo.GenTableInfo"
     */
    GenTableInfo getGenTableById(String id);

    /**
     * 功能描述:  通过生成表定义ID，获取生成表字段详情列表 <br/>
     *
     * @param tableId 生成表定义ID
     * @return "java.util.List<com.cah.project.module.generate.domain.vo.GenTableColumnInfo>"
     */
    List<GenTableColumnInfo> selectGenTableColumnInfoByTableId(Long tableId);

    /**
     * 功能描述: 分页查询 <br/>
     *
     * @param query 查询入参
     * @return "com.cah.project.core.domain.out.PageList<com.cah.project.module.generate.domain.vo.out.GenTableList>"
     */
    PageList<GenTableList> pageList(GenTableQuery query);

    /**
     * 功能描述: 分页查询数据当前数据库表列表 <br/>
     *
     * @param query 查询入参
     * @return "com.cah.project.core.domain.out.PageList<com.cah.project.module.generate.domain.vo.out.GenTableList>"
     */
    PageList<DbTableList> dbPageList(GenTableQuery query);

    /**
     * 功能描述: 保存数据 <br/>
     *
     * @param info 数据信息
     */
    void saveInfo(GenTableInfo info);

    /**
     * 功能描述: 保持引入的表定义数据，赋予默认值 <br/>
     *
     * @param tableNames 表名称组合，用“,”分隔
     */
    void saveImport(String tableNames);

    /**
     * 功能描述: 通过生成表定义ID列表，删除数据 <br/>
     *
     * @param tableIds 生成表定义ID列表
     */
    void deleteByIds(String tableIds);

    /**
     * 功能描述: 通过生成表定义ID，预览代码 <br/>
     *
     * @param tableId 生成表定义ID
     * @return "java.util.Map<java.lang.String,java.lang.String>"
     */
    Map<String, String> previewCode(Long tableId);

    /**
     * 功能描述: 生成代码文件 <br/>
     *
     * @param tableId 生成表定义ID
     */
    void genCode(Long tableId);

    /**
     * 功能描述: 打包成zip，并且返回二进制 <br/>
     *
     * @param tableId 生成表定义ID
     * @return "byte[]"
     */
    default byte[] downloadCode(String tableId) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(outputStream);
        genCode(Long.valueOf(tableId), zip);
        IoUtil.close(zip);
        return outputStream.toByteArray();
    }

    /**
     * 功能描述: 打包成zip <br/>
     *
     * @param tableId 生成表定义ID
     * @param zip zip流
     */
    void genCode(Long tableId, ZipOutputStream zip);

}
