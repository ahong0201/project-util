package com.cah.project.module.constant;

/**
 * 功能描述: 项目常量 <br/>
 */
public class ProjectConstant {

    /** 逗号-英文 */
    public static final String DOU_HAO_EN = ",";
    /** 逗号-中文 */
    public static final String DOU_HAO_CN = "，";
    /** 顿号 */
    public static final String DUN_HAO = "、";

    /**
     * UTF-8 编码格式
     * */
    public static String UTF8 ="UTF-8";


}
