package com.cah.project.exception;

import com.cah.project.core.domain.out.CommonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 功能描述: 控制层统一异常处理 <br/>
 */
@Slf4j
@ControllerAdvice
public class GlobalExceptionAdvice {

    public GlobalExceptionAdvice() {

    }

    @ResponseBody
    @ExceptionHandler({BusinessException.class})
    public CommonResult<String> exceptionHandle(BusinessException e) {
        String message = e.getMessage();
        return CommonResult.fail(message);
    }

}
