package com.cah.project.test.service;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

//@Component
@Service
public class AddCalcService {

    // 注入其他的服务，服务之间调用

    public BigDecimal calc(List<BigDecimal> list) {
        return list.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
    }

}
