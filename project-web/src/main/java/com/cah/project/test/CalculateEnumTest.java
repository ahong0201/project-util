package com.cah.project.test;

import cn.hutool.core.collection.ListUtil;
import com.cah.project.test.enums.CalculateEnum;

import java.math.BigDecimal;
import java.util.List;

public class CalculateEnumTest {

    public static void main(String[] args) {
        List<BigDecimal> datas = ListUtil.toList(BigDecimal.ONE, new BigDecimal(2), new BigDecimal(3));
        System.out.println(CalculateEnum.calculate("add", datas));
        System.out.println(CalculateEnum.calculate("Max", datas));
        System.out.println(CalculateEnum.calculate("MIN", datas));
        System.out.println(CalculateEnum.calculate("other", datas));
    }

}
