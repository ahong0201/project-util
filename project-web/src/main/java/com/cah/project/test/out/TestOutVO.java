package com.cah.project.test.out;

import com.cah.project.core.dict.annotation.Dict;
import com.cah.project.core.dict.enums.DictReEnum;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class TestOutVO implements Serializable {

    private String name;

    @Dict(type = "IND")
    private String status;

    @Dict(type = "SEX_TYPE", re = DictReEnum.DICT_DATA)
    private String sex;

    private List<ChildOutVO> children;

}
