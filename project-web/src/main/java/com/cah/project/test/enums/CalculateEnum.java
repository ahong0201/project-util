package com.cah.project.test.enums;

import cn.hutool.extra.spring.SpringUtil;
import com.cah.project.test.service.AddCalcService;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * 功能描述: 计算枚举 <br/>
 */
@Getter
@AllArgsConstructor
public enum CalculateEnum {

    ADD("求和", list -> SpringUtil.getBean(AddCalcService.class).calc(list)),
    MAX("最大", list -> list.stream().max(BigDecimal::compareTo).orElse(null)),
    MIN("最小", list -> list.stream().min(BigDecimal::compareTo).orElse(null)),
    OTHER("其他", list -> {
        System.out.println("自定义计算规则");
        return list.get(0).add(list.get(1));
    }),
    ;

    private final String deac;
    private final Function<List<BigDecimal>, BigDecimal> func;

    public static BigDecimal calculate(String type, List<BigDecimal> datas) {
        CalculateEnum ce = Stream.of(values()).filter(e -> e.name().equalsIgnoreCase(type)).findFirst().orElseThrow(NullPointerException::new);
        return ce.getFunc().apply(datas);
    }

    public BigDecimal calculate(List<BigDecimal> datas) {
        return getFunc().apply(datas);
    }

}
