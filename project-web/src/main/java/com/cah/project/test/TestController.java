package com.cah.project.test;

import cn.hutool.core.collection.ListUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.cah.project.core.domain.out.CommonResult;
import com.cah.project.module.meta.domain.entity.DataSourceEntity;
import com.cah.project.test.out.ChildOutVO;
import com.cah.project.test.out.TestOutVO;
import com.cah.project.test.service.TestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>测试 控制层 </p> <br/>
 *
 * @author cah
 * @since 2021-10-06
 */
@Api(tags = {"测试 控制层"})
@RestController
@RequestMapping("/test")
@RequiredArgsConstructor(onConstructor_ = {@Lazy})
public class TestController {

    @Autowired
    private TestService testService;

    @GetMapping("test")
    @ApiOperation(value="测试接口")
    public CommonResult<String> test(@RequestParam("id") String id){
        Wrapper wrapper = null;
        if(!"1".equals(id)) {
            wrapper = Wrappers.<DataSourceEntity>lambdaQuery().eq(DataSourceEntity::getId, 1);
        }
        return CommonResult.ok();
    }

    @GetMapping("getById")
    @ApiOperation(value="测试获取数据")
    public CommonResult<TestOutVO> getById(@RequestParam("id") String id){
        TestOutVO outVO = new TestOutVO();
        outVO.setName("名称");
        outVO.setStatus("1");
        outVO.setSex("0,1");
        ChildOutVO c1 = ChildOutVO.builder().name("子1").type(1).sex("0").build();
        ChildOutVO c2 = ChildOutVO.builder().name("子2").type(2).sex("1").build();
        List<ChildOutVO> list = ListUtil.toList(c1, c2);
        outVO.setChildren(list);
        testService.update();
        return CommonResult.data(outVO);
    }

    @PostMapping("save")
    @ApiOperation(value="测试保存数据")
    public CommonResult<TestOutVO> save(@RequestBody TestOutVO in) {
        System.out.println(in);
        return CommonResult.data(in);
    }

}
