package com.cah.project.test;

public class LeetCodeTest {

    public static void main(String[] args) {
        System.out.println(longestCommonPrefix(new String[]{"ab","a"}));
    }

    public static String longestCommonPrefix(String[] strs) {
        if(strs == null || strs.length == 0) {
            return "";
        }
        // 以 数组的第一个字符串为判断示例
        int i = 0;
        while(i < strs[0].length()) {
            boolean all = true;
            // 遍历之后的数组元素
            for(int n = 1; n < strs.length; n++) {
                if(i >= strs[n].length() || strs[0].charAt(i) != strs[n].charAt(i)) {
                    all = false;
                    break;
                }
            }
            if(!all) {
                break;
            }
            i++;
        }
        return strs[0].substring(0, i);
    }

    public static String countAndSay(int n) {
        // 定义递归出口
        if(n == 1) {
            return "1";
        }
        // 递归调用，获取上一个结果
        String preStr = countAndSay(n - 1);
        // 定义返回字符
        StringBuilder sb = new StringBuilder();
        System.out.println(n);
        // 定义当前连续字符, 并初始化为第一个字符
        char current = preStr.charAt(0);
        // 连续出现字符计数
        int count = 0;
        for(int i = 0; i < preStr.length(); i++) {
            // 如果字符相同，则计数器自增
            if(current == preStr.charAt(i)) {
                count++;
            } else {
                // 出现不同字符，将计数器和当前字符添加到返回结果中，并且重置当前字符与计数器
                sb.append(count);
                sb.append(current);
                // 因为循环过后，i会自增，所以需要将计数器设置为1
                count = 1;
                current = preStr.charAt(i);
            }
        }
        // 最终，结束循环，再次添加
        sb.append(count);
        sb.append(current);
        return sb.toString();
    }

    public static int strStr(String haystack, String needle) {
        // 如果 主字符串为空，则返回-1
        if(haystack == null ) {
            return -1;
        }
        // 如果 子字符串为空，或者空字符串，则返回0
        if(needle == null || needle.length() == 0) {
            return 0;
        }
        // 如果 子字符串比主字符串长度长，则肯定不存在
        if(haystack.length() < needle.length()) {
            return -1;
        }
        // 定义 主字符串下标
        int h = 0;
        // 定义 子字符串下标
        int n = 0;
        // 如果 主字符串下标 小于 主字符串长度-子字符串长度，则结束
        int length = haystack.length() - needle.length() + 1;
        while(h < length) {
            // 遍历子字符串
            while(h < haystack.length() && n < needle.length()) {
                if(haystack.charAt(h) == needle.charAt(n)) {
                    // 如果相同，则同时右移
                    h++;
                    n++;
                } else {
                    // 否则 主字符串下标回到匹配的第一个位置，并且右移一位
                    h = h - n + 1;
                    n = 0;
                }
                if(n == needle.length()) {
                    return h - n;
                }
            }
        }
        return -1;
    }

    public static int myAtoi(String s) {
        if(s == null || s.trim().length() == 0) {
            return 0;
        }
        // 去掉空格
        s = s.trim();
        int index = 0; // 定义索引下标
        int result = 0; // 定义结果
        int current = 0; // 定义当前数字
        int sign = 1; // 定义符号位，默认为0，如果有+号，则为1，-号为-1
        // 获取第一个字符，是否为正号或负号，则下标右移
        if(s.charAt(index) == '-' || s.charAt(index) == '+') {
            // 如果是负号，则sign赋值为-1
            sign = s.charAt(index) == '-' ? -1 : 1;
            index++;
        }
        while(index < s.length()) {
            // 得到当前数字
            current = s.charAt(index) - '0';
            // 如果范围再 0-9之间，说明是数字，否则为其余符号
            if(current >= 0 && current <= 9) {
                // 越界判断
                if (result > Integer.MAX_VALUE / 10 || (result == Integer.MAX_VALUE / 10 && current > Integer.MAX_VALUE % 10)){
                    return sign == 1 ? Integer.MAX_VALUE : Integer.MIN_VALUE;
                } else{
                    result = result * 10 + current;
                    // 向右移动
                    index++;
                }
            } else {
                break;
            }
        }
        // 符号 乘以 结果
        return sign * result;
    }

}
