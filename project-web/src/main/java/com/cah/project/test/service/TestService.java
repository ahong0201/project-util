package com.cah.project.test.service;

import com.cah.project.core.dict.event.DictReloadPublishEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestService {

    @Autowired
    private DictReloadPublishEvent dictReloadPublishEvent;

    public void update() {
        dictReloadPublishEvent.publishEvent("SEX_TYPE");
        System.out.println("-----------------");
    }

}
