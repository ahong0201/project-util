package com.cah.project.test;

import com.cah.project.test.enums.BizCharEnum;

public class BizCharEnumTest {

    public static void main(String[] args) {
        String bizChar = "1";
        System.out.println("写法1：" + BizCharEnum.indexOf(bizChar).isNewBizChar());
        System.out.println("写法1：" + BizCharEnum.indexOf(bizChar).isChangeBizChar());
        System.out.println("---------------------------------------------------------");
        System.out.println("写法2：" + BizCharEnum.isNewBizChar(bizChar));
        System.out.println("写法2：" + BizCharEnum.isChangeBizChar(bizChar));
        System.out.println("---------------------------------------------------------");
        System.out.println("写法3：" + BizCharEnum.isNewBizChar2(bizChar));
        System.out.println("写法3：" + BizCharEnum.isChangeBizChar2(bizChar));
    }

}
