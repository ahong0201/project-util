package com.cah.project.test.out;

import com.cah.project.core.dict.annotation.Dict;
import com.cah.project.core.dict.enums.DictReEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChildOutVO {

    private String name;

    @Dict(type = "DATA_SOURCE", re = DictReEnum.DICT_DATA)
    private Integer type;

    @Dict(type = "SEX_TYPE")
    private String sex;

}
