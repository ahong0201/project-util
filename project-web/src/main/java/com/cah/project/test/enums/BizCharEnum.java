package com.cah.project.test.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 功能描述: 对公信贷流程-业务性质枚举 <br/>
 */
@Getter
@AllArgsConstructor
public enum BizCharEnum {

    DEFAULT("0", "非业务性质"),
    S1("1", "一般业务") {
        @Override
        public boolean isNewBizChar() {
            return true;
        }
    },
    S2("2", "组合贷款"){
        @Override
        public boolean isNewBizChar() {
            return true;
        }
    },
    S3("3", "循环额度"),
    S4("4", "担保变更"){
        @Override
        public boolean isChangeBizChar() {
            return true;
        }
    },
    S5("5", "期限调整"){
        @Override
        public boolean isChangeBizChar() {
            return true;
        }
    },
    S6("6", "变更借款人"){
        @Override
        public boolean isChangeBizChar() {
            return true;
        }
    },
    S7("7", "再融资"){
        @Override
        public boolean isNewBizChar() {
            return true;
        }
    },
    S8("8", "回收再贷"),
    S9("9", "展期"){
        @Override
        public boolean isChangeBizChar() {
            return true;
        }
    },
    S10("10", "表外业务修改"){
        @Override
        public boolean isChangeBizChar() {
            return true;
        }
    },
    S11("11", "贸易融资额度合同"),
    S12("12", "框架性额度合同变更"){
        @Override
        public boolean isChangeBizChar() {
            return true;
        }
    },
    S13("13", "单位委托贷款"){
        @Override
        public boolean isChangeBizChar() {
            return true;
        }
    },

    ;

    private final String code;
    private final String msg;

    /**
     * 功能描述: 判断是否为新增类业务性质 <br/>
     *
     * @return "boolean" 是-true；否-false
     */
    public boolean isNewBizChar() {
        return false;
    }

    /**
     * 功能描述: 判断是否为变更类业务性质 <br/>
     *
     * @return "boolean" 是-true；否-false
     */
    public boolean isChangeBizChar() {
        return false;
    }

    /**
     * 功能描述: 判断是否为新增类业务性质 <br/>
     *
     * @return "boolean" 是-true；否-false
     */
    public static boolean isNewBizChar(String code) {
        return S1.code.equals(code) || S2.getCode().equals(code) || S3.getCode().equals(code);
    }

    /**
     * 功能描述: 判断是否为变更类业务性质 <br/>
     *
     * @return "boolean" 是-true；否-false
     */
    public static boolean isChangeBizChar(String code) {
        return S4.code.equals(code) || S5.getCode().equals(code) || S6.getCode().equals(code)
                || S9.getCode().equals(code) || S10.getCode().equals(code) || S12.getCode().equals(code)
                || S13.getCode().equals(code);
    }

    /**
     * 功能描述: 判断是否为新增类业务性质 <br/>
     *
     * @return "boolean" 是-true；否-false
     */
    public static boolean isNewBizChar2(String code) {
        return indexOf(code).isNewBizChar();
    }

    /**
     * 功能描述: 判断是否为变更类业务性质 <br/>
     *
     * @return "boolean" 是-true；否-false
     */
    public static boolean isChangeBizChar2(String code) {
        return indexOf(code).isChangeBizChar();
    }

    /** 如果枚举的内容有很多，可以这样缓存（注意：如果这里出现错误，启动不会报错，但是运行时会出现NoClassException。） */
    private final static Map<String, BizCharEnum> map = Stream.of(values()).collect(Collectors.toMap(BizCharEnum::getCode, e->e));

    /** 内容不是很多，直接使用for循环即可 */
    public static BizCharEnum indexOf(String code) {
        return map.getOrDefault(code, DEFAULT);
    }

    /** for 循环写法 */
    public static BizCharEnum indexOf2(String code) {
        for(BizCharEnum e : values()) {
            if(e.getCode().equals(code)) {
                return e;
            }
        }
        return DEFAULT;
    }

}
