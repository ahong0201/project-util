package com.cah.project.conf;

import com.cah.project.conf.property.SwaggerProperty;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

/**
 * 功能描述: 接口文档配置文件 <br/>
 */
@Slf4j
@Configuration
@EnableSwagger2
@EnableConfigurationProperties(SwaggerProperty.class)
public class SwaggerConfig {

    @Autowired
    private SwaggerProperty documentAutoProperties;

    @Bean
    public Docket docker(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName(documentAutoProperties.getGroupName())
                .enable(documentAutoProperties.getEnable())
                .select()
                .apis(RequestHandlerSelectors.basePackage(documentAutoProperties.getScanPackage()))
                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .build();
    }

    private ApiInfo apiInfo(){
        return new ApiInfo(
                documentAutoProperties.getTitle(),
                documentAutoProperties.getDescribe() ,
                documentAutoProperties.getVersion(),
                documentAutoProperties.getTermsOfServiceUrl(),
                documentAutoProperties.getContact(),
                documentAutoProperties.getLicence(),
                documentAutoProperties.getLicenceUrl(),
                new ArrayList<>()
        );
    }

}