package com.cah.project.conf.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import springfox.documentation.service.Contact;

/**
 * 功能描述: 接口文档配置类 <br/>
 */
@Data
@ConfigurationProperties("project.swagger")
public class SwaggerProperty {

    /** 是否开启 */
    private Boolean enable;

    /** 分组名称 */
    private String groupName;

    /** 系统标题 */
    private String title;

    /** 描述信息 */
    private String describe;

    /** 版本信息 */
    private String version;

    /** 扫描路径 */
    private String scanPackage;

    /** 扩展信息 */
    private Contact contact;

    /** 协议 */
    private String licence;

    /** 协议链接 */
    private String licenceUrl;

    /** 组织链接 */
    private String termsOfServiceUrl;

}