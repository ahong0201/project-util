package com.cah.project.conf;

import cn.hutool.core.collection.ListUtil;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 功能描述: 读取代码相关配置 <br/>
 */
@Data
@Component
@ConfigurationProperties(prefix = "gen")
@PropertySource(value = {"classpath:generate/generate.yml"})
public class GenConfig {

    /** 作者 **/
    public static String author;

    /** 生成包路径 **/
    public static String packageName;

    /** 模块名 **/
    public static String moduleName;

    /** 自动去除表前缀，默认是false **/
    public static boolean autoRemovePre;

    /** 表前缀(类名不会包含表前缀) **/
    public static String tablePrefix;

    /** 系统字段(中间不能又空格) */
    public static List<String> sysFields;

    /** 自动去除表前缀，默认是false **/
    public static String genPath;

    /** 是否swagger **/
    public static boolean swagger;

    public static String getAuthor() {
        return author;
    }

    @Value("${author}")
    public void setAuthor(String author) {
        GenConfig.author = author;
    }

    public static String getPackageName() {
        return packageName;
    }

    @Value("${moduleName}")
    public void setModuleName(String moduleName) {
        GenConfig.moduleName = moduleName;
    }

    public static String getModuleName() {
        return (moduleName == null || "".equals(moduleName)) ? GenConfig.tablePrefix.replace("_", "") : moduleName;
    }

    @Value("${packageName}")
    public void setPackageName(String packageName) {
        GenConfig.packageName = packageName;
    }

    public static boolean getAutoRemovePre() {
        return autoRemovePre;
    }

    @Value("${autoRemovePre}")
    public void setAutoRemovePre(boolean autoRemovePre) {
        GenConfig.autoRemovePre = autoRemovePre;
    }

    public static String getTablePrefix() {
        return tablePrefix;
    }

    @Value("${tablePrefix}")
    public void setTablePrefix(String tablePrefix) {
        GenConfig.tablePrefix = tablePrefix;
    }

    @Value("${sysField}")
    public void setSysFiled(String sysField) {
        GenConfig.sysFields = ListUtil.toList(sysField.split(","));
    }

    public static List<String> getSysFields() {
        return sysFields;
    }

    @Value("${genPath}")
    public void setGenPath(String genPath) {
        GenConfig.genPath = genPath;
    }

    public static String getGenPath() {
        return genPath;
    }

    @Value("${swagger}")
    public void setSwagger(String swagger) {
        GenConfig.swagger = "true".equals(swagger);
    }

    public static boolean getSwagger() {
        return swagger;
    }

}
