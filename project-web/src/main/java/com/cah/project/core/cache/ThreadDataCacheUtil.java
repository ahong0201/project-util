package com.cah.project.core.cache;

import cn.hutool.cache.Cache;
import cn.hutool.cache.CacheUtil;
import cn.hutool.core.thread.lock.LockUtil;

import java.util.concurrent.locks.Lock;

/**
 * 功能描述: 线程数据缓存工具 <br/>
 */
public class ThreadDataCacheUtil {

    /** 当前线程缓存 */
    private static final ThreadLocal<Cache<String, Object>> tl = new ThreadLocal<>();

    /** 默认超时时间 1分钟 */
    private static final long DEFAULT_TIME = 2 * 1000;

    /**
     * 功能描述: 获取缓存 <br/>
     * 如果当前线程没有，则创建
     * @param time 超时时间 毫秒
     * @return "cn.hutool.cache.Cache<java.lang.String,java.lang.Object>"
     */
    private static Cache<String, Object> get(long time) {
        Lock lock = LockUtil.getNoLock();
        try {
            lock.lock();
            if(tl.get() == null) {
                tl.set(CacheUtil.newTimedCache(time));
            }
        } finally {
            lock.unlock();
        }
        return tl.get();
    }

    /**
     * 功能描述: 清理缓存 <br/>
     */
    public static void remove() {
        tl.remove();
    }

    /**
     * 功能描述: 添加缓存 <br/>
     *
     * @param key key
     * @param obj Obj
     */
    public static void put(String key, Object obj) {
        get(DEFAULT_TIME).put(key, obj);
    }

    /**
     * 功能描述: 添加缓存 <br/>
     *
     * @param key key
     * @param obj Obj
     */
    public static void put(String key, Object obj, long time) {
        get(time).put(key, obj);
    }

    /**
     * 功能描述:  <br/>
     *
     * @param key key
     * @return "java.lang.Object"
     */
    public static Object get(String key) {
        return get(DEFAULT_TIME).get(key);
    }

}
