package com.cah.project;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 功能描述: 测试方法基类 <br/>
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebApplication.class)
public class BaseTest {

}
