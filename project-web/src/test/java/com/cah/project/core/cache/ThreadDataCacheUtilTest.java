package com.cah.project.core.cache;

import com.cah.project.BaseTest;
import com.cah.project.module.generate.domain.entity.GenTableColumnEntity;
import com.cah.project.module.generate.domain.entity.GenTableEntity;
import com.cah.project.module.generate.service.helper.GenDataCacheHelper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 功能描述: 线程缓存工具测试 <br/>
 *
 */
public class ThreadDataCacheUtilTest extends BaseTest {

    @Autowired
    private GenDataCacheHelper genDataCacheHelper;
    @Autowired
    private OtherService otherService;

    @Test
    public void cacheTest() throws InterruptedException {
        Long id = 14L;
        System.out.println("开始逻辑");
        // 创建缓存
        GenTableEntity genTableEntity = genDataCacheHelper.cacheGenTable(id);
        System.out.println("第一次表名----" + genTableEntity.getTableName());
        List<GenTableColumnEntity> genTableColumnEntityList = genDataCacheHelper.cacheGenTableColumn(id);
        System.out.println("第一次表字段====" + genTableColumnEntityList.size());
        // 调用其他服务
        System.out.println("----模拟其他逻辑使用场景----");
        otherService.exec(id);
        // 休眠5秒，模拟数据到期清理
        Thread.sleep(5000);
        // 再次获取
        System.out.println("-------------------------再次获取-------------------------");
        otherService.exec(id);
        ThreadDataCacheUtil.remove();
    }

    public static void main(String[] args) {
        Thread t1 = new Thread("t1") {
            @Override
            public void run() {
                System.out.println("t1");
                ThreadDataCacheUtil.put("name", "123456==" + this.getName());
                try {
                    sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("111-->" + ThreadDataCacheUtil.get("name"));
            }
        };
        Thread t2 = new Thread("t2") {
            @Override
            public void run() {
                System.out.println("t2");
                ThreadDataCacheUtil.put("name", "123456==" + this.getName());
                try {
                    sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("222-->" + ThreadDataCacheUtil.get("name"));
            }
        };
        t1.start();
        t2.start();
    }

}