package com.cah.project.core.allin;

import cn.hutool.core.convert.Convert;
import com.cah.project.core.allin.entity.BusinessEntity;
import com.googlecode.aviator.AviatorEvaluator;

import java.util.Map;

public class AllinTest {

    public static void main(String[] args) {
        BusinessEntity businessEntity = new BusinessEntity();
        businessEntity.setBizNum("123");
        Map<String, Object> map = Convert.toMap(String.class, Object.class, businessEntity);
        System.out.println(map);
        boolean result = (boolean) AviatorEvaluator.execute("bizNum != nil", map);
        System.out.println(result);
    }

}
