package com.cah.project.core.allin.entity;

import lombok.Data;

/**
 * 功能描述: 业务基本信息 <br/>
 */
@Data
public class BusinessEntity {

    private String bizNum;


}
