package com.cah.project.core.cache;

import com.cah.project.module.generate.domain.entity.GenTableColumnEntity;
import com.cah.project.module.generate.domain.entity.GenTableEntity;
import com.cah.project.module.generate.service.helper.GenDataCacheHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 功能描述: 模拟其他服务调用，需要使用到缓存 <br/>
 */
@Component
public class OtherService {

    @Autowired
    private GenDataCacheHelper genDataCacheHelper;

    public void exec(Long id) {
        GenTableEntity genTableEntity = genDataCacheHelper.getCacheGenTable(id);
        System.out.println("表名----" + genTableEntity.getTableName());
        List<GenTableColumnEntity> genTableColumnEntityList = genDataCacheHelper.getCacheGenTableColumn(id);
        System.out.println("字段长度====" + genTableColumnEntityList.size());

    }

}
