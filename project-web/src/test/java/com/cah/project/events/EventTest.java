package com.cah.project.events;

import cn.hutool.json.JSONUtil;

/**
 * <p> TODO </p>
 *
 * @author hongtool
 * @date 2023/3/1
 */
public class EventTest {
    
    public static void main(String[] args) {
        EventsBO bo1 = new EventsBO();
        bo1.setEventId("1");
        bo1.setEventType("1");
        EventData1 data1 = new EventData1();
        data1.setName1("name1");
        EventData11 data11 = new EventData11();
        data11.setName11("name11");
        data11.setType11("type11");
        data1.setData11(data11);
        bo1.setData(data1);
        String s1 = JSONUtil.toJsonStr(bo1);
        System.out.println("bo1---->" + s1);
        EventsBO bo2 = new EventsBO();
        bo2.setEventId("2");
        bo2.setEventType("2");
        EventData2 data2 = new EventData2();
        data2.setName2("name2");
        bo2.setData(data2);
        String s2 = JSONUtil.toJsonStr(bo2);
        System.out.println("bo2---->" + s2);
        
        // 开始解析
        EventsBO<EventData1> b1 = JSONUtil.toBean(s1, EventsBO.class);
        b1.setE(EventTypeEnum.indexOf(b1.getEventType()).changeData(b1.getData()));
        System.out.println(b1);
    
        // 开始解析
        EventsBO<EventData2> b2 = JSONUtil.toBean(s2, EventsBO.class);
        b2.setE(EventTypeEnum.indexOf(b2.getEventType()).changeData(b2.getData()));
        System.out.println(b2);
    }
    
}
