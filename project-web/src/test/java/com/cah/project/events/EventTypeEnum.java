package com.cah.project.events;

import cn.hutool.json.JSONUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * <p> 事件类型 </p>
 *
 * @author hongtool
 * @date 2023/3/1
 */
@Getter
@AllArgsConstructor
public enum EventTypeEnum {
    DEFAULT("") {
        @Override
        public <E extends EventBaseData> E changeData(Object data) {
            return null;
        }
    },
    S1("1") {
        @Override
        public EventData1 changeData(Object data) {
            return JSONUtil.toBean(JSONUtil.toJsonStr(data), EventData1.class);
        }
    },
    S2("2") {
        @Override
        public EventData2 changeData(Object data) {
            return JSONUtil.toBean(JSONUtil.toJsonStr(data), EventData2.class);
        }
    },
    
    ;
    
    public abstract <E extends EventBaseData>  E changeData(Object data);
    
    private final String code;
    
    public static EventTypeEnum indexOf(String code) {
        return Stream.of(values()).filter(e -> e.getCode().equals(code)).findFirst().orElse(DEFAULT);
    }
    
}
