package com.cah.project.events;

import lombok.Data;

/**
 * <p> TODO </p>
 *
 * @author hongtool
 * @date 2023/3/1
 */
@Data
public class EventData1 extends EventBaseData  {
    
    private String name1;
    
    private EventData11 data11;
    
}
