package com.cah.project.events;

import lombok.Data;

/**
 * <p> TODO </p>
 *
 * @author hongtool
 * @date 2023/3/1
 */
@Data
public class EventsBO<E> {
    
    private String eventId;
    /** 事件类型 */
    private String eventType;
    /** 接收对象 */
    private Object data;
    /** 真实解析后的对象 */
    private E e;
    
}
