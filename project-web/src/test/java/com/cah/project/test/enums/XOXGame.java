package com.cah.project.test.enums;

import java.util.Scanner;
public class XOXGame {
    public static void main(String[] args) {
        // Create a 2D array to represent the game board
        char[][] board = new char[3][3];
        // Initialize the board with empty spaces
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = ' ';
            }
        }
        // Create a Scanner object to read user input
        Scanner scanner = new Scanner(System.in);
        // Create a boolean to track if the game is over
        boolean gameOver = false;
        // Create a variable to track whose turn it is
        char turn = 'X';
        // Create a variable to track the winner
        char winner = ' ';
        // Print the board
        printBoard(board);
        // Start the game loop
        while (!gameOver) {
            // Ask the user for their move
            System.out.println("Player " + turn + ", enter your move (row, column): ");
            int row = scanner.nextInt();
            int col = scanner.nextInt();
            // Place the user's move on the board
            board[row][col] = turn;
            // Print the board
            printBoard(board);
            // Check if the game is over
            if (checkWin(board, turn)) {
                gameOver = true;
                winner = turn;
            } else if (checkDraw(board)) {
                gameOver = true;
            }
            // Switch turns
            if (turn == 'X') {
                turn = 'O';
            } else {
                turn = 'X';
            }
        }
        // Print the winner
        if (winner == 'X' || winner == 'O') {
            System.out.println("Player " + winner + " wins!");
        } else {
            System.out.println("It's a draw!");
        }
    }
    
    // Method to print the board
    public static void printBoard(char[][] board) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
    }
    
    // Method to check if a player has won
    public static boolean checkWin(char[][] board, char turn) {
        // Check rows
        for (int i = 0; i < 3; i++) {
            if (board[i][0] == turn && board[i][1] == turn && board[i][2] == turn) {
                return true;
            }
        }
        // Check columns
        for (int i = 0; i < 3; i++) {
            if (board[0][i] == turn && board[1][i] == turn && board[2][i] == turn) {
                return true;
            }
        }
        // Check diagonals
        if (board[0][0] == turn && board[1][1] == turn && board[2][2] == turn) {
            return true;
        }
        if (board[0][2] == turn && board[1][1] == turn && board[2][0] == turn) {
            return true;
        }
        return false;
    }
    
    // Method to check if the game is a draw
    public static boolean checkDraw(char[][] board) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == ' ') {
                    return false;
                }
            }
        }
        return true;
    }
}