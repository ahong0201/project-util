package com.cah.project.test.enums;

import cn.hutool.core.collection.ListUtil;
import com.cah.project.BaseTest;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

public class CalculateEnumTest extends BaseTest {

    @Test
    public void test() {
        List<BigDecimal> datas = ListUtil.toList(BigDecimal.ONE, new BigDecimal(2), new BigDecimal(3));
        System.out.println(CalculateEnum.calculate("add", datas));
        System.out.println(CalculateEnum.calculate("Max", datas));
        System.out.println(CalculateEnum.calculate("MIN", datas));
        System.out.println(CalculateEnum.calculate("other", datas));
        System.out.println(CalculateEnum.OTHER.calculate(datas));
    }

}