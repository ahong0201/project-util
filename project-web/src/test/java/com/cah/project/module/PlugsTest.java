package com.cah.project.module;


import com.cah.project.BaseTest;
import com.cah.project.PlugsContext;
import com.cah.project.module.meta.service.plugs.MetaPlugsGroup;
import com.cah.project.module.meta.service.plugs.tableManage.TableManageDataSourcePlug;
import org.junit.Test;

public class PlugsTest extends BaseTest {

    @Test
    public void test() {
        PlugsContext.exec("dataSource", "123");
        System.out.println("------------------------------------\n");
        PlugsContext.exec(MetaPlugsGroup.DATA_SOURCE, 456);
        System.out.println("------------------------------------\n");
        PlugsContext.exec(TableManageDataSourcePlug.class.getPackage().getName(), 123);
    }

}
