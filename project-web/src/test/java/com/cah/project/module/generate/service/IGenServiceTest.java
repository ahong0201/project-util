package com.cah.project.module.generate.service;


import com.cah.project.BaseTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class IGenServiceTest extends BaseTest {

    @Autowired
    private IGenService genService;

    @Test
    public void saveImport() {
        genService.saveImport("std_ci_hai,sys_dict_type");
    }
}