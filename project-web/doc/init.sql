CREATE TABLE `std_ci_hai` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `cn_name` varchar(255) NOT NULL COMMENT '中文名称',
  `en_full_name` varchar(255) NOT NULL COMMENT '英文全称',
  `en_name` varchar(255) DEFAULT NULL COMMENT '英文简称',
  `cn_describe` varchar(255) DEFAULT NULL COMMENT '中文描述',
  PRIMARY KEY (`id`),
  KEY `idx_cn_name` (`cn_name`,`cn_describe`),
  KEY `idx_en_name` (`en_full_name`,`en_name`)
) ENGINE=InnoDB COMMENT='辞海（命名规范）';


CREATE TABLE `gen_table` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `table_name` varchar(200) NOT NULL COMMENT '表名称',
  `table_comment` varchar(500) NOT NULL COMMENT '表描述',
  `sub_table_name` varchar(64) DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) DEFAULT NULL COMMENT '本表关联父表的字段名',
  `prefix` varchar(50) DEFAULT NULL COMMENT '表前缀',
  `class_name` varchar(100) DEFAULT NULL COMMENT '实体类名称（去除前缀）',
  `tpl_category` varchar(8) DEFAULT NULL COMMENT '使用的模板：crud-单表操作；tree-树表操作；sub-主子表操作',
  `package_name` varchar(100) DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` tinyint unsigned DEFAULT NULL COMMENT '生成代码方式：0-zip压缩包；1-自定义路径',
  `gen_path` varchar(200) DEFAULT NULL COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) DEFAULT NULL COMMENT '其它生成选项',
  `parent_menu_id` bigint DEFAULT NULL COMMENT '父级菜单ID',
  `parent_menu_name` varchar(255) DEFAULT NULL COMMENT '父级菜单名称',
  `remark` varchar(64) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `is_delete` tinyint unsigned DEFAULT '0' COMMENT '是否删除：1-是；0-否',
  PRIMARY KEY (`id`),
  KEY `idx_name` (`table_name`,`table_comment`)
) ENGINE=InnoDB COMMENT='生成表';

CREATE TABLE `gen_table_column` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `table_id` varchar(64) DEFAULT NULL COMMENT '归属表ID',
  `column_name` varchar(200) DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` unsigned unsigned DEFAULT NULL COMMENT '是否主键：1-是；0-否',
  `pk_type` unsigned unsigned  DEFAULT NULL COMMENT '主键类型：0-自增；1-未设置（跟随全局）；2-用户输入；3-雪花算法；4-UUID；5-全局唯一字符串',
  `is_required` unsigned unsigned  DEFAULT NULL COMMENT '是否必填：1-是；0-否',
  `is_insert` unsigned unsigned  DEFAULT '0' COMMENT '是否为插入字段：1-是；0-否',
  `is_edit` unsigned unsigned  DEFAULT '0' COMMENT '是否编辑字段：1-是；0-否',
  `is_list` unsigned unsigned  DEFAULT '0' COMMENT '是否列表字段：1-是；0-否',
  `is_query` unsigned unsigned  DEFAULT '0' COMMENT '是否查询字段：1-是；0-否',
  `query_type` varchar(200) DEFAULT NULL COMMENT '查询方式：EQ-等于；NE-不等于；GT-大于；GTE-大于等于；LT-小于；LTE-小于等于；LIKE-模糊；BETWEEN-范围',
  `html_type` varchar(200) DEFAULT NULL COMMENT '显示类型：input-文本框；textarea-文本域；select-下拉框；checkbox-复选框；radio-单选框；datetime-日期控件；upload-上传',
  `dict_type` varchar(200) DEFAULT '' COMMENT '字典类型：查找字典表的dict_type',
  `sort` int unsigned DEFAULT NULL COMMENT '排序',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_table_id` (`table_id`)
) ENGINE=InnoDB COMMENT='生成表字段';


CREATE TABLE `std_dict_type` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `type_code` varchar(50) DEFAULT NULL COMMENT '字典类型编码',
  `type_name` varchar(255) DEFAULT NULL COMMENT '字典类型名称',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述',
  `enable` tinyint unsigned DEFAULT NULL COMMENT '是否启用：1-是；0-否',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB COMMENT='字典类型';

CREATE TABLE `std_dict_data` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `type_code` varchar(50) DEFAULT NULL COMMENT '所属字典类型=std_dict_type.type_code',
  `data_key` varchar(50) DEFAULT NULL COMMENT '字典键值',
  `data_label` char(20) DEFAULT NULL COMMENT '字典显示值',
  `data_value` char(100) DEFAULT NULL COMMENT '字典代码值',
  `parent_data_label` char(20) DEFAULT NULL COMMENT '父字典标签',
  `is_default` tinyint unsigned DEFAULT NULL COMMENT '是否默认：1-是；0-否',
  `enable` tinyint unsigned DEFAULT NULL COMMENT '是否启用：1-是；0-否',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `idx_type_code` (`type_code`)
) ENGINE=InnoDB COMMENT='字典数据';

CREATE TABLE `meta_data_source` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(255) NOT NULL COMMENT '连接名',
  `host` varchar(20) NOT NULL COMMENT '主机',
  `port` varchar(10) NOT NULL COMMENT '端口',
  `service_name` varchar(255) NOT NULL COMMENT '服务名',
  `type` varchar(255) NOT NULL COMMENT '数据库类型',
  `driver_class_name` varchar(255) NOT NULL COMMENT '驱动类名',
  `username` varchar(255) DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `other_param` varchar(255) DEFAULT NULL COMMENT '其他参数（连接url中问号"?"的后面内容）',
  PRIMARY KEY (`id`),
  KEY `idx_name` (`name`),
  KEY `idx_host` (`host`)
) ENGINE=InnoDB COMMENT='数据源';
