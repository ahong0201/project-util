package com.cah.project.module.logger.sql;

import com.cah.project.module.logger.sql.impl.MySQLDdlSql;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 功能描述: SQL执行ddl语句工厂 <br/>
 */
@Getter
@AllArgsConstructor
public enum DdlSqlFactory {

    MYSQL(new MySQLDdlSql()),
    ;

    private final IDdlSql ddl;

}
