package com.cah.project.module.logger.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cah.project.module.logger.domain.entity.LoggerInfoEntity;
import com.cah.project.module.logger.mapper.LoggerInfoMapper;
import com.cah.project.module.logger.service.ILoggerInfoService;
import org.springframework.stereotype.Service;

/**
 * 功能描述: 日志服务实现 <br/>
 */
@Service
public class LoggerInfoServiceImpl extends ServiceImpl<LoggerInfoMapper, LoggerInfoEntity> implements ILoggerInfoService {



}
