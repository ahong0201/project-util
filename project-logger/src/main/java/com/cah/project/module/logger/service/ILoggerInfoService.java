package com.cah.project.module.logger.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cah.project.module.logger.domain.entity.LoggerInfoEntity;
import org.springframework.scheduling.annotation.Async;

/**
 * 功能描述: 日志服务接口 <br/>
 */
public interface ILoggerInfoService extends IService<LoggerInfoEntity> {

    /**
     * 功能描述: 异步保存 <br/>
     *
     * @param info 日志信息
     */
    @Async
    default void saveAsync(LoggerInfoEntity info) {
        save(info);
    }

}
