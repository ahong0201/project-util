package com.cah.project.module.logger.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cah.project.module.logger.domain.entity.LoggerInfoEntity;

/**
 * 功能描述: 日志信息mapper <br/>
 */
public interface LoggerInfoMapper extends BaseMapper<LoggerInfoEntity> {

}
