package com.cah.project;

import cn.hutool.core.collection.CollUtil;
import com.cah.project.plugs.PlugsModel;
import com.cah.project.plugs.aware.PlusAware;

import java.util.List;

/**
 * 功能描述: 插件执行上下文 <br/>
 */
public class PlugsContext {

    /** 执行插件 */
    public static void exec(String group, Object... objects) {
        List<PlugsModel> list = PlusAware.getPlusAware().getPlugsModel(group);
        if(CollUtil.isNotEmpty(list)) {
            list.forEach(p -> p.getPlugsSlot().exec(objects));
        }
    }

}
