package com.cah.project.plugs;

import lombok.Data;

/**
 * 功能描述: 插件扫描存储对象 <br/>
 */
@Data
public class PlugsModel {

    /** 插件类 */
    private IPlugsSlot plugsSlot;

    /** 插件所属组 */
    private String group;

    /** 插件执行顺序 */
    private float order;

}
