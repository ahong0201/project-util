package com.cah.project.plugs;

/**
 * 功能描述: 插件接口 <br/>
 */
public interface IPlugsSlot {

    /** 是否执行插件 */
    default boolean isExec(Object... objects) {
        return true;
    }

    /** 业务处理逻辑 */
    void handler(Object... objects);

    /** 执行 */
    default void exec(Object... objects) {
        if(isExec(objects)) {
            handler(objects);
        }
    }

}
