package com.cah.project.plugs.annotation;

import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Component;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 功能描述: 插件注释，用于定位是插件类 <br/>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Inherited
@Component
public @interface PlugsSlot {

    /** 插件名称 */
    @AliasFor(annotation = Component.class)
    String value() default "";

    /** 插件组 */
    String group() default "";

    /** 插件执行顺序 */
    float order() default 0F;

}
