package com.cah.project.enums;

import com.cah.project.core.dict.annotation.DictType;
import com.cah.project.core.dict.enums.IDictEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 功能描述: 是否枚举 <br/>
 *
 */
@Getter
@AllArgsConstructor
@DictType(typeCode = "IND", typeName = "是否")
public enum YesOrNoEnum implements IDictEnum {


    YES(1,"1", "是"),
    NO(0, "0", "否"),
    ;

    private final Integer index;
    private final String code;
    private final String msg;

    /**
     * 功能描述: 判断是否为真 <br/>
     *
     * @param code 码值
     * @return "boolean"
     */
    public static boolean judge(String code) {
        return YES.getCode().equals(code);
    }

    /**
     * 功能描述: 判断是否为真 <br/>
     *
     * @param index 码值
     * @return "boolean"
     */
    public static boolean judge(Integer index) {
        return YES.getIndex().equals(index);
    }

}
