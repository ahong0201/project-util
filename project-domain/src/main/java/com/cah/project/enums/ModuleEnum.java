package com.cah.project.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 功能描述: 模块枚举 <br/>
 *
 */
@Getter
@AllArgsConstructor
public enum ModuleEnum {

    SYSTEM("system", "系统管理"),
    STANDARD("standard", "数据标准"),
    ;

    private final String code;
    private final String msg;

}
