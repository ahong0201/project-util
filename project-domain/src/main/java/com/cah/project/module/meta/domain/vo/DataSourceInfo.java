package com.cah.project.module.meta.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>数据源 表单详情 </p> <br/>
 *
 * @author cah
 * @since 2021-10-06
 */
@Data
@ApiModel(value = "DataSourceInfo", description = "数据源 表单详情")
public class DataSourceInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键ID")
    private String id;

    @ApiModelProperty("连接名")
    private String name;

    @ApiModelProperty("主机")
    private String host;

    @ApiModelProperty("端口")
    private String port;

    @ApiModelProperty("服务名")
    private String serviceName;

    @ApiModelProperty("数据库类型")
    private String type;

    @ApiModelProperty("驱动类名")
    private String driverClassName;

    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("密码")
    private String password;

    @ApiModelProperty("其他参数（连接url中问号'?'的后面内容）")
    private String otherParam;

}
