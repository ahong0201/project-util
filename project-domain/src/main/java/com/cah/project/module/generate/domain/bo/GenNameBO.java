package com.cah.project.module.generate.domain.bo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "GenTablePackageBO", description = "代码生成表包名对象")
public class GenNameBO {

    /** 实体类 */
    private NameModelBO entity;
    /** 表单实体 */
    private NameModelBO infoEntity;
    /** 查询实体 */
    private NameModelBO queryEntity;
    /** 列表实体 */
    private NameModelBO outListEntity;
    /** 转换器 */
    private NameModelBO converter;
    /** 控制层 */
    private NameModelBO controller;
    /** 服务接口 */
    private NameModelBO service;
    /** 服务实现 */
    private NameModelBO serviceImpl;
    /** 读服务接口 */
    private NameModelBO serviceRead;
    /** 读服务实现 */
    private NameModelBO serviceReadImpl;
    /** 写服务接口 */
    private NameModelBO serviceWrite;
    /** 写服务实现 */
    private NameModelBO serviceWriteImpl;
    /** 帮助者 */
    private NameModelBO helper;
    /** 校验者 */
    private NameModelBO checker;
    /** 远程调用 */
    private NameModelBO remote;
    /** 远程调用入参对象 */
    private NameModelBO remoteInBo;
    /** 远程调用出参对象 */
    private NameModelBO remoteOutBo;
    /** javaMapper */
    private NameModelBO javaMapper;
    /** xmlMapper */
    private NameModelBO xmlMapper;

    /**
     * 功能描述: 命名模块 <br/>
     */
    @Data
    public static class NameModelBO {
        /** 类名：UserEntity */
        private String className;
        /** 类属性名：userEntity */
        private String clazzName;
        /** 类的包名：com.cah.project.module.generate.domain.entity */
        private String classPackage;
        /** 类完整的包名：com.cah.project.module.generate.domain.entity.UserEntity */
        private String classFullPackage;
    }

}
