package com.cah.project.module.standard.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cah.project.core.domain.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 辞海（命名规范） <br/>
 * @author cah
 * @since 2021-0-0
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("辞海（命名规范）")
@TableName("std_ci_hai")
public class CiHaiEntity extends BaseEntity {

    @ApiModelProperty("中文名称")
    @TableField("cn_name")
    private String cnName;

    @ApiModelProperty("英文全称")
    @TableField("en_full_name")
    private String enFullName;

    @ApiModelProperty("英文简称")
    @TableField("en_name")
    private String enName;

    @ApiModelProperty("中文描述")
    @TableField("cn_describe")
    private String cnDescribe;

}
