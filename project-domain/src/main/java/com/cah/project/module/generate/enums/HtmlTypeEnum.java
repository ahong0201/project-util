package com.cah.project.module.generate.enums;

import com.cah.project.core.dict.annotation.DictType;
import com.cah.project.core.dict.enums.IDictEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 功能描述: HTML控件类型枚举  <br/>
 */
@Getter
@AllArgsConstructor
@DictType(typeCode = "HTML_TYPE", typeName = "控件类型")
public enum HtmlTypeEnum implements IDictEnum {

    INPUT("input", "文本框"),
    TEXTAREA("textarea", "文本域"),
    SELECT("select", "下拉框"),
    RADIO("radio", "单选框"),
    CHECKBOX("checkbox", "复选框"),
    DATETIME("datetime", "日期控件"),
    UPLOAD("upload", "上传控件"),
    ;

    private final String code;
    private final String msg;

}
