package com.cah.project.module.generate.enums;

import com.cah.project.core.dict.annotation.DictType;
import com.cah.project.core.dict.enums.IDictEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 功能描述: 查询方式 <br/>
 */
@Getter
@AllArgsConstructor
@DictType(typeCode = "QUERY_TYPE", typeName = "查询方式")
public enum QueryTypeEnum implements IDictEnum {

    EQ("EQ", "等于"),
    NE("NE", "不等于"),
    GT("GT", "大于"),
    GTE("GTE", "大于等于"),
    LT("LT", "小于"),
    LTE("LTE", "小于等于"),
    LIKE("LIKE", "模糊"),
    BETWEEN("BETWEEN", "范围"),
    ;

    private final String code;
    private final String msg;

}
