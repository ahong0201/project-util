package com.cah.project.module.standard.domain.vo.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>字典数据 列表出参 </p> <br/>
 *
 * @author cah
 * @since 2022-03-02
 */
@Data
@ApiModel(value = "DictDataList", description = "字典数据 列表出参")
public class DictDataList implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键ID")
    private Long id;

    @ApiModelProperty("字典键值")
    private String dataKey;

    @ApiModelProperty("字典显示值")
    private String dataLabel;

    @ApiModelProperty("字典代码值")
    private String dataValue;

    @ApiModelProperty("是否启用")
    private Integer enable;

}