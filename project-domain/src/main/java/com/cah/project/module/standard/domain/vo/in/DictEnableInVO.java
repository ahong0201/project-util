package com.cah.project.module.standard.domain.vo.in;

import com.cah.project.core.domain.in.IdInVO;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "DictEnableInVO", description = "字典状态修改入参")
public class DictEnableInVO extends IdInVO {


}
