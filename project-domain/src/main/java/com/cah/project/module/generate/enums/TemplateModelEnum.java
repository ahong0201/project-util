package com.cah.project.module.generate.enums;

import cn.hutool.core.util.StrUtil;
import com.cah.project.module.generate.domain.bo.GenNameBO.NameModelBO;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 功能描述: 模板模块 <br/>
 */
@Getter
@AllArgsConstructor
public enum TemplateModelEnum {

    ENTITY("Entity", "实体") {
        @Override
        public String getFixed() {
            return "domain.entity";
        }
    },
    INFO_ENTITY("Info", "表单实体") {
        @Override
        public String getFixed() {
            return "domain.vo";
        }
    },
    QUERY_ENTITY("Query", "查询实体") {
        @Override
        public String getFixed() {
            return "domain.vo.in";
        }
    },
    OUT_LIST_ENTITY("List", "列表实体") {
        @Override
        public String getFixed() {
            return "domain.vo.out";
        }
    },
    CONVERTER("Converter", "转换器") {
        @Override
        public String getFixed() {
            return "domain.converter";
        }
    },
    CONTROLLER("Controller", "控制层") {
        @Override
        public String getFixed() {
            return "controller";
        }
    },
    SERVICE("Service", "服务接口") {
        @Override
        public String getFixed() {
            return "service";
        }
        @Override
        public String getPrefix() {
            return "I";
        }
    },
    SERVICE_IMPL("ServiceImpl", "服务实现") {
        @Override
        public String getFixed() {
            return "service.impl";
        }
    },
    SERVICE_READ("ServiceRead", "读服务接口") {
        @Override
        public String getFixed() {
            return "service";
        }
        @Override
        public String getPrefix() {
            return "I";
        }
    },
    SERVICE_READ_IMPL("ServiceReadImpl", "读服务实现") {
        @Override
        public String getFixed() {
            return "service.impl";
        }
    },
    SERVICE_WRITE("ServiceWrite", "写服务接口") {
        @Override
        public String getFixed() {
            return "service";
        }
        @Override
        public String getPrefix() {
            return "I";
        }
    },
    SERVICE_WRITE_IMPL("ServiceWriteImpl", "写服务实现") {
        @Override
        public String getFixed() {
            return "service.impl";
        }
    },
    HELPER("Helper", "服务帮助者") {
        @Override
        public String getFixed() {
            return "service.helper";
        }
    },
    CHECKER("Checker", "服务校验者") {
        @Override
        public String getFixed() {
            return "service.checker";
        }
    },
    REMOTE("Remote", "远程调用") {
        @Override
        public String getFixed() {
            return "remote";
        }
    },
    REMOTE_IN_BO("InBo", "远程调用入参") {
        @Override
        public String getFixed() {
            return "remote.bo.in";
        }
    },
    REMOTE_OUT_BO("OutBo", "远程调用出参") {
        @Override
        public String getFixed() {
            return "remote.bo.out";
        }
    },
    JAVA_MAPPER("Mapper", "mapper") {
        @Override
        public String getFixed() {
            return "mapper";
        }
    },

    XML_MAPPER("Mapper", "mapper") {
        @Override
        public String getFixed() {
            return "";
        }
    },
    ;

    /**
     * 功能描述: 所在包 <br/>
     *
     * @return "java.lang.String"
     */
    public abstract String getFixed();

    /**
     * 功能描述: 获取前缀 <br/>
     */
    public String getPrefix() {
        return "";
    }

    /**
     * 功能描述: 获取后缀 <br/>
     */
    public String getSuffix() {
        return "";
    }

    public NameModelBO getInstance(String packageFullName, String className) {
        NameModelBO bo = new NameModelBO();
        bo.setClassName(getPrefix() + className + getCode() + getSuffix());
        // 首字母小写
        bo.setClazzName(StrUtil.lowerFirst(StrUtil.subAfter(bo.getClassName(), getPrefix(), false)));
        bo.setClassPackage(packageFullName + "." + getFixed());
        // 全路径
        bo.setClassFullPackage(bo.getClassPackage() + "." + bo.getClassName());
        return bo;
    }

    private final String code;
    private final String msg;

}
