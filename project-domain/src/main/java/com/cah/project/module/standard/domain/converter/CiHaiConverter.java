package com.cah.project.module.standard.domain.converter;

import com.cah.project.core.domain.converter.BaseConverter;
import com.cah.project.module.standard.domain.entity.CiHaiEntity;
import com.cah.project.module.standard.domain.vo.CiHaiInfo;
import com.cah.project.module.standard.domain.vo.out.CiHaiList;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 功能描述: 辞海 对象转换器 <br/>
 */
@Mapper
public interface CiHaiConverter extends BaseConverter {

    CiHaiConverter INSTANCE = Mappers.getMapper(CiHaiConverter.class);


    default List<CiHaiList> toCiHaiList(List<CiHaiEntity> es) {
        return list2List(es, this::toCiHaiList);
    }

    CiHaiList toCiHaiList(CiHaiEntity e);

    CiHaiInfo toCiHaiInfo(CiHaiEntity entity);

    CiHaiEntity toCiHaiEntity(CiHaiInfo info);
}
