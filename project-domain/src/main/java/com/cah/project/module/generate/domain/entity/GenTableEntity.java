package com.cah.project.module.generate.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cah.project.core.domain.entity.BaseCommonEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 功能描述: 生成表定义 实体 <br/>
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("生成表定义")
@TableName("gen_table")
public class GenTableEntity extends BaseCommonEntity {

    @ApiModelProperty("表名称")
    @TableField("table_name")
    private String tableName;

    @ApiModelProperty("表描述")
    @TableField("table_comment")
    private String tableComment;

    @ApiModelProperty("关联父表的表名")
    @TableField("sub_table_name")
    private String subTableName;

    @ApiModelProperty("本表关联父表的字段名")
    @TableField("sub_table_fk_name")
    private String subTableFkName;

    @ApiModelProperty("表前缀")
    @TableField("prefix")
    private String prefix;

    @ApiModelProperty("实体类名称（去除前缀）")
    @TableField("class_name")
    private String className;

    @ApiModelProperty("使用的模板：crud-单表操作；tree-树表操作；sub-主子表操作；自定义-与resources文件夹下的包对应")
    @TableField("tpl_category")
    private String tplCategory;

    @ApiModelProperty("生成包路径")
    @TableField("package_name")
    private String packageName;

    @ApiModelProperty("生成模块名")
    @TableField("module_name")
    private String moduleName;

    @ApiModelProperty("生成业务名")
    @TableField("business_name")
    private String businessName;

    @ApiModelProperty("生成功能名")
    @TableField("function_name")
    private String functionName;

    @ApiModelProperty("生成功能作者")
    @TableField("function_author")
    private String functionAuthor;

    @ApiModelProperty("生成代码方式：0-zip压缩包；1-自定义路径")
    @TableField("gen_type")
    private Integer genType;

    @ApiModelProperty("生成路径（不填默认项目路径）")
    @TableField("gen_path")
    private String genPath;

    @ApiModelProperty("其它生成选项")
    @TableField("options")
    private String options;

    @ApiModelProperty("父级菜单ID")
    @TableField("parent_menu_id")
    private String parentMenuId;

    @ApiModelProperty("父级菜单名称")
    @TableField("parent_menu_name")
    private String parentMenuName;

    @ApiModelProperty("是否删除：1-是；0-否")
    @TableField("is_delete")
    @TableLogic
    private Integer isDelete;

}
