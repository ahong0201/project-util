package com.cah.project.module.generate.domain.vo.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@ApiModel(value = "GenTableList", description = "代码生成表定义列表")
public class GenTableList implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键ID")
    private Long id;

    @ApiModelProperty("表名称")
    private String tableName;

    @ApiModelProperty("表描述")
    private String tableComment;

    @ApiModelProperty("实体类名称（去除前缀）")
    private String className;

    @ApiModelProperty("使用的模板：crud-单表操作；tree-树表操作；sub-主子表操作；自定义-与resources文件夹下的包对应")
    private String tplCategory;

    @ApiModelProperty("生成模块名")
    private String moduleName;

    @ApiModelProperty("生成代码方式：0-zip压缩包；1-自定义路径")
    private Integer genType;

    @ApiModelProperty("创建时间")
    private LocalDate createTime;

    @ApiModelProperty("修改时间")
    private LocalDate updateTime;

}
