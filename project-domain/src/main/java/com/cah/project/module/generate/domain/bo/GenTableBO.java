package com.cah.project.module.generate.domain.bo;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "GenTableBO", description = "代码生成表对象")
public class GenTableBO {

    @ApiModelProperty("表名称")
    private String tableName;

    @ApiModelProperty("表描述")
    private String tableComment;

    @ApiModelProperty("关联父表的表名")
    private String subTableName;

    @ApiModelProperty("本表关联父表的字段名")
    private String subTableFkName;

    @ApiModelProperty("表前缀")
    private String prefix;

    @ApiModelProperty("实体类名称（去除前缀）")
    private String className;

    @ApiModelProperty("使用的模板：crud-单表操作；tree-树表操作；sub-主子表操作；自定义-与resources文件夹下的包对应")
    private String tplCategory;

    @ApiModelProperty("生成包路径")
    private String packageName;

    @ApiModelProperty("生成模块名")
    private String moduleName;

    @ApiModelProperty("生成业务名")
    private String businessName;

    @ApiModelProperty("生成功能名")
    private String functionName;

    @ApiModelProperty("生成功能作者")
    private String functionAuthor;

    @ApiModelProperty("生成代码方式：0-zip压缩包；1-自定义路径")
    private Integer genType;

    @ApiModelProperty("生成路径（不填默认项目路径）")
    private String genPath;

    @ApiModelProperty("其它生成选项")
    private String options;

    @ApiModelProperty("父级菜单ID")
    private String parentMenuId;

    @ApiModelProperty("父级菜单名称")
    private String parentMenuName;

    @ApiModelProperty("命名模块")
    private GenNameBO genName;

    @ApiModelProperty("子表信息")
    private GenTableBO subTable;

    @ApiModelProperty("字段信息（排除公共字段）")
    private List<GenTableFieldBO> columns;

    @ApiModelProperty("公共字段信息（不包含主键）")
    private List<GenTableFieldBO> commonColumns;

    @ApiModelProperty("字段引入的包")
    private List<String> importPackages;

}
