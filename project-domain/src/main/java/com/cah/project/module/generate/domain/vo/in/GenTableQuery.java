package com.cah.project.module.generate.domain.vo.in;

import com.cah.project.core.domain.in.PageQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "GenTableQuery", description = "代码生成查询")
public class GenTableQuery extends PageQuery {

    @ApiModelProperty("表名称")
    private String tableName;

}
