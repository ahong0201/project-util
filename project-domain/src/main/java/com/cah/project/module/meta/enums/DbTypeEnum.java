package com.cah.project.module.meta.enums;

import cn.hutool.core.util.StrUtil;
import com.cah.project.core.dict.annotation.DictType;
import com.cah.project.core.dict.enums.IDictEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * 功能描述: 数据库类型 <br/>
 */
@Getter
@AllArgsConstructor
@DictType(typeCode = "DB_TYPE", typeName = "数据库类型")
public enum DbTypeEnum implements IDictEnum {

    MYSQL("mysql", "MySQL"),
    ORACLE("oracle", "Oracle"),
    ;
    private final String code;
    private final String msg;

    public static DbTypeEnum indexOf(String code) {
        if(StrUtil.isEmpty(code)) {
            return MYSQL;
        }
        return Stream.of(values()).filter(e -> code.equals(e.getCode())).findFirst().orElse(MYSQL);
    }

}
