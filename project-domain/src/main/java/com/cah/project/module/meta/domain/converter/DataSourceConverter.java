package com.cah.project.module.meta.domain.converter;

import com.cah.project.core.domain.converter.BaseConverter;
import com.cah.project.module.meta.domain.entity.DataSourceEntity;
import com.cah.project.module.meta.domain.vo.DataSourceInfo;
import com.cah.project.module.meta.domain.vo.out.DataSourceList;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * <p>数据源 对象转换器 </p> <br/>
 *
 * @author cah
 * @since 2021-10-06
 */
@Mapper
public interface DataSourceConverter extends BaseConverter {

    DataSourceConverter INSTANCE = Mappers.getMapper(DataSourceConverter.class);

    default List<DataSourceList> toDataSourceList(List<DataSourceEntity> list) {
        return list2List(list, this::toDataSourceList);
    }

    DataSourceList toDataSourceList(DataSourceEntity entity);

    DataSourceInfo toDataSourceInfo(DataSourceEntity entity);

    DataSourceEntity toDataSourceEntity(DataSourceInfo info);

}
