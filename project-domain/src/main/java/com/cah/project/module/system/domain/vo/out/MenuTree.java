package com.cah.project.module.system.domain.vo.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * 功能描述: 系统菜单树 <br/>
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "MenuTree", description = "系统菜单树")
public class MenuTree {

    @ApiModelProperty("菜单ID")
    private String id;

    @ApiModelProperty("父节点ID，根节点的父节点为0")
    private String parentId;

    @ApiModelProperty("标题")
    private String title;

    @ApiModelProperty("菜单类型")
    private String type;

    @ApiModelProperty("打开类型")
    private String openType;

    @ApiModelProperty("图标")
    private String icon;

    @ApiModelProperty("跳转路径")
    private String href;

    @ApiModelProperty("子菜单")
    private List<MenuTree> children = new ArrayList<>();

    @ApiModelProperty("用于参数传递")
    private String username;
}
