package com.cah.project.module.standard.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cah.project.core.domain.entity.BaseCommonEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 功能描述: 字典类型 <br/>
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("字典类型")
@TableName("std_dict_type")
public class DictTypeEntity extends BaseCommonEntity {

    @ApiModelProperty("字典类型编码")
    @TableField("type_code")
    private String typeCode;

    @ApiModelProperty("字典类型名称")
    @TableField("type_name")
    private String typeName;

    @ApiModelProperty("描述")
    @TableField("remark")
    private String remark;

    @ApiModelProperty("是否启用：1-是；0-否")
    @TableField("enable")
    private Integer enable;

}
