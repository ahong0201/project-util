package com.cah.project.module.meta.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>数据源 数据库实体 </p> <br/>
 *
 * @author cah
 * @since 2021-10-06
 */
@Data
@ApiModel(value = "DataSourceEntity", description = "数据源 数据库实体")
@TableName("meta_data_source")
public class DataSourceEntity {

    @ApiModelProperty("主键ID")
    @TableId(value = "id", type = IdType.AUTO)
    private String id;

    @ApiModelProperty("项目名称")
    @TableField(value = "name")
    private String name;

    @ApiModelProperty("主机")
    @TableField(value = "host")
    private String host;

    @ApiModelProperty("端口")
    @TableField(value = "port")
    private String port;

    @ApiModelProperty("数据库名称")
    @TableField(value = "service_name")
    private String serviceName;

    @ApiModelProperty("数据库类型")
    @TableField(value = "type")
    private String type;

    @ApiModelProperty("驱动类名")
    @TableField(value = "driver_class_name")
    private String driverClassName;

    @ApiModelProperty("用户名")
    @TableField(value = "username")
    private String username;

    @ApiModelProperty("密码")
    @TableField(value = "password")
    private String password;

    @ApiModelProperty("其他参数（连接url中问号'?'的后面内容）")
    @TableField(value = "other_param")
    private String otherParam;

}
