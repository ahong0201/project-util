package com.cah.project.module.standard.domain.vo.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>字典类型 列表出参 </p> <br/>
 *
 * @author cah
 * @since 2022-03-02
 */
@Data
@ApiModel(value = "DictTypeList", description = "字典类型 列表出参")
public class DictTypeList implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键ID")
    private Long id;

    @ApiModelProperty("字典类型标识")
    private String typeCode;

    @ApiModelProperty("字典类型名称")
    private String typeName;

    @ApiModelProperty("是否启用")
    private Integer enable;

}