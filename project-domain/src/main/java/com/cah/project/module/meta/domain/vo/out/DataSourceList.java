package com.cah.project.module.meta.domain.vo.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>数据源 列表出参 </p> <br/>
 *
 * @author cah
 * @since 2021-10-06
 */
@Data
@ApiModel(value = "DataSourceList", description = "数据源 列表出参")
public class DataSourceList implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键ID")
    private String id;

    @ApiModelProperty("连接名")
    private String name;

    @ApiModelProperty("主机")
    private String host;

    @ApiModelProperty("端口")
    private String port;

    @ApiModelProperty("服务名")
    private String serviceName;

    @ApiModelProperty("数据库类型")
    private String type;

    @ApiModelProperty("用户名")
    private String username;

}
