package com.cah.project.module.generate.domain.converter;

import com.cah.project.core.domain.converter.BaseConverter;
import com.cah.project.module.generate.domain.entity.GenTableColumnEntity;
import com.cah.project.module.generate.domain.entity.GenTableEntity;
import com.cah.project.module.generate.domain.vo.GenTableColumnInfo;
import com.cah.project.module.generate.domain.vo.GenTableInfo;
import com.cah.project.module.generate.domain.vo.out.GenTableList;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.Optional;

/**
 * 功能描述: 代码生成 对象转换器 <br/>
 */
@Mapper
public interface GenConverter extends BaseConverter {

    GenConverter INSTANCE = Mappers.getMapper(GenConverter.class);

    default List<GenTableList> toGenTableList(List<GenTableEntity> list) {
        return list2List(list, this::toGenTableList);
    }

    GenTableList toGenTableList(GenTableEntity entity);

    GenTableInfo toGenTableInfo(GenTableEntity entity);

    default List<GenTableColumnInfo> toGenTableColumnInfoList(List<GenTableColumnEntity> list) {
        return list2List(list, this::toGenTableColumnInfo);
    }

    GenTableColumnInfo toGenTableColumnInfo(GenTableColumnEntity entity);

    @Mappings(@Mapping(source = "id", target = "id"))
    GenTableEntity toGenTableEntity(GenTableInfo info);

    default List<GenTableColumnEntity> toGenTableColumnEntityList(List<GenTableColumnInfo> list) {
        return list2List(list, this::toGenTableColumnEntity);
    }

    default GenTableColumnEntity toGenTableColumnEntity(GenTableColumnInfo columnInfo) {
        GenTableColumnEntity genTableColumnEntity = new GenTableColumnEntity();
        if ( columnInfo.getId() != null ) {
            genTableColumnEntity.setId( columnInfo.getId() );
        }
        genTableColumnEntity.setColumnName( columnInfo.getColumnName() );
        genTableColumnEntity.setColumnComment( columnInfo.getColumnComment() );
        genTableColumnEntity.setColumnType( columnInfo.getColumnType() );
        genTableColumnEntity.setJavaType( columnInfo.getJavaType() );
        genTableColumnEntity.setJavaField( columnInfo.getJavaField() );
        genTableColumnEntity.setIsPk( columnInfo.getIsPk() );
        genTableColumnEntity.setPkType( columnInfo.getPkType() );
        genTableColumnEntity.setIsRequired(Optional.ofNullable(columnInfo.getIsRequired()).orElse(0) );
        genTableColumnEntity.setIsInsert(Optional.ofNullable(columnInfo.getIsInsert()).orElse(0) );
        genTableColumnEntity.setIsEdit(Optional.ofNullable(columnInfo.getIsEdit()).orElse(0) );
        genTableColumnEntity.setIsList(Optional.ofNullable(columnInfo.getIsList()).orElse(0) );
        genTableColumnEntity.setIsQuery(Optional.ofNullable(columnInfo.getIsQuery()).orElse(0) );
        genTableColumnEntity.setQueryType( columnInfo.getQueryType() );
        genTableColumnEntity.setHtmlType( columnInfo.getHtmlType() );
        genTableColumnEntity.setDictType( columnInfo.getDictType() );
        genTableColumnEntity.setSort( columnInfo.getSort() );
        return genTableColumnEntity;
    }

}
