package com.cah.project.module.meta.domain.vo.in;

import com.cah.project.core.domain.in.PageQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>数据源 查询入参 </p> <br/>
 *
 * @author cah
 * @since 2021-10-06
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "DataSourceQuery", description = "数据源 查询入参")
public class DataSourceQuery extends PageQuery {

    @ApiModelProperty("连接名")
    private String name;

}
