package com.cah.project.module.generate.domain.vo.out;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@ApiModel(value = "DbTableList", description = "数据库表列表")
public class DbTableList implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("表名称")
    @TableField("table_name")
    private String tableName;

    @ApiModelProperty("表描述")
    @TableField("table_comment")
    private String tableComment;

    @ApiModelProperty("创建时间")
    private LocalDate createTime;

    @ApiModelProperty("修改时间")
    private LocalDate updateTime;

}
