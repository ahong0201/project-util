package com.cah.project.module.standard.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>字典数据 表单详情 </p> <br/>
 *
 * @author cah
 * @since 2022-03-02
 */
@Data
@ApiModel(value = "DictDataInfo", description = "字典数据 表单详情")
public class DictDataInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键ID")
    private Long id;

    @ApiModelProperty("所属字典类型=std_dict_type.type_code")
    private String typeCode;

    @ApiModelProperty("字典类型描述")
    private String typeName;

    @ApiModelProperty("字典键值")
    private String dataKey;

    @ApiModelProperty("字典显示值")
    private String dataLabel;

    @ApiModelProperty("字典代码值")
    private String dataValue;

    @ApiModelProperty("父字典标签")
    private String parentDataLabel;

    @ApiModelProperty("是否默认：1-是；0-否")
    private Integer isDefault;

    @ApiModelProperty("是否启用：1-是；0-否")
    private Integer enable;

    @ApiModelProperty("备注")
    private String remark;

}