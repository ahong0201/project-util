package com.cah.project.module.generate.domain.vo.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "DbTableColumnList", description = "数据库表字段列表")
public class DbTableColumnList {

    @ApiModelProperty("表名称")
    private String tableName;

    @ApiModelProperty("列名称")
    private String columnName;

    @ApiModelProperty("列描述")
    private String columnComment;

    @ApiModelProperty("列类型")
    private String columnType;

    @ApiModelProperty("是否主键：1-是；0-否")
    private Integer isPk;

    @ApiModelProperty("主键类型：0-自增；1-未设置（跟随全局）；2-用户输入；3-雪花算法；4-UUID；5-全局唯一字符串")
    private Integer pkType;

    @ApiModelProperty("是否必填：1-是；0-否")
    private Integer isRequired;

    @ApiModelProperty("排序")
    private Integer sort;

}
