package com.cah.project.module.standard.domain.vo.in;

import com.cah.project.core.domain.in.PageQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>字典数据 查询入参 </p> <br/>
 *
 * @author cah
 * @since 2022-03-02
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "DictDataQuery", description = "字典数据 查询入参")
public class DictDataQuery extends PageQuery {

    @ApiModelProperty("字典类型编码")
    private String typeCode;

}