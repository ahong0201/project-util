package com.cah.project.module.standard.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cah.project.core.domain.entity.BaseCommonEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 功能描述: 字典数据 <br/>
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("字典数据")
@TableName("std_dict_data")
public class DictDataEntity extends BaseCommonEntity {

    @ApiModelProperty("所属字典类型=std_dict_type.type_code")
    @TableField("type_code")
    private String typeCode;

    @ApiModelProperty("字典键值")
    @TableField("data_key")
    private String dataKey;

    @ApiModelProperty("字典显示值")
    @TableField("data_label")
    private String dataLabel;

    @ApiModelProperty("字典代码值")
    @TableField("data_value")
    private String dataValue;

    @ApiModelProperty("父字典标签")
    @TableField("parent_data_label")
    private String parentDataLabel;

    @ApiModelProperty("是否默认：1-是；0-否")
    @TableField("is_default")
    private Integer isDefault;

    @ApiModelProperty("是否启用：1-是；0-否")
    @TableField("enable")
    private Integer enable;

    @ApiModelProperty("备注")
    @TableField("remark")
    private String remark;

}
