package com.cah.project.module.generate.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cah.project.core.domain.entity.BaseCommonEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 功能描述: 生成表字段定义 实体 <br/>
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("生成表字段定义")
@TableName("gen_table_column")
public class GenTableColumnEntity extends BaseCommonEntity {

    @ApiModelProperty("归属表ID")
    @TableField("table_id")
    private Long tableId;

    @ApiModelProperty("列名称")
    @TableField("column_name")
    private String columnName;

    @ApiModelProperty("列描述")
    @TableField("column_comment")
    private String columnComment;

    @ApiModelProperty("列类型")
    @TableField("column_type")
    private String columnType;

    @ApiModelProperty("JAVA类型")
    @TableField("java_type")
    private String javaType;

    @ApiModelProperty("JAVA字段名")
    @TableField("java_field")
    private String javaField;

    @ApiModelProperty("是否主键：1-是；0-否")
    @TableField("is_pk")
    private Integer isPk;

    @ApiModelProperty("主键类型：0-自增；1-未设置（跟随全局）；2-用户输入；3-雪花算法；4-UUID；5-全局唯一字符串")
    @TableField("pk_type")
    private Integer pkType;

    @ApiModelProperty("是否必填：1-是；0-否")
    @TableField("is_required")
    private Integer isRequired;

    @ApiModelProperty("是否为插入字段：1-是；0-否")
    @TableField("is_insert")
    private Integer isInsert;

    @ApiModelProperty("是否编辑字段：1-是；0-否")
    @TableField("is_edit")
    private Integer isEdit;

    @ApiModelProperty("是否列表字段：1-是；0-否")
    @TableField("is_list")
    private Integer isList;

    @ApiModelProperty("是否查询字段：1-是；0-否")
    @TableField("is_query")
    private Integer isQuery;

    @ApiModelProperty("查询方式：EQ-等于；NE-不等于；GT-大于；GTE-大于等于；LT-小于；LTE-小于等于；LIKE-模糊；BETWEEN-范围")
    @TableField("query_type")
    private String queryType;

    @ApiModelProperty("显示类型：input-文本框；textarea-文本域；select-下拉框；checkbox-复选框；radio-单选框；datetime-日期控件；upload-上传")
    @TableField("html_type")
    private String htmlType;

    @ApiModelProperty("字典类型：查找字典表的dict_type")
    @TableField("dict_type")
    private String dictType;

    @ApiModelProperty("排序")
    @TableField("sort")
    private Integer sort;

}
