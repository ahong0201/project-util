package com.cah.project.module.standard.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "CiHaiInfo", description = "辞海详情")
public class CiHaiInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键ID")
    private Long id;

    @ApiModelProperty("中文名称")
    private String cnName;

    @ApiModelProperty("英文全称")
    private String enFullName;

    @ApiModelProperty("英文简称")
    private String enName;

    @ApiModelProperty("中文描述")
    private String cnDescribe;
}
