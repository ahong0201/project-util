package com.cah.project.module.meta.domain.vo.in;

import lombok.Data;

import java.io.Serializable;

@Data
public class DbTableInfoQuery implements Serializable {

    private static final long serialVersionUID = 1L;

    private String dataSourceId;

}
