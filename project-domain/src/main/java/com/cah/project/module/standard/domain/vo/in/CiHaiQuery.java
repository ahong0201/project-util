package com.cah.project.module.standard.domain.vo.in;

import com.cah.project.core.domain.in.PageQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "CiHaiQuery", description = "辞海查询")
public class CiHaiQuery extends PageQuery {

    @ApiModelProperty("中文名称")
    private String cnName;

    @ApiModelProperty("英文名称")
    private String enName;

}
