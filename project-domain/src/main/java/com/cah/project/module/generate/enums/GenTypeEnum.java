package com.cah.project.module.generate.enums;

import com.cah.project.core.dict.annotation.DictType;
import com.cah.project.core.dict.enums.IDictEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 功能描述: 代码生成方式 <br/>
 */
@Getter
@AllArgsConstructor
@DictType(typeCode = "GEN_TYPE", typeName = "代码生成方式")
public enum GenTypeEnum implements IDictEnum {

    ZIP(0, "0", "zip压缩包"),
    DEF(1, "1", "自定义路径"),
    ;

    private final Integer index;
    private final String code;
    private final String msg;

}
