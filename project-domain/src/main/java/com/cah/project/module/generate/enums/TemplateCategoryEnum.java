package com.cah.project.module.generate.enums;

import com.cah.project.core.dict.annotation.DictType;
import com.cah.project.core.dict.enums.IDictEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 功能描述: 模板种类菜单 <br/>
 */
@Getter
@AllArgsConstructor
@DictType(typeCode = "TPL_CATEGORY", typeName = "模板种类")
public enum TemplateCategoryEnum implements IDictEnum {

    NONE("none", "只有文件"),
    CURD("crud", "单表操作"),
    TREE("tree", "树表操作"),
    SUB("sub", "主子表操作"),
    ;

    private final String code;
    private final String msg;

}
