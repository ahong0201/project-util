package com.cah.project.module.generate.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "GenTableInfo", description = "代码生成表字段定义详情")
public class GenTableColumnInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键ID")
    private Long id;

    @ApiModelProperty("列名称")
    private String columnName;

    @ApiModelProperty("列描述")
    private String columnComment;

    @ApiModelProperty("列类型")
    private String columnType;

    @ApiModelProperty("JAVA类型")
    private String javaType;

    @ApiModelProperty("JAVA字段名")
    private String javaField;

    @ApiModelProperty("是否主键：1-是；0-否")
    private Integer isPk;

    @ApiModelProperty("主键类型：0-自增；1-未设置（跟随全局）；2-用户输入；3-雪花算法；4-UUID；5-全局唯一字符串")
    private Integer pkType;

    @ApiModelProperty("是否必填：1-是；0-否")
    private Integer isRequired;

    @ApiModelProperty("是否为插入字段：1-是；0-否")
    private Integer isInsert;

    @ApiModelProperty("是否编辑字段：1-是；0-否")
    private Integer isEdit;

    @ApiModelProperty("是否列表字段：1-是；0-否")
    private Integer isList;

    @ApiModelProperty("是否查询字段：1-是；0-否")
    private Integer isQuery;

    @ApiModelProperty("查询方式：EQ-等于；NE-不等于；GT-大于；GTE-大于等于；LT-小于；LTE-小于等于；LIKE-模糊；BETWEEN-范围")
    private String queryType;

    @ApiModelProperty("显示类型：input-文本框；textarea-文本域；select-下拉框；checkbox-复选框；radio-单选框；datetime-日期控件；upload-上传")
    private String htmlType;

    @ApiModelProperty("字典类型：查找字典表的dict_type")
    private String dictType;

    @ApiModelProperty("排序")
    private Integer sort;

}
