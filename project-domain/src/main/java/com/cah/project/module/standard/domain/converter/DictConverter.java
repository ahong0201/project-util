package com.cah.project.module.standard.domain.converter;

import com.cah.project.core.domain.converter.BaseConverter;
import com.cah.project.module.standard.domain.entity.DictDataEntity;
import com.cah.project.module.standard.domain.entity.DictTypeEntity;
import com.cah.project.module.standard.domain.vo.DictDataInfo;
import com.cah.project.module.standard.domain.vo.DictTypeInfo;
import com.cah.project.module.standard.domain.vo.out.DictDataList;
import com.cah.project.module.standard.domain.vo.out.DictTypeList;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * <p>字典 对象转换器 </p> <br/>
 *
 * @author cah
 * @since 2022-03-02
 */
@Mapper
public interface DictConverter extends BaseConverter {

    DictConverter INSTANCE = Mappers.getMapper(DictConverter.class);

    DictTypeInfo toDictTypeInfo(DictTypeEntity entity);

    default List<DictTypeList> toDictTypeList(List<DictTypeEntity> es) {
        return list2List(es, this::toDictTypeList);
    }

    DictTypeList toDictTypeList(DictTypeEntity entity);

    DictTypeEntity toDictTypeEntity(DictTypeInfo info);


    DictDataInfo toDictDataInfo(DictDataEntity entity);

    default List<DictDataList> toDictDataList(List<DictDataEntity> es) {
        return list2List(es, this::toDictDataList);
    }

    DictDataList toDictDataList(DictDataEntity entity);

    DictDataEntity toDictDataEntity(DictDataInfo info);

}