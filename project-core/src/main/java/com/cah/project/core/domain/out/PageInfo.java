package com.cah.project.core.domain.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "PageInfo", description = "分页信息")
public class PageInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "每页显示记录数")
    private int pageSize;

    @ApiModelProperty(value = "当前页码")
    private int currentNumber;

    @ApiModelProperty(value = "总记录数")
    private long recordCount;

    @ApiModelProperty(value = "总页数")
    private int pageCount;

    public PageInfo(long recordCount, int pageSize, int currentNumber) {
        this.recordCount = recordCount;
        this.pageSize = pageSize;
        this.currentNumber = currentNumber;
        this.pageCount = (int) Math.ceil((double) recordCount / (double) pageSize);
    }

}
