package com.cah.project.core.conf.mybatis;

import com.baomidou.mybatisplus.core.injector.ISqlInjector;
import com.baomidou.mybatisplus.extension.plugins.OptimisticLockerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.cah.project.core.conf.mybatis.injector.ProjectInjector;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 配置分页插件
 *
 */
@EnableTransactionManagement
@Configuration
@MapperScan("com.cah.project.**.mapper")
public class MybatisPlusConfig {

    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

    /**
     * 乐观锁插件
     */
    @Bean
    public OptimisticLockerInterceptor optimisticLockerInterceptor(){
        return new OptimisticLockerInterceptor();
    }

    /**
     * 自定义sql注入器
     * 或者application.properties配置：
     * mybatis-plus.globalConfig.sqlInjector=com.cah.project.conf.mybatis.injector.ProjectInjector
     */
    @Bean
    public ISqlInjector iSqlInjector() {
        return new ProjectInjector();
    }

}