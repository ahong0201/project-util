package com.cah.project.core.util;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cah.project.core.domain.in.PageQuery;
import com.cah.project.core.domain.out.PageInfo;

/**
 * 功能描述: 分页工具 <br/>
 */
public class PageUtil<T> {

    /**
     * 功能描述: 构建分页组件 <br/>
     *
     * @param query 查询入参
     * @return "com.baomidou.mybatisplus.extension.plugins.pagination.Page<T>"
     */
    public static <T> Page<T> buildPage(PageQuery query) {
        return new Page<>(query.getPage(), query.getLimit());
    }

    /**
     * 功能描述: 构建返回分页信息 <br/>
     *
     * @param page 分页数据
     * @return "com.cah.project.core.domain.out.PageInfo"
     */
    public static <T> PageInfo buildPageInfo(IPage<T> page) {
        return new PageInfo(page.getTotal(), (int) page.getSize(), (int) page.getCurrent() + 1);
    }

}
