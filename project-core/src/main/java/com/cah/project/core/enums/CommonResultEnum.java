package com.cah.project.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 功能描述: 统一返回枚举 <br/>
 */
@Getter
@AllArgsConstructor
public enum CommonResultEnum {

    SUCCESS("0", "成功"),
    ERROR("-1", "出错"),
    ;

    private final String code;
    private final String msg;

}
