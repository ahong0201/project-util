package com.cah.project.core.domain.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 功能描述: 前端 tree 结果封装数据 <br/>
 */
@Data
@ApiModel(value = "CommonTreeResult", description = "前端 tree 结果封装数据")
public class CommonTreeResult implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "状态信息")
    private Status status = new Status();

    @ApiModelProperty(value = "返回数据")
    private Object data;

    @Data
    @ApiModel(value = "Status", description = "所需内部类")
    public static class Status {

        @ApiModelProperty(value = "状态码")
        private Integer code = 200;

        @ApiModelProperty(value = "状态信息")
        private String message = "默认";
    }

    public static CommonTreeResult data(Object data) {
        CommonTreeResult result = new CommonTreeResult();
        result.setData(data);
        return result;
    }

}