package com.cah.project.core.conf.mybatis.injector.methods;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.cah.project.core.conf.mybatis.enums.ProjectSqlMethod;
import com.cah.project.core.dict.annotation.Dict;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;

import java.lang.reflect.Field;

public class SelectListPermission extends AbstractMethod {

    @Override
    public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {
        // 通过 权限注解，来控制注入的SQL，
        Field[] declaredFields = modelClass.getDeclaredFields();
        for(Field field : declaredFields) {
            Dict annotation = field.getAnnotation(Dict.class);
            if(annotation != null) {
                System.out.println("----------->>" + annotation.type());
            }
        }
        ProjectSqlMethod sqlMethod = ProjectSqlMethod.SELECT_LIST_PERMISSION;
        String whereSql = sqlWhereEntityWrapper(true, tableInfo)
                // 字段不为空，才执行
                + "<if test=\"ew != null\">and dept in ('1','2')</if>\n"
                + "<if test=\"ew == null\"> where dept in ('1','2')</if>";
        String sql = String.format(sqlMethod.getSql(), sqlSelectColumns(tableInfo, true),
                tableInfo.getTableName(), whereSql,
                sqlComment());
        SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, modelClass);
        return this.addSelectMappedStatementForTable(mapperClass, sqlMethod.getMethod(), sqlSource, tableInfo);
    }

}
