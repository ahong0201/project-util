package com.cah.project.core.util;

import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

/**
 * 功能描述: 返回页面工具类 <br/>
 */
public class ModelViewUtil {

    /**
     * 功能描述: 返回静态页面 <br/>
     *
     * @param viewPath 页面路径
     * @return "org.springframework.web.servlet.ModelAndView"
     */
    public static ModelAndView stringView(String viewPath) {
        return new ModelAndView(viewPath);
    }

    /**
     * 功能描述: 返回带数据的静态页面 <br/>
     *
     * @param viewPath 页面路径
     * @param model 模型
     * @param data 数据
     * @return "org.springframework.web.servlet.ModelAndView"
     */
    public static ModelAndView dataView(String viewPath, Model model, Object data) {
        model.addAttribute("dataInfo", data);
        return stringView(viewPath);
    }

}
