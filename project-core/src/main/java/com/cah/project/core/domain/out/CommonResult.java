package com.cah.project.core.domain.out;

import com.cah.project.core.enums.CommonResultEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "CommonResult", description = "统一返回类")
public class CommonResult<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "是否成功")
    private boolean success;

    @ApiModelProperty(value = "错误码")
    private String code;

    @ApiModelProperty(value = "错误码描述")
    private String msg;

    @ApiModelProperty(value = "返回内容")
    private T data;

    public static CommonResult<String> ok() {
        return new CommonResult<>(true, CommonResultEnum.SUCCESS.getCode(), CommonResultEnum.SUCCESS.getMsg(), "");
    }

    public static <T> CommonResult<T> data(T data) {
        CommonResult<T> result = new CommonResult<>();
        result.setSuccess(true);
        result.setCode(CommonResultEnum.SUCCESS.getCode());
        result.setMsg(CommonResultEnum.SUCCESS.getMsg());
        result.setData(data);
        return result;
    }

    public static CommonResult<String> fail() {
        return new CommonResult<>(false, CommonResultEnum.ERROR.getCode(), CommonResultEnum.ERROR.getMsg(), "");
    }

    public static CommonResult<String> fail(String msg) {
        return new CommonResult<>(false, CommonResultEnum.ERROR.getCode(), msg, "");
    }

}
