package com.cah.project.core.conf.mybatis.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 功能描述: 自定义本项目支持的SQL方法 <br/>
 */
@Getter
@AllArgsConstructor
public enum ProjectSqlMethod {

    SELECT_COUNT_PERMISSION("selectCountPermission", "查询满足条件总记录数（带权限控制）", "<script>\nSELECT COUNT(%s) FROM %s %s %s\n</script>"),
    SELECT_LIST_PERMISSION("selectListPermission", "查询满足条件所有数据（带权限控制）", "<script>\nSELECT %s FROM %s %s %s\n /**12*/</script>"),
    SELECT_PAGE_PERMISSION("selectPagePermission", "查询满足条件所有数据（并翻页）（带权限控制）", "<script>\nSELECT %s FROM %s %s %s\n</script>"),

    ;
    private final String method;
    private final String desc;
    private final String sql;
}
