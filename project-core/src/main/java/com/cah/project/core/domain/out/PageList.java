package com.cah.project.core.domain.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "PageList", description = "分页列表")
public class PageList<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "分页信息")
    private PageInfo pageInfo;

    @ApiModelProperty(value = "数据")
    private List<T> list;

}
