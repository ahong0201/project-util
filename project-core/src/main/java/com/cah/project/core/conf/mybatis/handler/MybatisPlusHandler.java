package com.cah.project.core.conf.mybatis.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

/**
 * 功能描述: mybatis-plus 自动处理 <br/>
 */
@Component
public class MybatisPlusHandler implements MetaObjectHandler {

    /**
     * 使用mp做添加操作时候，这个方法执行
     * @param metaObject metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        //设置属性值
        this.setFieldValByName("createTime", LocalDate.now(), metaObject);
        this.setFieldValByName("updateTime", LocalDate.now(), metaObject);
    }

    /**
     * 使用mp做修改操作时候，这个方法执行
     * @param metaObject metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        this.setFieldValByName("updateTime", LocalDate.now(), metaObject);
    }

}
