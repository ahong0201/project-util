package com.cah.project.core.domain.converter;

import cn.hutool.core.collection.CollectionUtil;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 功能描述: 对象转换基类 <br/>
 */
public interface BaseConverter {

    /**
     * 功能描述: 列表对象转换 <br/>
     *
     * @param es 带转换对象
     * @param func 单对象转换
     * @return "java.util.List<T>"
     */
    default <T, E> List<T> list2List(List<E> es, Function<E, T> func) {
        if(CollectionUtil.isNotEmpty(es)) {
            return es.stream().map(func).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

}
