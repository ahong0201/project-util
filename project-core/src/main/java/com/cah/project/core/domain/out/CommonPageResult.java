package com.cah.project.core.domain.out;

import com.cah.project.core.enums.CommonResultEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "CommonPageResult", description = "统一分页返回类")
public class CommonPageResult<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "错误码")
    private String code;

    @ApiModelProperty(value = "错误码描述")
    private String msg;

    @ApiModelProperty(value = "返回内容")
    private Object data;

    @ApiModelProperty(value = "总记录条数")
    private Long count;

    public static <T> CommonPageResult<T> data(PageList<T> data) {
        CommonPageResult<T> result = new CommonPageResult<>();
        result.setCode(CommonResultEnum.SUCCESS.getCode());
        result.setData(data.getList());
        result.setCount(data.getPageInfo().getRecordCount());
        return result;
    }

}
