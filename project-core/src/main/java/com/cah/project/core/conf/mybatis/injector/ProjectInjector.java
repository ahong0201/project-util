package com.cah.project.core.conf.mybatis.injector;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.injector.DefaultSqlInjector;
import com.cah.project.core.conf.mybatis.injector.methods.SelectListPermission;

import java.util.List;

public class ProjectInjector extends DefaultSqlInjector {

    @Override
    public List<AbstractMethod> getMethodList(Class<?> mapperClass) {
        List<AbstractMethod> methodList = super.getMethodList(mapperClass);
        // 通过权限，查询数据
        methodList.add(new SelectListPermission());
        return methodList;
    }
}
