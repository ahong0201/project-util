package com.cah.project.compare.comparator;

import java.util.Date;
import java.util.Objects;

/**
 * 功能描述: 默认比较器 <br/>
 */
public class DefaultComparator implements IComparator<Object> {

    @Override
    public int compare(Object o1, Object o2) {
        // 同时为空，为相同
        if(Objects.isNull(o1) && Objects.isNull(o2)) {
            return 0;
        }
        // 都不为空
        if(!Objects.isNull(o2) && !Objects.isNull(o1)) {
            if(o1 instanceof Date) {
                return ((Date) o1).compareTo((Date) o2);
            } else {
                if(o1 == o2 || o1.equals(o2)) {
                    return 0;
                }
            }
            return -1;
        }
        return -1;
    }

}
