package com.cah.project.compare.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 功能描述: 异常枚举 <br/>
 */
@Getter
@AllArgsConstructor
public enum ExceptionEnum {

    OVER_DEPTH("0", "数据结构深度超过指定范围"),
    INCONSISTENT_CLASS("1", "比较的对象类型不一致"),
    PROPERTY_ENTITY_NULL("2", "比较的对象必须拥有@PropertyEntity注解"),
    PROPERTY_ID_NULL("3", "比较的对象必须拥有@PropertyId注解"),
    PROPERTY_ID_TYPE("4", "对象%s的@PropertyId注解类型必须为String或Long"),
    PROPERTY_ID_VALUE_NULL("5", "对象%s属性%s的@PropertyId注解的值为空"),
    ;

    private final String code;
    private final String desc;
}
