package com.cah.project.compare.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 功能描述: 属性描述 <br/>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})
public @interface PropertyField {

    /** 中文描述 */
    String name() default "";

    /** 排序字段，与@PropertyOrder可以同时使用，取两个最大的为主 */
    float order() default 0.00F;

}
