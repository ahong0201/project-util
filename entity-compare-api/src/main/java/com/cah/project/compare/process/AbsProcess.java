package com.cah.project.compare.process;

import com.cah.project.compare.enums.ChangeTypeEnum;
import com.cah.project.compare.enums.ModelTypeEnum;
import com.cah.project.compare.model.ChangeModel;
import com.cah.project.compare.model.PropertyModel;

import java.util.Objects;

public abstract class AbsProcess implements IPropertyProcess {

    protected abstract ModelTypeEnum getModelType();

    /**
     * 功能描述: 获取基本的类型 <br/>
     *
     * @param pm 属性模型
     * @param cte 变化类型
     * @return "com.compare.model.ChangeModel"
     */
    protected ChangeModel getBaseChangeModel(PropertyModel pm, ChangeTypeEnum cte) {
        ChangeModel cm = getBaseChangeModel(pm);
        cm.setChangeType(cte);
        if(!Objects.isNull(pm.getValue())) {
            cm.setTypeValue(pm.getValue().toString());
        }
        // 删除的，说明before有，after没有
        cm.setBefore(ChangeTypeEnum.REMOVED.equals(cte) ? pm.getValue() : null);
        // 新增的，说明after有，before没有
        cm.setAfter(ChangeTypeEnum.ADDED.equals(cte) ? pm.getValue() : null);
        return cm;
    }

    /**
     * 功能描述: 获取基本的类型 <br/>
     *
     * @param beforePm 改变前属性模型
     * @param afterPm 改变后属性模型
     * @return "com.compare.model.ChangeModel"
     */
    protected ChangeModel getBaseChangeModel(PropertyModel beforePm, PropertyModel afterPm) {
        ChangeModel cm = getBaseChangeModel(beforePm);
        // 删除的，说明before有，after没有
        cm.setBefore(beforePm.getValue());
        // 新增的，说明after有，before没有
        cm.setAfter(afterPm.getValue());
        return cm;
    }

    /**
     * 功能描述: 获取基本的类型 <br/>
     *
     * @param pm 属性模型
     * @return "com.compare.model.ChangeModel"
     */
    protected ChangeModel getBaseChangeModel(PropertyModel pm) {
        ChangeModel cm = new ChangeModel();
        cm.setTypeName(pm.getName());
        cm.setTypeComment(pm.getPropertyName());
        cm.setModelType(getModelType());
        return cm;
    }

    /**
     * 功能描述: 比较对象 <br/>
     *
     * @param cm 变化模型
     * @param beforePm before
     * @param afterPm after
     */
    protected void compareChangeType(ChangeModel cm, PropertyModel beforePm, PropertyModel afterPm) {
        if(beforePm.getComparator().compare(beforePm.getValue(), afterPm.getValue()) == 0) {
            cm.setChangeType(ChangeTypeEnum.UNCHANGED);
        } else {
            cm.setChangeType(ChangeTypeEnum.MODIFIED);
        }
    }

}
