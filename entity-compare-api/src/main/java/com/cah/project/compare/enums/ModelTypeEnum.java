package com.cah.project.compare.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 功能描述: 模型类型 <br/>
 */
@Getter
@AllArgsConstructor
public enum ModelTypeEnum {

    ENTITY("Entity", "实体"),
    PROPERTY("Property", "基础属性"),
    ENTITY_PROPERTY("EntityProperty", "实体属性"),
    LIST_PROPERTY("ListProperty", "列表属性"),
    MAP_PROPERTY("MapProperty", "Map属性"),
    ;

    private final String code;
    private final String desc;
}
