package com.cah.project.compare.process.impl;

import com.cah.project.compare.enums.ChangeTypeEnum;
import com.cah.project.compare.enums.ModelTypeEnum;
import com.cah.project.compare.model.ChangeModel;
import com.cah.project.compare.model.PropertyModel;
import com.cah.project.compare.process.AbsProcess;
import com.cah.project.compare.util.CompareHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 功能描述: 自定义实体类型处理 <br/>
 */
public class EntityTypeProcess extends AbsProcess {

    @Override
    protected ModelTypeEnum getModelType() {
        return ModelTypeEnum.ENTITY_PROPERTY;
    }

    @Override
    public ChangeModel process(PropertyModel pm, ChangeTypeEnum cte) throws InstantiationException, IllegalAccessException {
        ChangeModel cm = getBaseChangeModel(pm, cte);
        // 设置子节点
        if(!Objects.isNull(pm.getValue())) {
            List<ChangeModel> children = new ArrayList<>();
            ChangeModel child = CompareHelper.assemblyChangeModelObj(pm.getValue(), cte);
            children.add(child);
            cm.setChildren(children);
        }
        return cm;
    }

    @Override
    public ChangeModel process(PropertyModel beforePm, PropertyModel afterPm) throws InstantiationException, IllegalAccessException {
        ChangeModel cm = getBaseChangeModel(beforePm, afterPm);
        // 如果两个都为空，则为没变化
        if(beforePm.getValue() == null && afterPm.getValue() == null) {
            cm.setChangeType(ChangeTypeEnum.UNCHANGED);
        }
        // 如果before的children不为空，after的为空，则为删除
        if(beforePm.getValue() != null && afterPm.getValue() == null) {
            cm.setChangeType(ChangeTypeEnum.REMOVED);
            List<ChangeModel> children = new ArrayList<>();
            ChangeModel child = CompareHelper.assemblyChangeModelObj(beforePm.getValue(), ChangeTypeEnum.REMOVED);
            children.add(child);
            cm.setChildren(children);
        }
        // 如果before的children为空，after的不为空，则为新增
        if(beforePm.getValue() == null && afterPm.getValue() != null) {
            cm.setChangeType(ChangeTypeEnum.ADDED);
            List<ChangeModel> children = new ArrayList<>();
            ChangeModel child = CompareHelper.assemblyChangeModelObj(afterPm.getValue(), ChangeTypeEnum.ADDED);
            children.add(child);
            cm.setChildren(children);
        }
        // 如果两个都不为空，则重新调用比较
        if(beforePm.getValue() != null && afterPm.getValue() != null) {
            if(beforePm.getComparator() != null) {
                compareChangeType(cm, beforePm, afterPm);
            } else {
                // 默认未变化
                cm.setChangeType(ChangeTypeEnum.UNCHANGED);
                // 设置子节点
                List<ChangeModel> children = new ArrayList<>();
                ChangeModel child = CompareHelper.assemblyChangeModelObj(beforePm.getKey(), beforePm.getValue(), afterPm.getValue());
                children.add(child);
                cm.setChildren(children);
                if(!children.isEmpty()) {
                    // 根据子信息，重新设置变化类型
                    cm.setChangeType(CompareHelper.getChildrenChangeType(children));
                }
            }
        }
        return cm;
    }
}
