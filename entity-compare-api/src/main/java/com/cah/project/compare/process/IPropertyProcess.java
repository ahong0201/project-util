package com.cah.project.compare.process;

import com.cah.project.compare.enums.ChangeTypeEnum;
import com.cah.project.compare.model.ChangeModel;
import com.cah.project.compare.model.PropertyModel;

/**
 * 功能描述: 实体过程 <br/>
 */
public interface IPropertyProcess {

    /**
     * 功能描述: 单个对象处理 <br/>
     *
     * @param pm 属性模型
     * @param cte 变化类型
     * @return "com.cah.project.compare.model.ChangeModel"
     */
    ChangeModel process(PropertyModel pm, ChangeTypeEnum cte) throws InstantiationException, IllegalAccessException;

    /**
     * 功能描述: 两个对象处理 <br/>
     *
     * @param beforePm before属性模型
     * @param afterPm after属性模型
     * @return "com.cah.project.compare.model.ChangeModel"
     */
    ChangeModel process(PropertyModel beforePm, PropertyModel afterPm) throws InstantiationException, IllegalAccessException;

}
