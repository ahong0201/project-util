package com.cah.project.compare.model;

import com.cah.project.compare.enums.ChangeTypeEnum;
import com.cah.project.compare.enums.ModelTypeEnum;
import lombok.Data;

import java.util.List;

/**
 * 功能描述: 改变模型 <br/>
 */
@Data
public class ChangeModel {

    /** 类型名 */
    private String typeName;
    /** 类型值 */
    private String typeValue;
    /** 类型描述（如果为对象，则是PropertyEntity，如果是属性，则为PropertyName） */
    private String typeComment;
    /** 变化类型 */
    private ChangeTypeEnum changeType;
    /** 改变前的数据 */
    private Object before;
    /** 改变后的数据 */
    private Object after;
    /** 子节点 */
    private List<ChangeModel> children;
    /** 模型类型 */
    private ModelTypeEnum modelType;

}
