package com.cah.project.compare.process.impl;

import com.cah.project.compare.enums.ChangeTypeEnum;
import com.cah.project.compare.enums.ModelTypeEnum;
import com.cah.project.compare.model.ChangeModel;
import com.cah.project.compare.model.PropertyModel;
import com.cah.project.compare.process.AbsProcess;

/**
 * 功能描述: 基本类型处理 <br/>
 */
public class BaseTypeProcess extends AbsProcess {

    @Override
    protected ModelTypeEnum getModelType() {
        return ModelTypeEnum.PROPERTY;
    }

    @Override
    public ChangeModel process(PropertyModel pm, ChangeTypeEnum cte) {
        return getBaseChangeModel(pm, cte);
    }

    @Override
    public ChangeModel process(PropertyModel beforePm, PropertyModel afterPm) {
        ChangeModel cm = getBaseChangeModel(beforePm, afterPm);
        compareChangeType(cm, beforePm, afterPm);
        return cm;
    }

}
