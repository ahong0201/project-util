package com.cah.project.compare.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 功能描述: 变化类型枚举 <br/>
 */
@Getter
@AllArgsConstructor
public enum ChangeTypeEnum {

    ADDED("1", "新增"),
    REMOVED("2", "删除"),
    MODIFIED("3", "修改"),
    UNCHANGED("4", "无变化"),
    ;

    private final String code;
    private final String desc;

}
