package com.cah.project.compare.model;

import com.cah.project.compare.comparator.IComparator;
import com.cah.project.compare.enums.PropertyTypeEnum;
import lombok.Data;

import java.lang.reflect.Type;

/**
 * 功能描述: 属性模型 <br/>
 */
@Data
public class PropertyModel {

    /** 如果是object，则key，否则与value一致 */
    private String key;

    /** 属性名 */
    private String name;

    /** 属性值 */
    private Object value;

    /** 设置属性描述 */
    private String propertyName;

    /** 属性所属的类 */
    private Class<?> declaring;

    /** 属性类型 */
    private Type type;

    /** 属性类型枚举 */
    private PropertyTypeEnum pte;

    /** 属性比较器 */
    private IComparator comparator;

    /** 所属实体标识 */
    private String propertyEntity;

    /** 排序 */
    private float order;


}
