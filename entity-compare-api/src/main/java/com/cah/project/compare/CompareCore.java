package com.cah.project.compare;

import com.cah.project.compare.enums.ExceptionEnum;
import com.cah.project.compare.exception.CompareException;
import com.cah.project.compare.model.ChangeModel;
import com.cah.project.compare.util.CompareHelper;

import java.util.*;

/**
 * 功能描述: 比较核心方法 <br/>
 */
public class CompareCore {

    /**
     * 功能描述: 列表比较 <br/>
     *
     * @param before 之前
     * @param after 之后
     */
    public static <E> List<ChangeModel> compare(Collection<E> before, Collection<E> after) throws IllegalAccessException, InstantiationException {
        List<ChangeModel> changeList = new ArrayList<>();
        CompareHelper.compareProcess(changeList, before, after);
        return changeList;
    }

    /**
     * 功能描述: 对象比较 <br/>
     *
     * @param before 之前
     * @param after 之后
     * @return "java.util.List<com.cah.project.compare.model.ChangeModel>"
     */
    public static List<ChangeModel> compare(Object before, Object after) throws IllegalAccessException, InstantiationException {
        // 校验 o1 和 o2 的类型是一致的
        if(!before.getClass().equals(after.getClass())) {
            throw new CompareException(ExceptionEnum.INCONSISTENT_CLASS);
        }
        return compare(Collections.singletonList(before), Collections.singletonList(after));
    }
    
}
