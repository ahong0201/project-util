package com.cah.project.compare.enums;

import com.cah.project.compare.process.IPropertyProcess;
import com.cah.project.compare.process.impl.BaseTypeProcess;
import com.cah.project.compare.process.impl.EntityTypeProcess;
import com.cah.project.compare.process.impl.ListTypeProcess;
import com.cah.project.compare.process.impl.MapTypeProcess;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;
import java.util.Map;

/**
 * 功能描述: 实体类型枚举 <br/>
 */
@Getter
@AllArgsConstructor
public enum PropertyTypeEnum {

    BASE_TYPE("base", "基础数据类型（int/String/...）", new BaseTypeProcess()),
    LIST_TYPE(List.class.getTypeName(), "List", new ListTypeProcess()),
    MAP_TYPE(Map.class.getTypeName(), "Map", new MapTypeProcess()),
    ENTITY_OBJECT_TYPE("entityObject", "自定义实体对象", new EntityTypeProcess()),
    ;

    private final String typeName;
    private final String desc;
    private final IPropertyProcess process;

}
