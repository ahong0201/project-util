package com.cah.project.compare.annotation;

import com.cah.project.compare.comparator.DefaultComparator;
import com.cah.project.compare.comparator.IComparator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 功能描述: 属性比较器，可以自定义 <br/>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})
public @interface PropertyComparator {

    /** 比较器，默认比较器 */
    Class<? extends IComparator> compare() default DefaultComparator.class;

}
