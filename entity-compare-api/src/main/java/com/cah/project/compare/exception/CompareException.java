package com.cah.project.compare.exception;

import com.cah.project.compare.enums.ExceptionEnum;
import lombok.AllArgsConstructor;

/**
 * 功能描述: 比较异常类 <br/>
 */
@AllArgsConstructor
public class CompareException extends RuntimeException {

    private final String code;
    private final String desc;

    public CompareException(ExceptionEnum ee) {
        this(ee.getCode(), ee.getDesc());
    }

    public CompareException(ExceptionEnum ee, Object... args) {
        this(ee.getCode(), String.format(ee.getDesc(), args));
    }

}
