package com.cah.project.compare.comparator;

/**
 * 功能描述: 比较器接口 <br/>
 */
public interface IComparator<T> {

    /**
     * 功能描述: 对象比较 <br/>
     *
     * @param t1 对象1
     * @param t2 对象2
     * @return "int" 返回比较结果 0-相同；非0-不同
     */
    int compare(T t1, T t2);

}
