package com.cah.project.compare.process.impl;

import com.cah.project.compare.enums.ChangeTypeEnum;
import com.cah.project.compare.enums.ModelTypeEnum;
import com.cah.project.compare.model.ChangeModel;
import com.cah.project.compare.model.PropertyModel;
import com.cah.project.compare.process.AbsProcess;

/**
 * 功能描述: Map类型处理 <br/>
 */
public class MapTypeProcess extends AbsProcess {
    @Override
    protected ModelTypeEnum getModelType() {
        return ModelTypeEnum.MAP_PROPERTY;
    }

    @Override
    public ChangeModel process(PropertyModel pm, ChangeTypeEnum cte) throws InstantiationException, IllegalAccessException {
        ChangeModel cm = getBaseChangeModel(pm, cte);
        // TODO ...
        return cm;
    }

    @Override
    public ChangeModel process(PropertyModel beforePm, PropertyModel afterPm) throws InstantiationException, IllegalAccessException {
        ChangeModel cm = getBaseChangeModel(beforePm, afterPm);
        // TODO ...
        return cm;
    }
}
