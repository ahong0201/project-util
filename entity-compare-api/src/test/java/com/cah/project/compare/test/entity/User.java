package com.cah.project.compare.test.entity;

import com.cah.project.compare.annotation.PropertyEntity;
import com.cah.project.compare.annotation.PropertyField;
import com.cah.project.compare.annotation.PropertyId;
import lombok.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * 功能描述: 用户 <br/>
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@PropertyEntity("用户信息")
public class User {

    @PropertyId
    @PropertyField(name = "身份证", order = 0)
    private String idCard;

    @PropertyField(name = "姓名", order = 1)
    private String name;

    @PropertyField(name = "年龄", order = 2)
    private Integer age;

    @PropertyField(name = "体重", order = 4)
    private Float weight;

    @PropertyField(name = "身高", order = 3)
    private float stature;

    @PropertyField(name = "性别", order = 5)
    private int gender;

    @PropertyField(name = "总存款", order = 6)
    private BigDecimal totalDeposit;

    @PropertyField(name = "配偶", order = 7)
    private User spouse;

    @PropertyField(name = "孩子们", order = 8)
    private List<User> children;

}
