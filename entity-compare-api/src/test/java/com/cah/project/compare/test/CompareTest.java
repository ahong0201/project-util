package com.cah.project.compare.test;

import com.cah.project.compare.CompareCore;
import com.cah.project.compare.model.ChangeModel;
import com.cah.project.compare.test.entity.User;

import java.util.ArrayList;
import java.util.List;

public class CompareTest {

    public static void main(String[] args) throws Exception {
        List<ChangeModel> compare = CompareCore.compare(getUserListLeft().get(1), getUserListRight().get(1));
        System.out.println(compare);
    }

    private static List<User> getUserListLeft() {
        List<User> list = new ArrayList<>();
        User user = new User();
        user.setIdCard("11111");
        user.setName("张三");
        list.add(user);


        User user1 = new User();
        user1.setIdCard("11112");
        user1.setName("李四");

        User user11 = new User();
        user11.setIdCard("1122");
        user11.setName("利佩欧");
        user1.setSpouse(user11);

        List<User> children = new ArrayList<>();
        User user111 = new User();
        user111.setIdCard("11221");
        user111.setName("利斯海1");
        children.add(user111);
        user1.setChildren(children);

        User user112 = new User();
        user112.setIdCard("11222");
        user112.setName("利斯海2");
        children.add(user112);
        user1.setChildren(children);
        list.add(user1);

        return list;
    }

    private static List<User> getUserListRight() {
        List<User> list = new ArrayList<>();
        User user = new User();
        user.setIdCard("11111");
        user.setName("张三");
        list.add(user);


        User user1 = new User();
        user1.setIdCard("11112");
        user1.setName("李四");

        User user11 = new User();
        user11.setIdCard("1122");
        user11.setName("利佩欧");
        user1.setSpouse(user11);

        List<User> children = new ArrayList<>();
        User user111 = new User();
        user111.setIdCard("11221");
        user111.setName("利斯海1");
        children.add(user111);
        user1.setChildren(children);

        User user112 = new User();
        user112.setIdCard("11222");
        user112.setName("利斯海22");
        children.add(user112);
        user1.setChildren(children);
        list.add(user1);

        return list;
    }

}
