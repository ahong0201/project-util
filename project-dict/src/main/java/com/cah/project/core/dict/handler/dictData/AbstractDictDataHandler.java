package com.cah.project.core.dict.handler.dictData;

import cn.hutool.cache.Cache;
import cn.hutool.core.collection.ListUtil;
import com.cah.project.core.dict.domain.vo.DictDataOptions;
import com.cah.project.core.dict.domain.vo.DictTypeOptions;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 功能描述: 字典表加载 <br/>
 */
public abstract class AbstractDictDataHandler implements IDictDataHandler {

    /** 定义分页每页大小 */
    protected static final int PAGE_SIZE = 50;

    @Override
    public void loadDictCache(Collection<DictTypeOptions> dictTypeList, Cache<String, List<DictDataOptions>> cacheData) {
        Set<DictTypeOptions> dictTypeOptionsSet = getDictTypeList();
        dictTypeList.addAll(dictTypeOptionsSet);
        List<String> typeCodeList = dictTypeOptionsSet.stream().map(DictTypeOptions::getTypeCode).collect(Collectors.toList());
        int total = typeCodeList.size() / PAGE_SIZE + 1;
        // 分页加载字典数据
        for(int i = 0; i < total; i++) {
            List<DictDataOptions> dictDataList = getDictDataOptions(ListUtil.page(i, PAGE_SIZE, typeCodeList));
            for(DictDataOptions ddp : dictDataList) {
                if(cacheData.get(ddp.getTypeCode()) == null) {
                    cacheData.put(ddp.getTypeCode(), ListUtil.list(false));
                }
                cacheData.get(ddp.getTypeCode()).add(ddp);
            }
        }
    }

}
