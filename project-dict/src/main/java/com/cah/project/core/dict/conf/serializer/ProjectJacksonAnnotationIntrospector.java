package com.cah.project.core.dict.conf.serializer;

import com.cah.project.core.dict.annotation.Dict;
import com.fasterxml.jackson.databind.introspect.Annotated;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;

/**
 * 功能描述: 自定义项目内省器 <省/>
 */
public class ProjectJacksonAnnotationIntrospector extends JacksonAnnotationIntrospector {

    public ProjectJacksonAnnotationIntrospector() {}

    /**
     * 功能描述: 序列号 <br/>
     *
     * @param a 带注释字段
     * @return "java.lang.Object"
     */
    @Override
    public Object findSerializer(Annotated a) {
        // 如果是字典注解
        Dict dict = _findAnnotation(a, Dict.class);
        if(dict != null) {
            return new DictSerializer(dict);
        }
        // 其他扩展。。。
        return super.findSerializer(a);
    }

//    /**
//     * 功能描述: 反序列化 <br/>
//     *
//     * @param a 带注释字段
//     * @return "java.lang.Object"
//     */
//    @Override
//    public Object findDeserializer(Annotated a) {
        // 如果是字典注解
//        Dict dict = _findAnnotation(a, Dict.class);
//        if(dict != null) {
//            return new DictDeserializer(dict.type(), dict.separator());
//        }
        // 其他扩展。。。
//        return super.findSerializer(a);
//    }

}
