package com.cah.project.core.dict.annotation;

import com.cah.project.core.dict.enums.DictReEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 功能描述: 字典注解（用于出参对象字段，将其字典化返回） <br/>
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Dict {

    /** 字典类型 */
    String type();

    /** 字典类型描述 */
    String desc() default "";

    /** 如果多个字典拼接，自定义分隔符 */
    String separator() default ",";

    /** 返回类型 */
    DictReEnum re() default DictReEnum.STRING;

}
