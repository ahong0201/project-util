package com.cah.project.core.dict.cache;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import com.cah.project.core.dict.domain.vo.DictDataOptions;
import com.cah.project.core.dict.domain.vo.DictTypeOptions;
import com.cah.project.core.dict.enums.DictLoadEnum;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 功能描述: 字典数据 缓存 <br/>
 */
public class DictDataCache {

    /**
     * 功能描述: 枚举字典类型 <br/>
     */
    private static final Set<DictTypeOptions> dictTypeList = new HashSet<>();

    /**
     * 功能描述: 添加类型 <br/>
     *
     * @param typeCode 字典类型
     * @param typeName 类型名称
     */
    public static void addType(String typeCode, String typeName) {
        dictTypeList.add(DictTypeOptions.builder().typeCode(typeCode).typeName(typeName).build());
    }

    /**
     * 功能描述: 获取类型列表 <br/>
     *
     * @return "java.util.List<com.cah.project.module.standard.domain.vo.out.DictTypeOptions>"
     */
    public static Set<DictTypeOptions> getTypeList() {
        return dictTypeList;
    }

    /**
     * 功能描述: 获取显示值 <br/>
     *
     * @param type 字典类型
     * @param value 字典码值
     * @return "java.lang.String"
     */
    public static String getLabel(String type, String value) {
        List<DictDataOptions> list = get(type);
        DictDataOptions dataOptions = list.stream().filter(d -> d.getDataValue().equals(value)).findFirst().orElse(null);
        return dataOptions == null ? value : dataOptions.getDataLabel();
    }

    public static String getTypeName(String typeCode) {
        DictTypeOptions dto = DictDataCache.getTypeList().stream().filter(e -> e.getTypeCode().equals(typeCode)).findFirst().orElse(null);
        return dto == null ? typeCode : dto.getTypeName();
    }

    /**
     * 功能描述: 通过加载类型，获取枚举字典 <br/>
     *
     * @param loadType 加载类型
     * @param typeCode 字典类型
     * @return "java.util.List<com.cah.project.module.standard.domain.vo.out.DictDataOptions>"
     */
    public static List<DictDataOptions> get(String loadType, String typeCode) {
        List<DictDataOptions> dictDataOptions = DictLoadEnum.indexOf(loadType).getDictHandler().getDictDataOptions(typeCode);
        if(CollUtil.isNotEmpty(dictDataOptions)) {
            return dictDataOptions;
        }
        return ListUtil.empty();
    }

    /**
     * 功能描述: 获取枚举字典 <br/>
     *
     * @param typeCode 字典类型
     * @return "java.util.List<com.cah.project.module.standard.domain.vo.out.DictDataOptions>"
     */
    public static List<DictDataOptions> get(String typeCode) {
        // 首先从字典加载中获取
        List<DictDataOptions> dictTableCache = DictLoadEnum.DICT_TABLE.getDictHandler().getDictDataOptions(typeCode);
        if(CollUtil.isNotEmpty(dictTableCache)) {
            return dictTableCache;
        }
        // 再从枚举缓存中获取
        List<DictDataOptions> enumTableCache = DictLoadEnum.ENUM.getDictHandler().getDictDataOptions(typeCode);
        if(CollUtil.isNotEmpty(enumTableCache)) {
            return enumTableCache;
        }
        // 最后从业务缓存中获取
        List<DictDataOptions> businessDataCache = DictLoadEnum.BUSINESS_DATA.getDictHandler().getDictDataOptions(typeCode);
        if(CollUtil.isNotEmpty(businessDataCache)) {
            return businessDataCache;
        }
        return ListUtil.empty();
    }

}
