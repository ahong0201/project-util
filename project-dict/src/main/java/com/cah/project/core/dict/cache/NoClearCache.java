package com.cah.project.core.dict.cache;

import cn.hutool.cache.impl.AbstractCache;

import java.util.HashMap;

/**
 * 功能描述: 无清理缓存 <br/>
 */
public class NoClearCache<K, V> extends AbstractCache<K, V> {

    private static final long serialVersionUID = 1L;

    public NoClearCache() {
        cacheMap = new HashMap<>();
    }

    @Override
    protected int pruneCache() {
        return 0;
    }
}
