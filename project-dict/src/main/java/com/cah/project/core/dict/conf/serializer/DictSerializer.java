package com.cah.project.core.dict.conf.serializer;

import com.cah.project.core.dict.annotation.Dict;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

/**
 * 功能描述: 字典序列化 <br/>
 */
public class DictSerializer extends StdSerializer<Object> {

    /** 字典注解 */
    private final Dict dict;

    public DictSerializer(Dict dict) {
        super(Object.class);
        this.dict = dict;
    }

    @Override
    public void serialize(Object value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeObject(dict.re().getLabel(dict.type(), dict.separator(), value.toString()));
    }

}
