package com.cah.project.core.dict.enums;

/**
 * 功能描述: 字典枚举接口 <br/>
 */
public interface IDictEnum {

    /**
     * 功能描述: 码值 <br/>
     *
     * @return "java.lang.String"
     */
    String getCode();

    /**
     * 功能描述: 显示值 <br/>
     *
     * @return "java.lang.String"
     */
    String getMsg();

}
