package com.cah.project.core.dict.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "DictDataOptions", description = "字典数据选项")
public class DictDataOptions implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("字典类型")
    private String typeCode;

    @ApiModelProperty("字典标签")
    private String dataLabel;

    @ApiModelProperty("字典值")
    private String dataValue;

}
