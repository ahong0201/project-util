package com.cah.project.core.dict.enums;

import com.cah.project.core.dict.cache.DictDataCache;
import com.cah.project.core.dict.domain.bo.DictData;

/**
 * 功能描述: 字典返回类型 <br/>
 */
public enum DictReEnum {

    /** 字典字符 */
    STRING {
        @Override
        public Object getLabel(String type, String separator, String code) {
            return changeLabel(type, separator, code);
        }
    },
    /** 字典对象 */
    DICT_DATA {
        @Override
        public Object getLabel(String type, String separator, String code) {
            return DictData.builder().type(type).value(code).label(changeLabel(type, separator, code)).build();
        }
    },
    ;

    /**
     * 功能描述: 改变值 <br/>
     *
     * @param type 字典类型
     * @param separator 分隔符
     * @param code 字典码值
     * @return "java.lang.String"
     */
    private static String changeLabel(String type, String separator, String code) {
        if(separator != null && separator.length() > 0) {
            String[] strs = code.split(separator);
            if (strs.length > 1) {
                StringBuilder sb = new StringBuilder();
                for (String str : strs) {
                    // 从缓存中获取字典。如果不行，通过SpringUtil.getBean(); 获取服务处理
                    sb.append(DictDataCache.getLabel(type, str)).append(separator);
                }
                return sb.substring(0, sb.length() - 1);
            }
        }
        // 从缓存中获取字典。如果不行，通过SpringUtil.getBean(); 获取服务处理
        return DictDataCache.getLabel(type, code);
    }

    /**
     * 功能描述: 抽象的获取字典显示值的方法 <br/>
     *
     * @param type 字典类型
     * @param separator 分隔符
     * @param code 字典码值
     * @return "java.lang.Object"
     */
    public abstract Object getLabel(String type, String separator, String code);

}
