package com.cah.project.core.dict.handler.dictData;

import com.cah.project.core.dict.domain.vo.DictDataOptions;
import com.cah.project.core.dict.domain.vo.DictTypeOptions;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * 功能描述: 默认实现 <br/>
 */
public class DefaultDictDataHandler extends AbstractDictDataHandler {

    public DefaultDictDataHandler() {
        super();
    }

    @Override
    public Set<DictTypeOptions> getDictTypeList() {
        return Collections.emptySet();
    }

    @Override
    public List<DictDataOptions> getDictDataOptions(String typeCode) {
        return Collections.emptyList();
    }

    @Override
    public List<DictDataOptions> getDictDataOptions(Collection<String> typeCodeList) {
        return Collections.emptyList();
    }
}
