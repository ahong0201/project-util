package com.cah.project.core.dict.domain.bo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 功能描述: 字典映射 <br/>
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DictData implements Serializable {

    /** 字典类型 */
    private String type;
    /** 代码值 */
    private String value;
    /** 显示值 */
    private String label;

}
