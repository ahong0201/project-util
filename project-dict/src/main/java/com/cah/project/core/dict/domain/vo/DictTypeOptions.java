package com.cah.project.core.dict.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Objects;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "DictTypeOptions", description = "字典类型选项")
public class DictTypeOptions implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("字典类型")
    private String typeCode;

    @ApiModelProperty("字典类型名称")
    private String typeName;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DictTypeOptions)) return false;
        DictTypeOptions that = (DictTypeOptions) o;
        return getTypeCode().equals(that.getTypeCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTypeCode());
    }

}
