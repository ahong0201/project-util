package com.cah.project.core.dict.event;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 功能描述: 字典重新加载事件操作 <br/>
 */
@Component
public class DictReloadPublishEvent {

    @Resource
    private ApplicationContext applicationContext;

    public void publishEvent(String typeCode) {
        DictEvent de = new DictEvent(this, typeCode);
        applicationContext.publishEvent(de);
    }

}
