package com.cah.project.core.dict.conf.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;

public class DictDeserializer extends JsonDeserializer<Object> {

    /** 字典类型 */
    private final String type;
    /** 分隔符 */
    private final String separator;

    public DictDeserializer(String type, String separator) {
        this.type = type;
        this.separator = separator;
    }


    @Override
    public Object deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        return null;
    }
}
