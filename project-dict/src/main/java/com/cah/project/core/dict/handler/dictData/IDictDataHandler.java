package com.cah.project.core.dict.handler.dictData;

import cn.hutool.cache.Cache;
import com.cah.project.core.dict.domain.vo.DictDataOptions;
import com.cah.project.core.dict.domain.vo.DictTypeOptions;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface IDictDataHandler {

    /**
     * 功能描述: 获取全部的字典类型 <br/>
     *
     * @return "java.util.List<com.cah.project.module.standard.domain.entity.DictTypeEntity>"
     */
    Set<DictTypeOptions> getDictTypeList();

    /**
     * 功能描述: 通过字典类型，获取字典数据 <br/>
     *
     * @param typeCode 字典类型
     * @return "java.util.List<com.cah.project.module.standard.domain.vo.out.DictDataOptions>"
     */
    List<DictDataOptions> getDictDataOptions(String typeCode);

    /**
     * 功能描述: 通过批量字典类型，查询字典数据 <br/>
     *
     * @param typeCodeList 字典类型集合
     * @return "java.util.List<com.cah.project.module.standard.domain.vo.out.DictDataOptions>"
     */
    List<DictDataOptions> getDictDataOptions(Collection<String> typeCodeList);

    /**
     * 功能描述: 加载缓存 <br/>
     *
     * @param dictTypeList 字典类型列表
     * @param cacheData 字典缓存数据
     */
    void loadDictCache(Collection<DictTypeOptions> dictTypeList, Cache<String, List<DictDataOptions>> cacheData);

}
