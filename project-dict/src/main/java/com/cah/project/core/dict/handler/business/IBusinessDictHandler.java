package com.cah.project.core.dict.handler.business;

import com.cah.project.core.dict.domain.vo.DictDataOptions;

import java.util.List;

/**
 * 功能描述: 业务字典处理接口 <br/>
 */
public interface IBusinessDictHandler {

    /**
     * 功能描述: 获取字典类型 <br/>
     *
     * @return "java.lang.String"
     */
    String typeCode();

    /**
     * 功能描述: 字典描述 <br/>
     *
     * @return "java.lang.String"
     */
    String typeName();

    /**
     * 功能描述: 获取字典数据 <br/>
     */
    List<DictDataOptions> getDictDataList();

}
