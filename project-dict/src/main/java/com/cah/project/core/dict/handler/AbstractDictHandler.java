package com.cah.project.core.dict.handler;

import cn.hutool.cache.Cache;
import cn.hutool.core.collection.ListUtil;
import com.cah.project.core.dict.cache.DictDataCache;
import com.cah.project.core.dict.domain.vo.DictDataOptions;
import com.cah.project.core.dict.event.DictEvent;

import java.util.List;

/**
 * 功能描述: 字典处理，抽象类 <br/>
 */
public abstract class AbstractDictHandler implements IDictHandler {

    /***
     * 功能描述: 缓存对象 <br/>
     */
    protected Cache<String, List<DictDataOptions>> cacheData;

    public AbstractDictHandler() {

    }

    @Override
    public void onApplicationEvent(DictEvent event) {
        reloadDictCache(event.getTypeCode());
    }

    /**
     * 功能描述: 重新加载缓存 <br/>
     */
    abstract void reloadDictCache(String typeCode);

    /**
     * 功能描述: 添加字典类型 <br/>
     *
     * @param typeCode 字典类型
     * @param typeName 字典类型名称
     */
    protected void addDictType(String typeCode, String typeName) {
        DictDataCache.addType(typeCode, typeName);
    }

    /***
     * 功能描述: 批量添加字典数据缓存 <br/>
     *
     * @param typeCode 字典类型
     * @param dataList 字典数据列表
     */
    protected void put(String typeCode, List<DictDataOptions> dataList) {
        if(cacheData.get(typeCode) == null) {
            cacheData.put(typeCode, ListUtil.list(false));
        }
        cacheData.get(typeCode).addAll(dataList);
    }

    /**
     * 功能描述: 单条添加字典数据缓存 <br/>
     *
     * @param typeCode 字典类型
     * @param dataValue 字典值
     * @param dataLabel 字典标签
     */
    protected void put(String typeCode, String dataValue, String dataLabel) {
        if(cacheData.get(typeCode) == null) {
            cacheData.put(typeCode, ListUtil.list(false));
        }
        cacheData.get(typeCode).add(DictDataOptions.builder().typeCode(typeCode).dataValue(dataValue).dataLabel(dataLabel).build());
    }

    /**
     * 功能描述: 移除缓存 <br/>
     *
     * @param typeCode 字典类型
     */
    protected void remove(String typeCode) {
        cacheData.remove(typeCode);
    }

}
