package com.cah.project.core.dict.handler;

import com.cah.project.core.dict.domain.vo.DictDataOptions;
import com.cah.project.core.dict.event.DictEvent;
import org.springframework.context.ApplicationListener;

import java.util.List;

/***
 * 功能描述: 字典加载接口 <br/>
 */
public interface IDictHandler extends ApplicationListener<DictEvent> {

    /**
     * 功能描述: 加载缓存 <br/>
     */
    void loadDictCache();

    /**
     * 功能描述: 通过字典类型，获取字典数据 <br/>
     *
     * @param typeCode 字典类型
     * @return "java.util.List<com.cah.project.module.standard.domain.vo.out.DictDataOptions>"
     */
    List<DictDataOptions> getDictDataOptions(String typeCode);

}
