package com.cah.project.core.dict.conf;

import com.cah.project.core.dict.conf.serializer.ProjectJacksonAnnotationIntrospector;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

/**
 * 功能描述: Jackson配置 <br/>
 */
@Configuration
public class JacksonConfig {

    public JacksonConfig() {}

    @Bean
    @Primary
    @ConditionalOnMissingBean({ObjectMapper.class})
    public ObjectMapper jacksonObjectMapper(Jackson2ObjectMapperBuilder builder) {
        ObjectMapper objectMapper = builder.createXmlMapper(false).build();
        // 定义 不为空，才能进行序列化
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        // 添加 自定义 项目注解内省器
        objectMapper.setAnnotationIntrospector(new ProjectJacksonAnnotationIntrospector());
        return objectMapper;
    }

}
