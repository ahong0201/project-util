package com.cah.project.core.dict;

import com.cah.project.core.dict.enums.DictLoadEnum;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 功能描述: 字典启动加载加载 <br/>
 */
@Component
@Order(value = 2)
public class DictStartRunner implements ApplicationRunner {

    @Override
    public void run(ApplicationArguments args) {
        try {
            DictLoadEnum.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
