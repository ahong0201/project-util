package com.cah.project.core.dict.enums;

import cn.hutool.extra.spring.SpringUtil;
import com.cah.project.core.dict.annotation.DictType;
import com.cah.project.core.dict.handler.BusinessDictHandler;
import com.cah.project.core.dict.handler.DictDataHandler;
import com.cah.project.core.dict.handler.DictEnumHandler;
import com.cah.project.core.dict.handler.IDictHandler;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * 功能描述: 字典加载枚举 <br/>
 */
@Getter
@AllArgsConstructor
@DictType(typeCode= "LOAD_TYPE", typeName = "字典加载类型")
public enum DictLoadEnum implements IDictEnum {

    /** 枚举（永久缓存） */
    ENUM("1", "枚举", DictEnumHandler.class),
    /** 字典表（先进先出）[如果与枚举冲突，将会覆盖枚举] */
    DICT_TABLE("2", "字典表", DictDataHandler.class),
    /** 业务数据（使用频率） */
    BUSINESS_DATA("3", "业务数据", BusinessDictHandler.class),
    ;

    private final String type;
    private final String msg;
    private final Class<? extends IDictHandler> handler;

    public static DictLoadEnum indexOf(String type) {
        return Stream.of(values()).filter(d -> d.getType().equals(type)).findFirst().orElse(ENUM);
    }

    public IDictHandler getDictHandler() {
        return SpringUtil.getBean(getHandler());
    }

    @Override
    public String getCode() {
        return getType();
    }

    /**
     * 功能描述: 启动加载 <br/>
     */
    public static void start() {
        for(DictLoadEnum dle : DictLoadEnum.values()) {
            dle.getDictHandler().loadDictCache();
        }
    }

}
