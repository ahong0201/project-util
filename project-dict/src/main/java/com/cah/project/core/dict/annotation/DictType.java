package com.cah.project.core.dict.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 功能描述: 字典类型注解（用于将枚举类头部，将枚举变成字典缓存起来） <br/>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface DictType {

    /** 字典类型 */
    String typeCode();

    /** 字典名称 */
    String typeName();

}
