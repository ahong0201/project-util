package com.cah.project.core.dict.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * 功能描述: 字典重新加载事件 <br/>
 */
@Getter
public class DictEvent extends ApplicationEvent {

    private final String typeCode;

    /**
     * Create a new {@code ApplicationEvent}.
     *
     * @param source the object on which the event initially occurred or with
     *               which the event is associated (never {@code null})
     */
    public DictEvent(Object source, String typeCode) {
        super(source);
        this.typeCode = typeCode;
    }
}
