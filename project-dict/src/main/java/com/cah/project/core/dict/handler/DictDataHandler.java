package com.cah.project.core.dict.handler;

import cn.hutool.cache.CacheUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import com.cah.project.core.dict.cache.DictDataCache;
import com.cah.project.core.dict.domain.vo.DictDataOptions;
import com.cah.project.core.dict.handler.dictData.IDictDataHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 功能描述: 字典表加载 <br/>
 */
@Component
public class DictDataHandler extends AbstractDictHandler {

    @Autowired
    private DictHandlerAware dictHandlerAware;

    public DictDataHandler() {
        cacheData = CacheUtil.newFIFOCache(10000);
    }

    @Override
    public void loadDictCache() {
        // 加载字典缓存
        getDictDataHandler().loadDictCache(DictDataCache.getTypeList(), cacheData);
    }

    @Override
    public void reloadDictCache(String typeCode) {
        remove(typeCode);
        List<DictDataOptions> dictDataList = getDictDataHandler().getDictDataOptions(typeCode);
        if(CollUtil.isNotEmpty(dictDataList)) {
            put(typeCode, dictDataList);
        }
    }

    @Override
    public List<DictDataOptions> getDictDataOptions(String typeCode) {
        List<DictDataOptions> dictDataOptions = cacheData.get(typeCode);
        // 如果缓存中不存在，则说明可能失效了，再次从数据库中查询
        if(CollUtil.isEmpty(dictDataOptions)) {
            List<DictDataOptions> dictDataList = getDictDataHandler().getDictDataOptions(typeCode);
            if(CollUtil.isEmpty(dictDataList)) {
                put(typeCode, dictDataList);
            } else {
                return ListUtil.empty();
            }
        }
        return cacheData.get(typeCode);
    }

    private IDictDataHandler getDictDataHandler() {
        return dictHandlerAware.getDictHandler();
    }

}
