# project-util

#### 介绍
项目工具
在项目开发过程中，总是会遇到一些需要协同处理，开发规范，或者开发工具的内容。本项目就是为了集成这类内容而开发的。

#### 模块介绍
database-compare-api: 数据库比对工具

entity-compare-api: 实体比对工具（已完成，入口为：CompareCore）

project-allin: 表达式校验规则（未完成）

project-domain: 实体包与枚举

project-web: 前端与控制层（公共模块需要抽取）

project-dict：字典框架


#### 安装教程

1.  拉取代码，使用idea打开。
2.  创建数据库，库名为：project-util（或者其他，自行修改project-util/project-web/src/main/resources/application-dev.yml对应的数据库配置）
3.  执行数据库初始化脚本：init.sql（project-util/project-web/doc/init.sql）
4.  执行数据初始化脚本：init_data.sql（project-util/project-web/doc/init_data.sql）

#### 代码量统计
git log --since="2022-01-01" --until="2022-08-31" --author="chenah" --pretty=tformat: --numstat|gawk '{add += $1; subs += $2; loc += $1-$2} END {printf "added lines: %s remove lines: %s total lines: %s\n",add,subs,loc}'


#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 特技
