package com.cah.project.module.standard.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cah.project.core.dict.domain.vo.DictTypeOptions;
import com.cah.project.core.domain.out.PageList;
import com.cah.project.enums.YesOrNoEnum;
import com.cah.project.module.standard.domain.entity.DictTypeEntity;
import com.cah.project.module.standard.domain.vo.DictTypeInfo;
import com.cah.project.module.standard.domain.vo.in.DictTypeQuery;
import com.cah.project.module.standard.domain.vo.out.DictTypeList;

import java.util.Collection;
import java.util.List;

/**
 * 功能描述: 字典类型 服务接口 <br/>
 */
public interface IDictTypeService extends IService<DictTypeEntity> {

    /**
     * 功能描述: 获取字典类型详情 <br/>
     *
     * @param id 主键ID
     * @return "com.cah.project.module.standard.domain.vo.DictTypeInfo"
     */
    DictTypeInfo selectById(String id);

    /**
     * 功能描述: 分页查询字典类型列表 <br/>
     *
     * @param query 查询条件
     * @return "com.cah.project.core.domain.out.PageList<com.cah.project.module.standard.domain.vo.out.DictTypeList>"
     */
    PageList<DictTypeList> pageList(DictTypeQuery query);

    /**
     * 功能描述: 保存字典类型数据 <br/>
     *
     * @param info 数据
     */
    void saveInfo(DictTypeInfo info);

    /**
     * 功能描述: 修改字典类型启用标识 <br/>
     *
     * @param id 主键ID
     * @param enable 标识枚举
     */
    void updateEnable(String id, YesOrNoEnum enable);

    /**
     * 功能描述: 获取字典类型下拉选项 <br/>
     *
     * @return "java.util.List<com.cah.project.core.dict.domain.vo.out.DictTypeOptions>"
     */
    Collection<DictTypeOptions> selectOptions();

    /**
     * 功能描述: 查询全部的字典类型 <br/>
     *
     * @return "java.util.List<com.cah.project.module.standard.domain.entity.DictTypeEntity>"
     */
    List<DictTypeEntity> selectAll();

}
