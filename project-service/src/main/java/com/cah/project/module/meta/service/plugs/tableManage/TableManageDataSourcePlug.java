package com.cah.project.module.meta.service.plugs.tableManage;

import com.cah.project.plugs.IPlugsSlot;
import com.cah.project.plugs.annotation.PlugsSlot;

/**
 * 功能描述: 数据源插件1 <br/>
 */
@PlugsSlot(order = 1f)
public class TableManageDataSourcePlug implements IPlugsSlot {

    @Override
    public void handler(Object... objects) {
        System.out.println(this.getClass());
    }

}
