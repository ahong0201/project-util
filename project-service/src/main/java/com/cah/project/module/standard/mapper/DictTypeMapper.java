package com.cah.project.module.standard.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cah.project.module.standard.domain.entity.DictTypeEntity;

/**
 * 功能描述: 字典类型 mapper <br/>
 */
public interface DictTypeMapper extends BaseMapper<DictTypeEntity> {

}
