package com.cah.project.module.meta.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cah.project.core.domain.out.PageList;
import com.cah.project.module.meta.domain.entity.DataSourceEntity;
import com.cah.project.module.meta.domain.vo.DataSourceInfo;
import com.cah.project.module.meta.domain.vo.in.DataSourceQuery;
import com.cah.project.module.meta.domain.vo.out.DataSourceList;

import java.util.List;

/**
 * <p>数据源 服务接口 </p> <br/>
 *
 * @author cah
 * @since 2021-10-06
 */
public interface IDataSourceService extends IService<DataSourceEntity> {

    /**
     * 功能描述: 分页查询 <br/>
     *
     * @param query 查询入参
     * @return "com.cah.project.core.domain.out.PageList<com.cah.project.module.meta.domain.vo.out.DataSourceList>"
     */
    PageList<DataSourceList> pageList(DataSourceQuery query);

    /**
     * 功能描述: 通过主键ID，查看详情 <br/>
     *
     * @param id 主键ID
     * @return "com.cah.project.module.meta.domain.vo.DataSourceInfo"
     */
    DataSourceInfo selectById(String id);

    /**
     * 功能描述: 保存数据 <br/>
     *
     * @param info 数据信息
     */
    void saveInfo(DataSourceInfo info);

    /**
     * 功能描述: 连接测试 <br/>
     *
     * @param info 数据信息
     */
    void linkTest(DataSourceInfo info);

    /**
     * 功能描述: 连接测试 <br/>
     *
     * @param id 数据信息ID
     */
    void linkTest(String id);

    /**
     * 功能描述: 通过主键ID，删除数据 <br/>
     *
     * @param id 主键ID
     */
    void deleteById(String id);

    /**
     * 功能描述: 查询全部数据 <br/>
     *
     * @return "java.util.List<com.cah.project.module.meta.domain.entity.DataSourceEntity>"
     */
    List<DataSourceEntity> selectAll();

}
