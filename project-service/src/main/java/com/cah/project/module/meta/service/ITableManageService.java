package com.cah.project.module.meta.service;

import com.cah.project.module.meta.domain.vo.in.DbTableInfoQuery;
import com.cah.project.module.meta.domain.vo.out.DbTableInfoList;

import java.util.List;

/**
 * <p>表管理  服务接口 </p> <br/>
 *
 * @author cah
 * @since 2022-2-24
 */
public interface ITableManageService {


    List<DbTableInfoList> selectAllList(DbTableInfoQuery query);

}
