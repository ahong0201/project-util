package com.cah.project.module.meta.service.plugs.tableManage;

import com.cah.project.plugs.IPlugsSlot;
import org.springframework.stereotype.Component;

/**
 * 功能描述: 数据源插件1 <br/>
 */
@Component
public class TableManageDataSourcePlug2 implements IPlugsSlot {

    @Override
    public void handler(Object... objects) {
        System.out.println(this.getClass());
    }

}
