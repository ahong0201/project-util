package com.cah.project.module.standard.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cah.project.core.dict.cache.DictDataCache;
import com.cah.project.core.dict.domain.vo.DictTypeOptions;
import com.cah.project.core.domain.out.PageList;
import com.cah.project.core.util.PageUtil;
import com.cah.project.enums.YesOrNoEnum;
import com.cah.project.exception.BusinessException;
import com.cah.project.module.standard.domain.converter.DictConverter;
import com.cah.project.module.standard.domain.entity.DictTypeEntity;
import com.cah.project.module.standard.domain.vo.DictTypeInfo;
import com.cah.project.module.standard.domain.vo.in.DictTypeQuery;
import com.cah.project.module.standard.domain.vo.out.DictTypeList;
import com.cah.project.module.standard.mapper.DictTypeMapper;
import com.cah.project.module.standard.service.IDictTypeService;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

/**
 * 功能描述: 字典类型 服务实现 <br/>
 */
@Service
public class DictTypeServiceImpl extends ServiceImpl<DictTypeMapper, DictTypeEntity> implements IDictTypeService {

    @Override
    public DictTypeInfo selectById(String id) {
        if(StrUtil.isEmpty(id)) {
            return new DictTypeInfo();
        }
        DictTypeEntity entity = getById(id);
        return DictConverter.INSTANCE.toDictTypeInfo(entity);
    }

    @Override
    public PageList<DictTypeList> pageList(DictTypeQuery query) {
        IPage<DictTypeEntity> page = lambdaQuery()
                .like(StrUtil.isNotEmpty(query.getTypeName()), DictTypeEntity::getTypeName, query.getTypeName())
                .page(PageUtil.buildPage(query));
        // 对象转换
        List<DictTypeList> list = DictConverter.INSTANCE.toDictTypeList(page.getRecords());
        return new PageList<>(PageUtil.buildPageInfo(page), list);
    }

    @Override
    public void saveInfo(DictTypeInfo info) {
        DictTypeEntity entity = DictConverter.INSTANCE.toDictTypeEntity(info);
        // 通过 typeCode 查询，判断数据库中是否已经存在
        DictTypeEntity dbEntity = selectOneByTypeCode(info.getTypeCode());
        // 如果为修改
        if(ObjectUtil.isNotNull(dbEntity) && !dbEntity.getId().equals(entity.getId())) {
            throw new BusinessException("已经存在类型代码相同的数据。");
        }
        if(ObjectUtil.isEmpty(entity.getId())) {
            entity.setEnable(YesOrNoEnum.YES.getIndex());
        }
        saveOrUpdate(entity);
    }

    @Override
    public void updateEnable(String id, YesOrNoEnum enable) {
        DictTypeEntity entity = new DictTypeEntity();
        entity.setId(Long.valueOf(id));
        entity.setEnable(enable.getIndex());
        updateById(entity);
    }

    private DictTypeEntity selectOneByTypeCode(String typeCode) {
        return lambdaQuery().eq(DictTypeEntity::getTypeCode, typeCode).one();
    }

    @Override
    public Collection<DictTypeOptions> selectOptions() {
        // 从缓存中获取
        return DictDataCache.getTypeList();
    }

    @Override
    public List<DictTypeEntity> selectAll() {
        return lambdaQuery().eq(DictTypeEntity::getEnable, YesOrNoEnum.YES.getCode()).list();
    }


}
