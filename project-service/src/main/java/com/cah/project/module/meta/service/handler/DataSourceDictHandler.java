package com.cah.project.module.meta.service.handler;

import com.cah.project.core.dict.domain.vo.DictDataOptions;
import com.cah.project.core.dict.handler.business.AbstractBusinessDictHandler;
import com.cah.project.module.meta.domain.entity.DataSourceEntity;
import com.cah.project.module.meta.service.IDataSourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 功能描述: 数据源字典处理 <br/>
 */
@Component
public class DataSourceDictHandler extends AbstractBusinessDictHandler {

    @Autowired
    private IDataSourceService dataSourceService;

    @Override
    public String typeCode() {
        return "DATA_SOURCE";
    }

    @Override
    public String typeName() {
        return "数据源";
    }

    @Override
    public List<DictDataOptions> getDictDataList() {
        List<DataSourceEntity> dataSourceList = dataSourceService.selectAll();
        return dataSourceList.stream()
                .map(e -> DictDataOptions.builder().typeCode(typeCode()).dataLabel(e.getName()).dataValue(e.getId()).build())
                .collect(Collectors.toList());
    }
}
