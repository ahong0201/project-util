package com.cah.project.module.standard.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cah.project.core.domain.out.PageList;
import com.cah.project.core.util.PageUtil;
import com.cah.project.module.standard.domain.converter.CiHaiConverter;
import com.cah.project.module.standard.domain.entity.CiHaiEntity;
import com.cah.project.module.standard.domain.vo.CiHaiInfo;
import com.cah.project.module.standard.domain.vo.in.CiHaiQuery;
import com.cah.project.module.standard.domain.vo.out.CiHaiList;
import com.cah.project.module.standard.mapper.CiHaiMapper;
import com.cah.project.module.standard.service.ICiHaiService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 功能描述: 辞海 服务实现 <br/>
 */
@Service
@RequiredArgsConstructor(onConstructor_ = {@Autowired, @Lazy})
public class CiHaiServiceImpl extends ServiceImpl<CiHaiMapper, CiHaiEntity> implements ICiHaiService {

    @Override
    public PageList<CiHaiList> pageList(CiHaiQuery query) {
        // 组转查询条件
        Wrapper<CiHaiEntity> wrapper = Wrappers.<CiHaiEntity>lambdaQuery()
                .and(StrUtil.isNotEmpty(query.getCnName()), w -> w.like(CiHaiEntity::getCnName, query.getCnName())
                                                                  .or()
                                                                  .like(CiHaiEntity::getCnDescribe, query.getCnName()))
                .and(StrUtil.isNotEmpty(query.getEnName()), w -> w.like(CiHaiEntity::getEnFullName, query.getEnName())
                                                                  .or()
                                                                  .like(CiHaiEntity::getEnName, query.getEnName()))
                ;
        // 查询数据
        IPage<CiHaiEntity> page = page(PageUtil.buildPage(query), wrapper);
        // 对象转换
        List<CiHaiList> list = CiHaiConverter.INSTANCE.toCiHaiList(page.getRecords());
        return new PageList<>(PageUtil.buildPageInfo(page), list);
    }

    @Override
    public CiHaiInfo selectById(String id) {
        if(StrUtil.isEmpty(id)) {
            return new CiHaiInfo();
        }
        CiHaiEntity entity = getById(id);
        return CiHaiConverter.INSTANCE.toCiHaiInfo(entity);
    }

    @Override
    public void saveInfo(CiHaiInfo info) {
        CiHaiEntity entity = CiHaiConverter.INSTANCE.toCiHaiEntity(info);
        saveOrUpdate(entity);
    }

    @Override
    public void deleteById(String id) {
        removeById(id);
    }

}
