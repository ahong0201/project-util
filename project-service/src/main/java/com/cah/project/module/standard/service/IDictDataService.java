package com.cah.project.module.standard.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cah.project.core.dict.domain.vo.DictDataOptions;
import com.cah.project.core.domain.out.PageList;
import com.cah.project.enums.YesOrNoEnum;
import com.cah.project.module.standard.domain.entity.DictDataEntity;
import com.cah.project.module.standard.domain.vo.DictDataInfo;
import com.cah.project.module.standard.domain.vo.in.DictDataQuery;
import com.cah.project.module.standard.domain.vo.out.DictDataList;

import java.util.Collection;
import java.util.List;

/**
 * 功能描述: 字典数据 服务接口 <br/>
 */
public interface IDictDataService extends IService<DictDataEntity> {

    /**
     * 功能描述: 通过字典类型，查询字典数据 <br/>
     *
     * @param dictType 字典类型
     * @return "java.util.List<com.cah.project.core.dict.domain.bo.DictDataOptions>"
     */
    List<DictDataOptions> selectDataOptionsByCode(String dictType);

    /**
     * 功能描述: 获取字典类型下拉选项，可以根据加载参数，找找不同的数据 <br/>
     *
     * @param loadType 加载方式
     * @param dictType 字典类型
     * @return "java.util.List<com.cah.project.core.dict.domain.bo.DictDataOptions>"
     */
    List<DictDataOptions> selectDataOptionsByLoadType(String loadType, String dictType);

    /**
     * 功能描述: 通过字典类型，获取字典数据 <br/>
     *
     * @param dictType 字典类型
     * @return "java.util.List<com.cah.project.module.standard.domain.entity.DictDataEntity>"
     */
    List<DictDataEntity> selectListByCode(String dictType);

    /**
     * 功能描述: 通过字典类型集合，批量获取字典数据 <br/>
     *
     * @param dictTypeList 字典类型集合
     * @return "java.util.List<com.cah.project.module.standard.domain.entity.DictDataEntity>"
     */
    List<DictDataEntity> selectListByCodes(Collection<String> dictTypeList);

    /**
     * 功能描述: 获取字典数据详情 <br/>
     *
     * @param id 主键ID
     * @return "com.cah.project.module.standard.domain.vo.DictDataInfo"
     */
    DictDataInfo selectById(String typeCode, String id);

    /**
     * 功能描述: 分页查询字典数据列表 <br/>
     *
     * @param query 查询条件
     * @return "com.cah.project.core.domain.out.PageList<com.cah.project.module.standard.domain.vo.out.DictTypeList>"
     */
    PageList<DictDataList> pageList(DictDataQuery query);

    /**
     * 功能描述: 保存字典数据 <br/>
     *
     * @param info 数据
     */
    void saveInfo(DictDataInfo info);

    /**
     * 功能描述: 修改字典类型启用标识 <br/>
     *
     * @param id 主键ID
     * @param enable 标识枚举
     */
    void updateEnable(String id, YesOrNoEnum enable);

}
