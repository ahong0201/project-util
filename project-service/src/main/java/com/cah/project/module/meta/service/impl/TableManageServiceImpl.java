package com.cah.project.module.meta.service.impl;

import cn.hutool.core.collection.ListUtil;
import com.cah.project.module.meta.domain.vo.in.DbTableInfoQuery;
import com.cah.project.module.meta.domain.vo.out.DbTableInfoList;
import com.cah.project.module.meta.service.ITableManageService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>表管理 服务实现 </p> <br/>
 *
 * @author cah
 * @since 2022-2-24
 */
@Service
public class TableManageServiceImpl implements ITableManageService {

    @Override
    public List<DbTableInfoList> selectAllList(DbTableInfoQuery query) {
        System.out.println(query.getDataSourceId());
        return ListUtil.toList(new DbTableInfoList("表1", "table_info"),
                new DbTableInfoList("表的名单自很长的阿道夫阿斯顿是", "212123ssdfsdf"));
    }

}
