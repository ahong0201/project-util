package com.cah.project.module.meta.service.plugs.dataSource;

import com.cah.project.module.meta.service.plugs.MetaPlugsGroup;
import com.cah.project.plugs.IPlugsSlot;
import com.cah.project.plugs.annotation.PlugsSlot;

/**
 * 功能描述: 数据源插件1 <br/>
 */
@PlugsSlot(group = MetaPlugsGroup.DATA_SOURCE, order = 1f)
public class DataSourcePlug1 implements IPlugsSlot {

    @Override
    public boolean isExec(Object... objects) {
        // 重写判断，是否需要执行handler
        return "123".equals(objects[0]);
    }

    @Override
    public void handler(Object... objects) {
        System.out.println("执行处理逻辑1--->" + objects[0]);
    }

}
