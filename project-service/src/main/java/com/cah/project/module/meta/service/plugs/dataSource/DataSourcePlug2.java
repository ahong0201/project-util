package com.cah.project.module.meta.service.plugs.dataSource;

import com.cah.project.module.meta.service.plugs.MetaPlugsGroup;
import com.cah.project.plugs.IPlugsSlot;
import com.cah.project.plugs.annotation.PlugsSlot;

/**
 * 功能描述: 数据源插件2 <br/>
 */
@PlugsSlot(group = MetaPlugsGroup.DATA_SOURCE, order = 0.9f)
public class DataSourcePlug2 implements IPlugsSlot {

    @Override
    public void handler(Object... objects) {
        System.out.println("执行处理逻辑2--->" + objects[0]);
    }

}
