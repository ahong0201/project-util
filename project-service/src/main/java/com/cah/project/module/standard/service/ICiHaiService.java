package com.cah.project.module.standard.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cah.project.core.domain.out.PageList;
import com.cah.project.module.standard.domain.entity.CiHaiEntity;
import com.cah.project.module.standard.domain.vo.CiHaiInfo;
import com.cah.project.module.standard.domain.vo.in.CiHaiQuery;
import com.cah.project.module.standard.domain.vo.out.CiHaiList;

/**
 * 功能描述: 辞海（命名规范） 服务接口 <br/>
 */
public interface ICiHaiService extends IService<CiHaiEntity> {

    /**
     * 功能描述: 分页查询 <br/>
     *
     * @param query 查询入参
     * @return "com.cah.project.core.domain.out.PageList<com.cah.project.module.standard.domain.vo.out.CiHaiList>"
     */
    PageList<CiHaiList> pageList(CiHaiQuery query);

    /**
     * 功能描述: 通过主键ID，查看详情 <br/>
     *
     * @param id 主键ID
     * @return "com.cah.project.module.standard.domain.vo.out.CiHaiInfo"
     */
    CiHaiInfo selectById(String id);

    /**
     * 功能描述: 保存数据 <br/>
     *
     * @param info 数据信息
     */
    void saveInfo(CiHaiInfo info);

    /**
     * 功能描述: 通过主键ID，删除数据 <br/>
     *
     * @param id 主键ID
     */
    void deleteById(String id);

}
