package com.cah.project.module.standard.service.handler;

import com.cah.project.core.dict.domain.vo.DictDataOptions;
import com.cah.project.core.dict.domain.vo.DictTypeOptions;
import com.cah.project.core.dict.handler.dictData.AbstractDictDataHandler;
import com.cah.project.module.standard.service.IDictDataService;
import com.cah.project.module.standard.service.IDictTypeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
public class StandardAbstractDictDataHandler extends AbstractDictDataHandler {

    @Autowired
    private IDictTypeService dictTypeService;
    @Autowired
    private IDictDataService dictDataService;

    @Override
    public Set<DictTypeOptions> getDictTypeList() {
        return dictTypeService.selectAll().stream()
                .map(e -> DictTypeOptions.builder().typeCode(e.getTypeCode()).typeName(e.getTypeName()).build())
                .collect(Collectors.toSet());
    }

    @Override
    public List<DictDataOptions> getDictDataOptions(String typeCode) {
        return dictDataService.selectListByCode(typeCode).stream()
                .map(e -> DictDataOptions.builder().typeCode(e.getTypeCode()).dataLabel(e.getDataLabel()).dataValue(e.getDataValue()).build())
                .collect(Collectors.toList());
    }

    @Override
    public List<DictDataOptions> getDictDataOptions(Collection<String> typeCodeList) {
        return dictDataService.selectListByCodes(typeCodeList).stream()
                .map(e -> DictDataOptions.builder().typeCode(e.getTypeCode()).dataLabel(e.getDataLabel()).dataValue(e.getDataValue()).build())
                .collect(Collectors.toList());
    }
}
