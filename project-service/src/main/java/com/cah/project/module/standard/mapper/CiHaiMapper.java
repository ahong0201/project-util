package com.cah.project.module.standard.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cah.project.module.standard.domain.entity.CiHaiEntity;

/**
 * 功能描述: 辞海（命名规范） mapper <br/>
 */
public interface CiHaiMapper extends BaseMapper<CiHaiEntity> {

}
