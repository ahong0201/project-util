package com.cah.project.module.meta.config.query.impl;

import com.cah.project.module.meta.config.query.AbstractDbQuery;

import java.util.List;

/**
 * 功能描述: 数据库类型为mysql的执行语句 <br/>
 */
public class MySqlQuery extends AbstractDbQuery {

    @Override
    public String getUrl(String ip, String port, String schemaName) {
        return "jdbc:mysql://" + ip + ":" + port + "/" + schemaName;
    }

    @Override
    public String tablesSql(List<String> tables, String schemaName) {
        return null;
    }

    @Override
    public String tableFieldsSql(List<String> tables, String schemaName) {
        return null;
    }

}
