package com.cah.project.module.meta.config.query;

import java.util.List;

/**
 * 功能描述: 数据库执行语句 接口 <br/>
 */
public interface IDbQuery {

    /**
     * 功能描述: 获取 连接地址 <br/>
     *
     * @param ip IP地址
     * @param ip 端口
     * @param schemaName 数据库实例
     * @return "java.lang.String"
     */
    String getUrl(String ip, String port, String schemaName);

    /**
     * 功能描述: 查询表语句 <br/>
     *
     * @param tables 表名列表
     * @param schemaName 数据库实例
     * @return "java.lang.String"
     */
    String tablesSql(List<String> tables, String schemaName);

    /**
     * 功能描述: 查询表字段语句 <br/>
     *
     * @param tables 表名列表
     * @param schemaName 数据库实例
     * @return "java.lang.String"
     */
    String tableFieldsSql(List<String> tables, String schemaName);
}
