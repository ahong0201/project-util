package com.cah.project.module.meta.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cah.project.module.meta.domain.entity.DataSourceEntity;

/**
 * <p>数据源 Mapper </p> <br/>
 *
 * @author cah
 * @since 2021-10-06
 */
public interface DataSourceMapper extends BaseMapper<DataSourceEntity> {

}
