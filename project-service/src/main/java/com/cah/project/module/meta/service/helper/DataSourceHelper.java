package com.cah.project.module.meta.service.helper;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * <p>数据源 服务帮助者 </p> <br/>
 * 只能使用到service的读取方法，不能调用写入方法。
 * @author cah
 * @since 2021-10-06
 */
@Component
@RequiredArgsConstructor(onConstructor_ = {@Autowired, @Lazy})
public class DataSourceHelper {


}
