package com.cah.project.module.meta.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cah.project.core.domain.out.PageList;
import com.cah.project.core.util.PageUtil;
import com.cah.project.exception.BusinessException;
import com.cah.project.module.meta.config.DataSourceConfig;
import com.cah.project.module.meta.domain.converter.DataSourceConverter;
import com.cah.project.module.meta.domain.entity.DataSourceEntity;
import com.cah.project.module.meta.domain.vo.DataSourceInfo;
import com.cah.project.module.meta.domain.vo.in.DataSourceQuery;
import com.cah.project.module.meta.domain.vo.out.DataSourceList;
import com.cah.project.module.meta.mapper.DataSourceMapper;
import com.cah.project.module.meta.service.IDataSourceService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>数据源 服务实现 </p> <br/>
 *
 * @author cah
 * @since 2021-10-06
 */
@Service
@RequiredArgsConstructor(onConstructor_ = {@Autowired, @Lazy})
public class DataSourceServiceImpl extends ServiceImpl<DataSourceMapper, DataSourceEntity> implements IDataSourceService {

    @Override
    public PageList<DataSourceList> pageList(DataSourceQuery query) {
        // 组转查询条件
        Wrapper<DataSourceEntity> wrapper = Wrappers.<DataSourceEntity>lambdaQuery()
                .and(StrUtil.isNotEmpty(query.getName()), w -> w.like(DataSourceEntity::getName, query.getName())
                .or()
                .like(DataSourceEntity::getServiceName, query.getName())
                .or()
                .like(DataSourceEntity::getUsername, query.getName()));
        // 查询数据
        IPage<DataSourceEntity> page = page(PageUtil.buildPage(query), wrapper);
        // 对象转换
        List<DataSourceList> list = DataSourceConverter.INSTANCE.toDataSourceList(page.getRecords());
        return new PageList<>(PageUtil.buildPageInfo(page), list);
    }

    @Override
    public DataSourceInfo selectById(String id) {
        if(StrUtil.isEmpty(id)) {
            return new DataSourceInfo();
        }
        DataSourceEntity entity = getById(id);
        return DataSourceConverter.INSTANCE.toDataSourceInfo(entity);
    }

    @Override
    public void saveInfo(DataSourceInfo info) {
        DataSourceEntity entity = DataSourceConverter.INSTANCE.toDataSourceEntity(info);
        saveOrUpdate(entity);
    }

    @Override
    public void linkTest(DataSourceInfo info) {
        DataSourceConfig dsc = new DataSourceConfig(info);
        if(ObjectUtil.isNull(dsc.getConn())) {
            throw new BusinessException("连接失败，请检查参数");
        }
    }

    @Override
    public void linkTest(String id) {
        DataSourceEntity entity = getById(id);
        DataSourceConfig dsc = new DataSourceConfig(entity);
        if(ObjectUtil.isNull(dsc.getConn())) {
            throw new BusinessException("连接失败，请检查参数");
        }
    }

    @Override
    public void deleteById(String id) {
        removeById(id);
    }

    @Override
    public List<DataSourceEntity> selectAll() {
        return lambdaQuery().list();
    }

}
