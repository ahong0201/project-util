package com.cah.project.module.meta.config.query;

import com.cah.project.module.meta.config.query.impl.MySqlQuery;
import com.cah.project.module.meta.config.query.impl.OracleQuery;
import com.cah.project.module.meta.enums.DbTypeEnum;

import java.util.EnumMap;
import java.util.Map;

/**
 * 功能描述: 数据库查询语句注册表 <br/>
 */
public class DbQueryRegistry {

    private final static Map<DbTypeEnum, IDbQuery> DB_QUERY_ENUM_MAP = new EnumMap<>(DbTypeEnum.class);

    static {
        DB_QUERY_ENUM_MAP.put(DbTypeEnum.MYSQL, new MySqlQuery());
        DB_QUERY_ENUM_MAP.put(DbTypeEnum.ORACLE, new OracleQuery());
    }

    /**
     * 功能描述: 通过数据库类型，返回查询语句 <br/>
     *
     * @param dte 数据库类型
     * @return "com.cah.project.module.meta.config.query.IDbQuery"
     */
    public static IDbQuery getDbQuery(DbTypeEnum dte) {
        return DB_QUERY_ENUM_MAP.get(dte);
    }

}
