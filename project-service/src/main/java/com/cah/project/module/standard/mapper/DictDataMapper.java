package com.cah.project.module.standard.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cah.project.module.standard.domain.entity.DictDataEntity;

/**
 * 功能描述: 字典数据 mapper <br/>
 */
public interface DictDataMapper extends BaseMapper<DictDataEntity> {

}
