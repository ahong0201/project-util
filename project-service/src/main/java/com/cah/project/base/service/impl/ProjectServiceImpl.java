package com.cah.project.base.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.cah.project.base.mapper.ProjectBaseMapper;
import com.cah.project.base.service.IProjectService;
import com.cah.project.core.conf.mybatis.enums.ProjectSqlMethod;
import org.apache.ibatis.session.SqlSession;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProjectServiceImpl<M extends ProjectBaseMapper<T>, T> extends ServiceImpl<M, T> implements IProjectService<T> {

    protected String sqlStatement(ProjectSqlMethod sqlMethod) {
        return SqlHelper.table(currentModelClass()).getSqlStatement(sqlMethod.getMethod());
    }

    @Override
    public List<T> ListByPermission(Wrapper<T> queryWrapper) {
        Map<String, Object> map = new HashMap<>(1);
        map.put(Constants.WRAPPER, queryWrapper);
        SqlSession sqlSession = SqlHelper.sqlSession(currentModelClass());
        try {
            return sqlSession.selectList(sqlStatement(ProjectSqlMethod.SELECT_LIST_PERMISSION), map);
        } finally {
            closeSqlSession(sqlSession);
        }
    }

}
