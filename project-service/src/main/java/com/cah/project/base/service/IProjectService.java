package com.cah.project.base.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface IProjectService<T> extends IService<T> {

    /**
     * 功能描述: 带权限的查询 <br/>
     *
     * @param queryWrapper 实体对象封装操作类（可以为 null）
     * @return "java.util.List<T>"
     */
    List<T> ListByPermission(Wrapper<T> queryWrapper);

}
