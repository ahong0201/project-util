package com.cah.project.base.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 功能描述: 自定义项目BaseMapper <br/>
 */
public interface ProjectBaseMapper<T> extends BaseMapper<T> {

    /**
     * 功能描述: 带权限的查询 <br/>
     *
     * @param queryWrapper 实体对象封装操作类（可以为 null）
     * @return "java.util.List<T>"
     */
    List<T> selectListPermission(@Param(Constants.WRAPPER) Wrapper<T> queryWrapper);

}
