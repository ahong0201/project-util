package com.cah.project.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BigDecimalChainTest {

    public static void main(String[] args) {
        BigDecimal build = BigDecimalChain
                .chain(new BigDecimal("2"))
                .add(new BigDecimal("3"))
                .add("3", "4")
                .sub(5)
                .mul(2.2F)
                // 存在问题，如果除不尽，再除以相同的数，会出现精度错误问题。有待优化
                .div(4.1D, 5)
                .mul(4.1D).scale(6).scale(2, RoundingMode.HALF_UP).get();
        System.out.println(build);
    }

}