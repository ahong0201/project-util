package com.cah.project.yapi.enums;

import com.cah.project.core.dict.annotation.Dict;
import io.swagger.annotations.ApiModelProperty;

import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 功能描述: 字段工具 <br/>
 */
public class FieldUtil {

    /** 通过类，获取字段信息 */
    public static Map<String, Object> getClassFieldParam(Class<?> clazz) {
        // 暂时不考虑父类的父类，递归往上，后续补充
        Class<?> superclass = clazz.getSuperclass();
        Field[] superFields = new Field[0];
        if(superclass != null && !"Object".equals(superclass.getSimpleName())) {
            // 存在父类，获取父类字段
            superFields = superclass.getDeclaredFields();
        }
        Field[] fields = clazz.getDeclaredFields();
        Map<String, Object> fieldMap = new LinkedHashMap<>(fields.length + superFields.length);
        for (Field field : fields) {
            // 这里对字段进行过滤，不需要的剔除
            if("serialVersionUID".equals(field.getName())) {
                continue;
            }
            fieldMap.put(field.getName(), FieldTypeEnum.getObjectParam(field.getType().getSimpleName(), getDescription(field), field));
        }
        for (Field field : superFields) {
            // 这里对字段进行过滤，不需要的剔除
            if("serialVersionUID".equals(field.getName())) {
                continue;
            }
            fieldMap.put(field.getName(), FieldTypeEnum.getObjectParam(field.getType().getSimpleName(), getDescription(field), field));
        }
        return fieldMap;
    }

    /** 通过swagger注解，获取字段描述 */
    public static String getDescription(Field field) {
        ApiModelProperty apiModeProperty = field.getAnnotation(ApiModelProperty.class);
        String desc = "";
        if(apiModeProperty != null) {
            desc = apiModeProperty.value();
            // 可以做一些其他处理，比如说如果有字典的话，拼接字典内容
            // 例如依赖 project-dict框架
            Dict dict = field.getAnnotation(Dict.class);
            if(dict != null) {
                desc += "(" + dict.type() + ", " + dict.desc() + ")";
            }
        }
        return desc;
    }

}
