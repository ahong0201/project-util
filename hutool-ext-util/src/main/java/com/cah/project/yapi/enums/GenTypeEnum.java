package com.cah.project.yapi.enums;

import com.cah.project.yapi.FieldParam;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 功能描述: 生成类型枚举（可自定义修改） <br/>
 */
@Getter
@AllArgsConstructor
public enum GenTypeEnum {

    PAGE("分页数据") {
        @Override
        public Object getResponseData(String desc, Class<?> clazz) {
            Map<String, Object> dataMap = new LinkedHashMap<>();
            // 分页对象，根据自己项目进行修改 {@link com.cah.project.core.domain.out.PageInfo}
            Map<String, Object> pageMap = new LinkedHashMap<>(4);
            pageMap.put("recordCount", "总数量");
            pageMap.put("pageCount", "总页数");
            pageMap.put("pageSize", "每页最大数量");
            pageMap.put("currentNumber", "当前页码");
            dataMap.put("pageInfo", pageMap);
            // 添加数据列表
            dataMap.put("list", LIST.getResponseData(clazz));
            return dataMap;
        }

        @Override
        public Object getRequestData(String desc, Class<?> clazz) {
            return OBJECT.getRequestData(desc, clazz);
        }
    },

    LIST("数据列表") {
        @Override
        public Object getResponseData(String desc, Class<?> clazz) {
            Map<String, Object> listMap = new LinkedHashMap<>();
            listMap.put("type", "array");
            listMap.put("description", desc);
            listMap.put("item", OBJECT.getResponseData(clazz));
            return listMap;
        }

        @Override
        public Object getRequestData(String desc, Class<?> clazz) {
            return getResponseData(desc, clazz);
        }
    },

    OBJECT("数据对象") {
        @Override
        public Object getResponseData(String desc, Class<?> clazz) {
            return FieldParam.getObject(desc, FieldUtil.getClassFieldParam(clazz));
        }

        @Override
        public Object getRequestData(String desc, Class<?> clazz) {
            return getResponseData(desc, clazz);
        }
    },
    ;

    private final String desc;

    /** 获取YApi响应数据 */
    public Object getResponseData(Class<?> clazz) {
        return getResponseData(getDesc(), clazz);
    }

    /** 获取YApi响应数据 */
    public abstract Object getResponseData(String desc, Class<?> clazz);

    /** 获取YApi请求数据 */
    public Object getRequestData(Class<?> clazz) {
        return getRequestData(getDesc(), clazz);
    }

    /** 获取YApi请求数据 */
    public abstract Object getRequestData(String desc, Class<?> clazz);

}
