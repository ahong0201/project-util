package com.cah.project.yapi.enums;

import com.cah.project.yapi.FieldParam;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.stream.Stream;

/**
 * 功能描述: 类字段类型枚举（字段类型需要再补充） <br/>
 */
@Getter
@AllArgsConstructor
public enum FieldTypeEnum {

    INT("int") {
        @Override
        public Object getFieldParam(String desc, Field field) {
            return FieldParam.getInteger(desc);
        }
    },
    INTEGER("Integer") {
        @Override
        public Object getFieldParam(String desc, Field field) {
            return FieldParam.getInteger(desc);
        }
    },
    LONG("long") {
        @Override
        public Object getFieldParam(String desc, Field field) {
            return FieldParam.getNumber(desc);
        }
    },
    LONG_UP("Long") {
        @Override
        public Object getFieldParam(String desc, Field field) {
            return FieldParam.getNumber(desc);
        }
    },
    DOUBLE("double") {
        @Override
        public Object getFieldParam(String desc, Field field) {
            return FieldParam.getNumber(desc);
        }
    },
    DOUBLE_UP("Double") {
        @Override
        public Object getFieldParam(String desc, Field field) {
            return FieldParam.getNumber(desc);
        }
    },
    FLOAT("float") {
        @Override
        public Object getFieldParam(String desc, Field field) {
            return FieldParam.getNumber(desc);
        }
    },
    FLOAT_UP("Float") {
        @Override
        public Object getFieldParam(String desc, Field field) {
            return FieldParam.getNumber(desc);
        }
    },
    BIG_DECIMAL("BigDecimal") {
        @Override
        public Object getFieldParam(String desc, Field field) {
            return FieldParam.getNumber(desc);
        }
    },
    BOOLEAN("boolean") {
        @Override
        public Object getFieldParam(String desc, Field field) {
            return FieldParam.getBoolean(desc);
        }
    },
    BOOLEAN_UP("Boolean") {
        @Override
        public Object getFieldParam(String desc, Field field) {
            return FieldParam.getBoolean(desc);
        }
    },
    STRING("String") {
        @Override
        public Object getFieldParam(String desc, Field field) {
            return FieldParam.getString(desc);
        }
    },
    LIST("List") {
        @Override
        public Object getFieldParam(String desc, Field field) {
            return GenTypeEnum.LIST.getResponseData(FieldUtil.getDescription(field),
                    // 获取泛型比较麻烦
                    (Class<?>) ((ParameterizedType)field.getGenericType()).getActualTypeArguments()[0]);
        }
    },
    MAP("Map") {
        @Override
        public Object getFieldParam(String desc, Field field) {
            // 用Map返回的，直接打死吧
            return null;
        }
    },
    OBJECT("") {
        @Override
        public Object getFieldParam(String desc, Field field) {
            return GenTypeEnum.OBJECT.getResponseData(FieldUtil.getDescription(field), field.getType());
        }
    },;

    private final String type;

    /** 获取字段返回 */
    public abstract Object getFieldParam(String desc, Field field);

    /** 获取字段返回 */
    public static Object getObjectParam(String type, String desc, Field field) {
        return indexOf(type).getFieldParam(desc, field);
    }

    /** 获取字段类型枚举 */
    public static FieldTypeEnum indexOf(String type) {
        return Stream.of(values()).filter(e -> e.getType().equals(type)).findFirst().orElse(OBJECT);
    }

}
