package com.cah.project.yapi;

import cn.hutool.json.JSONUtil;
import com.cah.project.module.standard.domain.vo.in.DictDataQuery;
import com.cah.project.yapi.enums.GenTypeEnum;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 功能描述: yapi 生成raw工具 <br/>
 */
public class YApiGenRawUtil {

    public static void main(String[] args) {
        requestBody(DictDataQuery.class, GenTypeEnum.PAGE);
    }

    /** 功能描述: 请求对象json字符串 */
    private static void requestBody(Class<?> clazz, GenTypeEnum type) {
        // 添加固定头
        System.out.println("\n");
        System.out.println(JSONUtil.toJsonStr(type.getRequestData(clazz)));
        System.out.println("\n");
        System.out.println(JSONUtil.toJsonPrettyStr(type.getRequestData(clazz)));
    }

    /**
     * 功能描述: 响应对象Raw字符串 <br/>
     */
    private static void responseBody(Class<?> clazz, GenTypeEnum type) {
        Map<String, Object> resMap = new LinkedHashMap<>();
        // 添加固定头
        resMap.put("$schema", "http://json-schema.org/draft-04/schema#");
        resMap.put("type", "object");
        // 添加统一返回对象，根据项目不同，返回不同 {@link com.cah.project.core.domain.out.CommonResult}
        Map<String, Object> commonResultMap = new LinkedHashMap<>();
        commonResultMap.put("code", "错误码");
        commonResultMap.put("msg", "错误码描述");
        commonResultMap.put("data", type.getResponseData(clazz));
        resMap.put("properties", commonResultMap);
        // 添加必填字段，如果有需要，自行开发
        resMap.put("required", new ArrayList<>());
        System.out.println("\n");
        System.out.println(JSONUtil.toJsonStr(resMap));
        System.out.println("\n");
        System.out.println(JSONUtil.toJsonPrettyStr(resMap));
    }


}
