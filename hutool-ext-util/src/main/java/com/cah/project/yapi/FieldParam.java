package com.cah.project.yapi;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 功能描述: 字段参数类 <br/>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FieldParam {

    /** yapi字段类型 */
    private String type;

    /** yapi描述 */
    private String description;

    /** yapi对象 */
    private Object properties;

    /** 获取字符串类型 */
    public static FieldParam getString(String description) {
        return new FieldParam("string", description, null);
    }

    /** 获取整型类型 */
    public static FieldParam getInteger(String description) {
        return new FieldParam("integer", description, null);
    }

    /** 获取数值类型 */
    public static FieldParam getNumber(String description) {
        return new FieldParam("number", description, null);
    }

    /** 获取字布尔值类型 */
    public static FieldParam getBoolean(String description) {
        return new FieldParam("boolean", description, null);
    }

    /** 获取对象类型 */
    public static FieldParam getObject(String description, Object properties) {
        return new FieldParam("object", description, properties);
    }

}
